import QtQuick 2.2
import Ubuntu.Components 1.1

MainView {
    id: mainWindow

    width: units.gu(40)
    height: units.gu(71)
    useDeprecatedToolbar: false

    backgroundColor: UbuntuColors.coolGrey
    //headerColor: styleMusic.mainView.headerColor

    PageStack {
        id: mainStack

        anchors.fill: parent
    }

    Component.onCompleted: {
        Qt.application.name = "trakt.touch"
        Qt.application.organization = ""
        mainStack.push(Qt.resolvedUrl("FavoritePage.qml"))
    }
}
