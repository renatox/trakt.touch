set(EXAMPLES_QMLS
#    CaledarPage.qml
#    EpidosePage.qml
#    EpisodesPage.qml
    FavoritePage.qml
    OnlinePage.qml
#    SeasonsPage.qml
    ShowDelegate.qml
    main.qml
)

add_custom_target(trakt_examples_components_QmlFiles ALL SOURCES ${EXAMPLES_QMLS})

