import QtQuick 2.2
import Ubuntu.Components 1.1
import Trakt 0.1 as Trakt

Page {
    title: i18n.tr("Favorites")
    ListView {
        id: listView

        anchors.fill: parent
        model: Trakt.OnlineShowsModel {}
        delegate: ShowDelegate {
            title: "%1 (%2)".arg(showTitle).arg(year)
            imageSource: poster ? poster.thumb : ""
            ratingNumber: rating
            extendInfo: {
                var detail = status
                if (status === "returning series") {
                    detail = "%2 %3".arg(airs.day).arg(airs.time)
                } else if (status === "in production") {
                    detail = i18n.tr("TBD")
                } else if (status === "canceled") {
                    detail = "Canceled"
                } else if (status === "ended") {
                    detail = "Ended"
                }
                return "%1 - %2".arg(network).arg(detail)
            }
            Icon {
                anchors {
                    right: parent.right
                    top: parent.top
                    margins: units.gu(1)
                }
                height: units.gu(3)
                width: height
                name: saved ? "starred" : "non-starred"
                MouseArea {
                    anchors.fill: parent
                    onClicked:listView.model.save(index)
                }
            }
        }
    }
}
