import QtQuick 2.2
import Ubuntu.Components 1.1
import Ubuntu.Components.ListItems 1.0 as ListItem

Item {
    property alias title: title.text
    property alias extendInfo: extendInfo.text
    property alias imageSource: image.source
    property double ratingNumber: 0
    readonly property double realRating: (ratingNumber > 0) ? Math.floor(ratingNumber / 2) : 0
    onRealRatingChanged: console.debug("Real rating:" + realRating)
    property string imageFallback: Qt.resolvedUrl("art/trakt_poster.png")


    anchors {
        left: parent.left
        right: parent.right
    }
    height: units.gu(15)

    UbuntuShape {
        id: imageShape

        anchors {
            top: parent.top
            left: parent.left
            bottom: parent.bottom
            margins: units.gu(1)
        }
        width: units.gu(10)
        visible: image.status === Image.Ready

        image: Image {
            id: image
        }
    }
    UbuntuShape {
        id: imageFallbackShape

        anchors.fill: imageShape
        visible: image.status != Image.Ready

        image: Image {
            source: imageFallbackShape.visible ? imageFallback : ""
        }
    }

    Label {
        id: title

        anchors {
            left: imageShape.right
            top: parent.top
            right: parent.right
            margins: units.gu(1)
        }
        //color: "white"
        elide: Text.ElideRight
    }

    Row {
        anchors {
            left: imageShape.right
            top: title.bottom
            margins: units.gu(1)
        }
        height: units.gu(3)
        width: childrenRect.width
        spacing: units.gu(0.5)

        Repeater {
            id: ratingRepeat
            model: 5
            Icon {
                name: (realRating > index) ? "starred" : "non-starred"
                height: units.gu(1.5)
                width: height
                color: UbuntuColors.warmGrey
            }
        }
    }

    Label {
        id: extendInfo

        anchors {
            right: parent.right
            bottom: parent.bottom
            margins: units.gu(1)
        }
        //color: "white"
        fontSize: "small"
    }

    ListItem.ThinDivider {}
}
