if(CLICK_MODE)
    set(APPARMOR_FILE "trakt-tv-touch.apparmor")

    set(CLICK_FILES
        manifest.json.in
        ${APPARMOR_FILE}
    )

    configure_file(manifest.json.in ${CMAKE_CURRENT_BINARY_DIR}/manifest.json)
    install(FILES ${CMAKE_CURRENT_BINARY_DIR}/manifest.json ${APPARMOR_FILE}
            DESTINATION ${CMAKE_INSTALL_PREFIX})

    # make the click files visible in qtcreator
    add_custom_target(com_ubuntu_trakt_touch_CLICKFiles ALL SOURCES ${CLICK_FILES})
endif(CLICK_MODE)
