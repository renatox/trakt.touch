import Ubuntu.OnlineAccounts.Plugin 1.0

OAuthMain {
    creationComponent: OAuth {
        function completeCreation(reply) {
            console.log("Access token: " + reply.AccessToken)
            var http = new XMLHttpRequest()
            var url = "https://api-v2launch.trakt.tv/users/settings";
            http.open("GET", url, true);
            http.setRequestHeader("Content-Type", "application/json")
            http.setRequestHeader("Authorization", "Bearer " + reply.AccessToken)
            http.setRequestHeader("trakt-api-version", "2")
            http.setRequestHeader("trakt-api-key", "bb8679f2a76eb67ef64aee6e728b0d215b561f32d11387736e550dcc6d412c55")
            http.onreadystatechange = function() {
                if (http.readyState === 4){
                    if (http.status == 200) {
                        console.log("ok")
                        console.log("response text: " + http.responseText)
                        var response = JSON.parse(http.responseText)
                        account.updateDisplayName(response.user.username)
                        account.synced.connect(finished)
                        account.sync()

                    } else {
                        console.log("error: " + http.status)
                        cancel()
                    }
                }
            };

            http.send(null);
        }
    }
}
