/*
 *  This file is part of Trakt.tv.touch
 *
 * Trakt.tv.touch App is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * Ubuntu Calendar App is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __MEDIA_INFO__
#define __MEDIA_INFO__

#include "database.h"

#include <QObject>
#include <QMap>
#include <QByteArray>
#include <QNetworkReply>
#include <QJsonObject>
#include <QQueue>
#include <QScopedPointer>

class DownloadQueue;
class TraktRequest;

class MediaInfo : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QString traktId READ traktId NOTIFY infoChanged)
    Q_PROPERTY(QString imdbId READ imdbId NOTIFY infoChanged)
    Q_PROPERTY(QString title READ title NOTIFY infoChanged)
    Q_PROPERTY(QDateTime updatedAt READ updatedAt NOTIFY infoChanged)
    Q_PROPERTY(bool saved READ saved NOTIFY savedChanged)
    Q_PROPERTY(bool downloading READ downloading NOTIFY downaloadingChanged)

public:
    enum ImageSize {
        ImageSizeFull = 0,
        ImageSizeMedium,
        ImageSizeThumb
    };

    enum Field {
        FieldTraktId = 0,
        FieldImdbId,
        FieldTitle,
        FieldUpdatedAt,
        FiledLastDownload,
        FieldLast,
    };

    MediaInfo(QObject *parent = 0);
    ~MediaInfo();

    QString id() const;
    QString traktId() const;
    QString imdbId() const;
    QString title() const;
    bool downloading() const;
    bool saved() const;
    QDateTime updatedAt() const;
    QDateTime lastDownload() const;

    bool hasField(uint field) const;

    virtual void download(bool force) = 0;
    virtual QString fieldName(uint field) const;
    virtual QVariant value(uint field) const;

    void downloadData();
    Q_INVOKABLE virtual void save();
    Q_INVOKABLE virtual void remove();

Q_SIGNALS:
    void infoChanged();
    void savedChanged();
    void downloadFinished(bool error);
    void downaloadingChanged();
    void downloadDataFinished();

private Q_SLOTS:
    void onReplyFinished(const QUrl &url, const QJsonDocument &doc);
    void onReplyError(QNetworkReply::NetworkError);
    void onDownloadFinished(const QUrl &remoteFile, const QUrl &localFile, bool sucess);

protected:
    MediaInfo(const DBItemCache  &item, QObject *parent = 0);
    MediaInfo(const QString &table, const QString &id, QObject *parent = 0);

    void setField(uint field, const QVariant &value);
    void reloadInfo(const QString &suffix);
    //void downloadFile(const QUrl &file);

    // Images map field
    QVariantMap parseImageEntry(const QJsonObject &obj) const;
    void updateNonLocalImages(QVariantMap *images, const QVariantMap &newImages);
    void updateNonLocalImage(QVariant *image, const QVariant &newImage);

    virtual bool parseResult(const QUrl &url, const QJsonDocument &result);
    virtual bool parseExtendedInfo(const QUrl &url, const QJsonDocument &result);
    virtual QList<QUrl> filesForDownload() const;

    // download control
    bool downloadAboutToStart();
    void downloadAboutToFinish(bool error);

private:
    TraktRequest *m_currentRequest;
    DBItemCache m_fields;
    QSet<QUrl> m_downloadList;

    bool m_saved;
    bool m_downloading;
    bool m_dataChanged;
};

#endif
