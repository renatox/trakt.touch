﻿/*
 *  This file is part of Trakt.tv.touch
 *
 * Trakt.tv.touch App is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * Ubuntu Calendar App is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __SHOW_INFO_H__
#define __SHOW_INFO_H__

#include "media-info.h"
#include "database.h"

#include <QObject>
#include <QNetworkReply>
#include <QScopedPointer>
#include <QQmlListProperty>

class SeasonInfo;

class ShowInfo : public MediaInfo
{
    Q_OBJECT
    Q_PROPERTY(uint year READ year NOTIFY showChanged)
    Q_PROPERTY(QVariantMap fanArt READ fanArt NOTIFY showChanged)
    Q_PROPERTY(QVariantMap poster READ poster NOTIFY showChanged)
    Q_PROPERTY(QVariantMap logo READ logo NOTIFY showChanged)
    Q_PROPERTY(QVariantMap clearArt READ clearArt NOTIFY showChanged)
    Q_PROPERTY(QVariantMap banner READ banner NOTIFY showChanged)
    Q_PROPERTY(QUrl thumb READ thumb NOTIFY showChanged)
    Q_PROPERTY(QString overview READ overview NOTIFY showChanged)
    Q_PROPERTY(QDate firstAired READ firstAired NOTIFY showChanged)
    Q_PROPERTY(QVariantMap airs READ airs NOTIFY showChanged)
    Q_PROPERTY(QString network READ network NOTIFY showChanged)
    Q_PROPERTY(QUrl homePage READ homePage NOTIFY showChanged)
    Q_PROPERTY(QString status READ status NOTIFY showChanged)
    Q_PROPERTY(double rating READ rating NOTIFY showChanged)
    Q_PROPERTY(QStringList genres READ genres NOTIFY showChanged)
    Q_PROPERTY(uint airedEpisodes READ airedEpisodes NOTIFY showChanged)
    Q_PROPERTY(QQmlListProperty<SeasonInfo> seasons READ seasonList NOTIFY seasonsChanged)

public:
    enum ShowInfoFields {
        FieldYear = 1000,
        FieldFanArt,
        FieldPoster,
        FieldLogo,
        FieldClearArt,
        FieldBanner,
        FieldThumb,
        // Extended
        FieldOverview,
        FieldFirstAired,
        FieldAirs,
        FieldNetwork,
        FieldHomePage,
        FieldStatus,
        FieldRating,
        FieldGenres,
        FieldAiredEpisodes,
        FieldLast
    };

    ShowInfo(const QString &id, QObject *parent = 0);
    ~ShowInfo();

    uint year() const;
    QVariantMap fanArt() const;
    QVariantMap poster() const;
    QVariantMap logo() const;
    QVariantMap clearArt() const;
    QVariantMap banner() const;
    QUrl thumb() const;
    // Extended
    QString overview() const;
    QDate firstAired() const;
    QVariantMap airs() const;
    QString network() const;
    QUrl homePage() const;
    QString status() const;
    double rating() const;
    QStringList genres() const;
    uint airedEpisodes();

    QString fieldName(uint field) const;
    Q_INVOKABLE void saveAll();

    QQmlListProperty<SeasonInfo> seasonList();
    QList<SeasonInfo*> seasons() const;

    // MediaInfo
    void download(bool force);
    void downloadSeasons(bool force = false);
    QVariant value(uint field) const;

    static QString staticTableName();
    static QList<ShowInfo*> all(QObject *parent=0);

Q_SIGNALS:
    void showChanged();
    void seasonsDowloaded(bool fail);
    void seasonsChanged();

protected:
    bool parseResult(const QUrl &url, const QJsonDocument &result);
    QList<QUrl> filesForDownload() const;
    QUrl getSmallestImage(const QVariantMap &urls) const;

private:
    QList<SeasonInfo*> m_seasons;

    ShowInfo(DBItemCache item, QObject *parent=0);
    void loadSeasons();
    void parseSeasonsImage(const QJsonDocument &result);
    void parseSeasonsFull(const QJsonDocument &result);

};

#endif
