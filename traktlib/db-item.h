/*
 *
 * This file is part of Trakt.tv.touch
 *
 * Trakt.tv.touch App is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * Ubuntu Calendar App is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __DB_ITEM_H__
#define __DB_ITEM_H__

#include <QObject>
#include <QHash>
#include <QVariant>
#include <QSqlDatabase>
#include <QSqlField>

typedef QMap<QString, QVariant> DBItemFields;

class DBItem : public QObject
{
    Q_OBJECT
public:
    ~DBItem();
    QString id() const;
    void setData(const QString &field, const QVariant &value);
    QVariant data(const QString &field) const;
    bool hasField(const QString &field) const;
    bool save();
    bool remove();
    bool isDirty() const;
    QStringList fieldNames() const;
    QString table() const;

Q_SIGNALS:
    void changed(const QString &table, const QString &id, const QString &field, const QVariant &value);

private:
    QSqlDatabase m_database;
    QString m_table;
    QString m_id;
    DBItemFields m_fields;
    bool m_dirty;

    DBItem(const QSqlDatabase &db, const QString &table, const QString &id, QObject *parent);
    DBItem(const QSqlDatabase &db, const QString &table, const QSqlRecord &record, QObject *parent);
    void createTable(const QString &table, const DBItemFields &fields);
    QString typeSQLName(const QVariant &v) const;
    QVariant valueFromField(const QSqlField &f);
    QVariant fieldToSQLData(const QVariant &v);

    bool exists() const;
    void loadData();
    void loadData(const QSqlRecord &query);

    friend class Database;
};

#endif
