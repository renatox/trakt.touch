/*
 *  This file is part of Trakt.tv.touch
 *
 * Trakt.tv.touch App is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * Ubuntu Calendar App is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include "config.h"
#include "offline-shows.h"
#include "database.h"

#include <QNetworkAccessManager>
#include <QNetworkReply>

#include <QSqlDatabase>

struct ShowLess
{
    bool operator()(const ShowInfo *a, const ShowInfo *b) const
    {
        return a->title() < b->title();
  }
};

OfflineShows::OfflineShows(QObject *parent)
    : QAbstractListModel(parent)
{
    load();
    connect(Database::instance(),
            SIGNAL(itemAdded(QString,QString)),
            SLOT(onItemAdded(QString,QString)),
            Qt::QueuedConnection);
    connect(Database::instance(),
            SIGNAL(itemRemoved(QString,QString)),
            SLOT(onItemRemoved(QString,QString)),
            Qt::QueuedConnection);
}

OfflineShows::~OfflineShows()
{
}

QVariant OfflineShows::data(const QModelIndex &index, int role) const
{
    if ((index.row() >= 0) && (index.row() < m_shows.size())) {
        return m_shows.at(index.row())->value(role);
    }
    return QVariant();
}

int OfflineShows::rowCount(const QModelIndex &) const
{
    return m_shows.count();
}

void OfflineShows::save(int index)
{
    ShowInfo *info = m_shows.at(index);
    if (info) {
        info->save();
    }
}

ShowInfo *OfflineShows::show(int index)
{
    return m_shows.at(index);
}

QHash<int, QByteArray> OfflineShows::roleNames() const
{
    static QHash<int, QByteArray> roles;
    if (roles.isEmpty()) {
        roles.insert(ShowInfo::FieldTraktId, "traktId");
        roles.insert(ShowInfo::FieldImdbId, "imdbId");
        roles.insert(ShowInfo::FieldTitle, "showTitle");
        roles.insert(ShowInfo::FieldYear, "year");
        roles.insert(ShowInfo::FieldBanner, "banner");
        roles.insert(ShowInfo::FieldClearArt, "clearArt");
        roles.insert(ShowInfo::FieldLogo, "logo");
        roles.insert(ShowInfo::FieldPoster, "poster");
        roles.insert(ShowInfo::FieldThumb, "thumb");
        roles.insert(ShowInfo::FieldOverview, "overview");
        roles.insert(ShowInfo::FieldFirstAired, "firstAired");
        roles.insert(ShowInfo::FieldAirs, "airs");
        roles.insert(ShowInfo::FieldNetwork, "network");
        roles.insert(ShowInfo::FieldHomePage, "homePage");
        roles.insert(ShowInfo::FieldStatus, "status");
        roles.insert(ShowInfo::FieldRating, "rating");
        roles.insert(ShowInfo::FieldAiredEpisodes, "airedEpisodes");
        roles.insert(ShowInfo::FieldGenres, "genres");
    }

    return roles;
}

void OfflineShows::load()
{
    beginResetModel();
    Q_FOREACH(ShowInfo *show, ShowInfo::all(this)) {
        connect(show,
                SIGNAL(showChanged()),
                this, SLOT(onShowChanged()));
        m_shows << show;
    }
    qSort(m_shows.begin(), m_shows.end(), ShowLess());
    endResetModel();
}

void OfflineShows::onShowChanged()
{
    ShowInfo *info = qobject_cast<ShowInfo*>(QObject::sender());
    int row = m_shows.indexOf(info);
    dataChanged(index(row), index(row));
}

void OfflineShows::onItemAdded(const QString &table, const QString &id)
{
    if (table == ShowInfo::staticTableName()) {
        ShowInfo *newShow = new ShowInfo(id, this);
        QList<ShowInfo*> newList = m_shows;

        newList << newShow;
        qSort(newList.begin(), newList.end(), ShowLess());

        int index = newList.indexOf(newShow);

        beginInsertRows(QModelIndex(), index, index);
        m_shows.insert(index, newShow);
        endInsertRows();
    }
}

void OfflineShows::onItemRemoved(const QString &table, const QString &id)
{
    if (table == ShowInfo::staticTableName()) {
        int index = -1;
        for(int i=0; i < m_shows.size(); i++) {
            if (m_shows.at(i)->id() == id) {
                index = i;
                break;
            }
        }

        if (index != -1) {
            beginRemoveRows(QModelIndex(), index, index);
            delete m_shows.takeAt(index);
            endRemoveRows();
        }
    }
}
