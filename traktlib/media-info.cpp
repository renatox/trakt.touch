/*
 *
 * This file is part of Trakt.tv.touch
 *
 * Trakt.tv.touch App is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * Ubuntu Calendar App is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "media-info.h"
#include "trakt-request.h"
#include "database.h"
#include "db-item.h"
#include "download-queue.h"

#include <QJsonDocument>
#include <QTemporaryFile>
#include <QStandardPaths>
#include <QDir>
#include <QUuid>

MediaInfo::MediaInfo(const DBItemCache &data, QObject *parent)
    : QObject(parent),
      m_fields(data),
      m_currentRequest(0),
      m_downloading(false)
{
    connect(m_fields.data(),
            SIGNAL(changed(QString,QString,QString,QVariant)),
            SIGNAL(infoChanged()));

    connect(DownloadQueue::instance(),
            SIGNAL(fileDownloaded(QUrl,QUrl,bool)),
            SLOT(onDownloadFinished(QUrl,QUrl,bool)));
}

MediaInfo::MediaInfo(const QString &table, const QString &id, QObject *parent)
    : m_currentRequest(0),
      m_downloading(false)
{
    m_fields = Database::instance()->item(table, id);
    connect(m_fields.data(),
            SIGNAL(changed(QString,QString,QString,QVariant)),
            SIGNAL(infoChanged()));
    connect(DownloadQueue::instance(),
            SIGNAL(fileDownloaded(QUrl,QUrl,bool)),
            SLOT(onDownloadFinished(QUrl,QUrl,bool)));
}

MediaInfo::~MediaInfo()
{
    if (m_currentRequest) {
        delete m_currentRequest;
    }
}

QString MediaInfo::id() const
{
    return m_fields->id();
}

QString MediaInfo::traktId() const
{
    return value(MediaInfo::FieldTraktId).toString();
}

QString MediaInfo::imdbId() const
{
    return value(MediaInfo::FieldImdbId).toString();
}

QString MediaInfo::title() const
{
    return  value(MediaInfo::FieldTitle).toString();
}

QDateTime MediaInfo::updatedAt() const
{
    return value(MediaInfo::FieldUpdatedAt).toDateTime();
}

QDateTime MediaInfo::lastDownload() const
{
    return value(MediaInfo::FiledLastDownload).toDateTime();
}

bool MediaInfo::hasField(uint field) const
{
    return m_fields->hasField(fieldName(field));
}

void MediaInfo::setField(uint field, const QVariant &value)
{
    m_fields->setData(fieldName(field), value);
}

QVariant MediaInfo::value(uint field) const
{
    return m_fields->data(fieldName(field));
}

void MediaInfo::downloadData()
{
    if (!m_downloadList.isEmpty()) {
        qWarning() << "Download in progress";
        Q_EMIT downloadDataFinished();
        return;
    }

    QSet<QUrl> newUrls = QSet<QUrl>::fromList(filesForDownload());
    if (newUrls.isEmpty()) {
        Q_EMIT downloadDataFinished();
        return;
    }
    m_downloadList += newUrls;
    DownloadQueue::instance()->append(newUrls.toList());
}

void MediaInfo::reloadInfo(const QString &suffix)
{
    if (m_currentRequest) {
        qWarning() << "Reload already in progress";
        return;
    }

    m_currentRequest = newTrackRequest(this);
    m_currentRequest->setSuffix(suffix);

    connect(m_currentRequest,
            SIGNAL(finished(QUrl,QJsonDocument)),
            SLOT(onReplyFinished(QUrl,QJsonDocument)));
    connect(m_currentRequest,
            SIGNAL(error(QNetworkReply::NetworkError)),
            SLOT(onReplyError(QNetworkReply::NetworkError)));

    m_currentRequest->start();
}

bool MediaInfo::parseResult(const QUrl &url, const QJsonDocument &result)
{
    Q_UNUSED(url);
    QJsonObject media = result.object();

    setField(MediaInfo::FieldTitle, media["title"].toString());
    QJsonObject ids = media["ids"].toObject();
    setField(MediaInfo::FieldTraktId, QString::number(ids["trakt"].toInt()));
    setField(MediaInfo::FieldImdbId, ids["imdb"].toString());

    return true;
}

bool MediaInfo::parseExtendedInfo(const QUrl &url, const QJsonDocument &result)
{
    Q_UNUSED(url);
    QJsonObject media = result.object();

    setField(MediaInfo::FieldUpdatedAt, media.value("updated_at").toString());
    return true;
}

QVariantMap MediaInfo::parseImageEntry(const QJsonObject &obj) const
{
    QVariantMap images;
    images.insert(QStringLiteral("full"),
                  QUrl(obj.value("full").toString()));
    images.insert(QStringLiteral("medium"),
                  QUrl(obj.value("medium").toString()));
    images.insert(QStringLiteral("thumb"),
                  QUrl(obj.value("thumb").toString()));

    return images;
}

void MediaInfo::updateNonLocalImages(QVariantMap *images, const QVariantMap &newImages)
{
    static QStringList imagesNames;
    if (imagesNames.isEmpty()) {
        imagesNames << "full" << "medium" << "thumb";
    }

    Q_FOREACH(const QString &imageName, imagesNames) {
        if (images->contains(imageName)) {
            QVariant &image = (*images)[imageName];
            updateNonLocalImage(&image, newImages.value(imageName));
        } else {
            images->insert(imageName, newImages.value(imageName));
        }
    }
}

void MediaInfo::updateNonLocalImage(QVariant *image, const QVariant &newImage)
{
    Q_ASSERT((int) newImage.type() == QMetaType::QUrl);
    // update image only if not local file
    if (!image->isValid() || !image->toUrl().isLocalFile()) {
        *image = newImage;
    }
}

QList<QUrl> MediaInfo::filesForDownload() const
{
    QList<QUrl> files;

    Q_FOREACH(const QString &key, m_fields->fieldNames()) {
        QVariant value = m_fields->data(key);
        if (value.type() == QVariant::Url) {
            QUrl url = value.toUrl();
            if (url.isValid() && !url.isLocalFile()) {
                files << url;
            }
        } else if (value.type() == QVariant::Map) {
            QVariantMap map = value.toMap();
            Q_FOREACH (const QVariant &v, map.values()) {
                if (v.type() == QVariant::Url) {
                    QUrl url = v.toUrl();
                    if (url.isValid() && !url.isLocalFile()) {
                        files << url;
                    }
                }
            }
        }
    }

    return files;
}

bool MediaInfo::downloadAboutToStart()
{
    if (!m_downloading) {
        m_downloading = true;
        Q_EMIT downaloadingChanged();
        return true;
    }
    return false;
}

void MediaInfo::downloadAboutToFinish(bool error)
{
    if (m_downloading) {
        if (!error) {
            setField(MediaInfo::FiledLastDownload, QDateTime::currentDateTime());
        }
        m_downloading = false;
        Q_EMIT downaloadingChanged();
    }

    Q_EMIT downloadFinished(error);
}

void MediaInfo::save()
{
    m_fields->save();
    Q_EMIT savedChanged();
}

void MediaInfo::remove()
{
    m_fields->remove();
    Q_EMIT savedChanged();
}

void MediaInfo::onReplyFinished(const QUrl &url, const QJsonDocument &doc)
{
    m_currentRequest->deleteLater();
    m_currentRequest = 0;
    parseResult(url, doc);
}

void MediaInfo::onReplyError(QNetworkReply::NetworkError error)
{
    qWarning() << "Fail to load["
               << m_currentRequest->url() <<"]:" << error;
    m_currentRequest->deleteLater();
    m_currentRequest = 0;
    Q_EMIT downloadFinished(true);
}

void MediaInfo::onDownloadFinished(const QUrl &remoteFile, const QUrl &localFile, bool sucess)
{
    if (sucess) {
        Q_FOREACH(const QString &key, m_fields->fieldNames()) {
            QVariant value = m_fields->data(key);
            if ((value.type() == QVariant::Url) && (value.toUrl() == remoteFile)) {
                m_fields->setData(key, localFile);
            } else if (value.type() == QVariant::Map) {
                QVariantMap map = value.toMap();
                Q_FOREACH(const QString vKey, map.keys()) {
                    QVariant vValue = map.value(vKey);
                    if ((vValue.type() == QVariant::Url) &&
                        (vValue.toUrl() == remoteFile)) {
                        map.insert(vKey, localFile);
                        m_fields->setData(key, map);
                    }
                }
            }
        }
    }

    m_downloadList.remove(remoteFile);
    if (m_downloadList.isEmpty()) {
        Q_EMIT downloadDataFinished();
    }
}

QString MediaInfo::fieldName(uint field) const
{
    switch(field) {
    case FieldTraktId:
        return QStringLiteral("trakId");
    case FieldImdbId:
        return QStringLiteral("imdbId");
    case FieldTitle:
        return QStringLiteral("title");
    case FieldUpdatedAt:
        return QStringLiteral("updatedAt");
    case FiledLastDownload:
        return QStringLiteral("lastDownload");
    default:
        qWarning() << "Field without name" << field;
        return QString();
    };
}


bool MediaInfo::downloading() const
{
    return m_downloading;
}

bool MediaInfo::saved() const
{
    return !m_fields->isDirty();
}
