/*
 *  This file is part of Trakt.tv.touch
 *
 * Trakt.tv.touch App is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * Ubuntu Calendar App is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __SHOW_DOWNLOADER_H__
#define __SHOW_DOWNLOADER_H__

#include <QObject>
#include <QPointer>
#include <QQueue>

class ShowInfo;
class MediaInfo;

class ShowDownloader : public QObject
{
    Q_OBJECT
    Q_PROPERTY(bool forceDownload READ forceDownload WRITE setForceDownload NOTIFY forceDownloadChanged)
    Q_PROPERTY(bool checkForNewEpisode READ checkForNewEpisode WRITE setCheckForNewEpisode NOTIFY checkForNewEpisodeChanged)
    Q_PROPERTY(ShowInfo *show READ show WRITE setShow NOTIFY showChanged)
    Q_PROPERTY(bool downloading READ downloading NOTIFY downloadingChanged)
    Q_PROPERTY(bool downloadEpisodeExtendedData READ downloadEpisodeExtendedData WRITE setDownloadEpisodeExtendedData NOTIFY downloadEpisodeExtendedDataChanged)
    Q_PROPERTY(qreal progress READ progress NOTIFY progressChanged)

public:
    ShowDownloader(QObject *parent = 0);
    ShowDownloader(ShowInfo *info, bool singleDownload = true, bool forceDownload = false, QObject *parent = 0);
    ~ShowDownloader();

    ShowInfo *show() const;
    void setShow(ShowInfo *show);

    bool forceDownload() const;
    void setForceDownload(bool flag);

    bool checkForNewEpisode() const;
    void setCheckForNewEpisode(bool flag);

    bool downloading() const;

    bool downloadEpisodeExtendedData() const;
    void setDownloadEpisodeExtendedData(bool flag);

    qreal progress() const;

public Q_SLOTS:
    void start();

Q_SIGNALS:
    void downloadFinished();
    void downloadError();
    void showChanged();
    void forceDownloadChanged();
    void downloadingChanged();
    void checkForNewEpisodeChanged();
    void downloadEpisodeExtendedDataChanged();
    void progressChanged();

private Q_SLOTS:
    void onShowDataDownloaded();
    void onShowDownloaded(bool error);
    void onSeasonsDownloaded(bool error);
    void onSeasonDownloaded(bool error);
    void onSeasonDataDownloaded();
    void onEpisodeDownloaded(bool error);
    void onEpisodeExtendedDownloaded(bool error);
    void onEpisodeDataDownloaded();
    void onDownloadingDone();

private:
    QPointer<ShowInfo> m_show;
    QQueue<MediaInfo*> m_downloadQueue;
    bool m_singleDownload;
    bool m_forceDownload;
    bool m_downloading;
    bool m_checkForNewEpisodes;
    bool m_downloadEpisodeExtendedData;
    qreal m_progress;
    qreal m_totalProgress;
};

#endif
