/*
 *  This file is part of Trakt.tv.touch
 *
 * Trakt.tv.touch App is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * Ubuntu Calendar App is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "qml-plugin.h"
#include "calendar-model.h"
#include "show-info.h"
#include "season-info.h"
#include "episode-info.h"
#include "online-shows.h"
#include "offline-shows.h"
#include "show-downloader.h"
#include "trakt-request.h"


#include <QQmlEngine>
#include <qqml.h>

TraktRequest *newTrackRequest(QObject *parent)
{
    return new TraktRequest(parent);
}


void TraktPlugin::initializeEngine(QQmlEngine *engine, const char *uri)
{
    Q_UNUSED(engine);
    Q_UNUSED(uri);
}

void TraktPlugin::registerTypes(const char *uri)
{
    // @uri Ubuntu.Contacts
    qmlRegisterUncreatableType<ShowInfo>(uri, 0, 1, "ShowInfo", "");
    qmlRegisterUncreatableType<SeasonInfo>(uri, 0, 1, "SeasonInfo", "");
    qmlRegisterUncreatableType<EpisodeInfo>(uri, 0, 1, "EpisodeInfo", "");
    qmlRegisterType<OnlineShows>(uri, 0, 1, "OnlineShowsModel");
    qmlRegisterType<OfflineShows>(uri, 0, 1, "OfflineShowsModel");
    qmlRegisterType<CalendarModel>(uri, 0, 1, "CalendarModel");
    qmlRegisterType<ShowDownloader>(uri, 0, 1, "ShowDownloader");
}
