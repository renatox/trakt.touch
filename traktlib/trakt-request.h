/*
 *  This file is part of Trakt.tv.touch
 *
 * Trakt.tv.touch App is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * Ubuntu Calendar App is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __TRAKT_REQUEST_H__
#define __TRAKT_REQUEST_H__

#include <QObject>
#include <QScopedPointer>
#include <QNetworkReply>
#include <QNetworkRequest>
#include <QNetworkAccessManager>
#include <QJsonDocument>

class TraktRequest : public QObject
{
    Q_OBJECT
public:
    TraktRequest(QObject *parent);
    ~TraktRequest();

    void setPagination(int pageNumber, int pageSize);
    void setSuffix(const QString &suffix);
    virtual void start();
    QUrl url() const;

Q_SIGNALS:
    void finished(const QUrl &url, const QJsonDocument &data);
    void error(QNetworkReply::NetworkError error);

private Q_SLOTS:
    void onReplyFinished();
    void onReplyError(QNetworkReply::NetworkError errorCode);

private:
    QScopedPointer<QNetworkAccessManager> m_manager;
    QNetworkReply *m_reply;
    QString m_suffix;
    int m_pageNumber;
    int m_pageSize;

    QString appendPageInfo(const QString &url) const;
};

TraktRequest *newTrackRequest(QObject *parent);

#endif
