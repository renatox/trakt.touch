/*
 *
 * This file is part of Trakt.tv.touch
 *
 * Trakt.tv.touch App is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * Ubuntu Calendar App is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "config.h"
#include "database.h"
#include "db-item.h"

#include <QSqlDatabase>
#include <QSqlQuery>
#include <QSqlRecord>
#include <QSqlError>

#include <QDebug>
#include <QStandardPaths>
#include <QFile>
#include <QDir>

#define DB_CONNECTION_NAME  "trakt.tv"
Database *Database::m_instance = 0;

static void deleteItem(DBItem *obj)
{
    delete obj;
}

Database *Database::instance()
{
    if (!m_instance) {
        m_instance = new Database();
    }
    return m_instance;
}

void Database::destroy()
{
    if (m_instance) {
        delete m_instance;
        m_instance = 0;
    }
}

QList<DBItemCache> Database::items(const QString &table)
{
    return items(table, QString());
}

QList<DBItemCache> Database::items(const QString &table, const QString &where)
{
    QString fullWhere;
    if (!where.isEmpty()) {
        fullWhere = QString(" WHERE %1").arg(where);
    }

    QList<DBItemCache> result;
    QString querySQL = QString("SELECT * FROM %1%2;")
            .arg(table)
            .arg(fullWhere);
    QSqlQuery query(m_db);
    query.prepare(querySQL);
    query.exec();
    while(query.next()) {
        QString id = query.value("id").toString();
        DBItemCache cache = fromCache(table, id);
        if (cache.isNull()) {
            cache = DBItemCache(new DBItem(m_db, table, query.record(), this), deleteItem);
            addToCache(cache);
        }
        result << cache;
    }
    return result;
}

DBItemCache Database::item(const QString &table, const QString &id)
{
    // check cache
    DBItemCache cache = fromCache(table, id);
    if (!cache.isNull()) {
        return cache;
    }

    // check database
    cache = DBItemCache(new DBItem(m_db, table, id, this), deleteItem);
    addToCache(cache);

    return cache;
}

void Database::clearCache()
{
    m_cache.clear();
}

Database::Database()
    : QObject(0)
{
    open();
}

Database::~Database()
{
    m_cache.clear();
    m_db.close();
    m_db = QSqlDatabase();
    QSqlDatabase::removeDatabase(DB_CONNECTION_NAME);
}

bool Database::open()
{
    if (m_db.isValid() && m_db.isOpen()) {
        return true;
    }

    m_db = QSqlDatabase::addDatabase("QSQLITE", DB_CONNECTION_NAME);
    QFileInfo dbPath(fullPath());
    bool newDB = !QFile::exists(dbPath.absoluteFilePath());
    if (newDB) {
        QDir().mkpath(dbPath.absoluteDir().absolutePath());
    }
    m_db.setDatabaseName(dbPath.absoluteFilePath());
    if (!m_db.open()) {
        qWarning() << "Fail to open database" << dbPath.absoluteFilePath() << m_db.lastError();
        return false;
    }
    return true;
}

QString Database::fullPath()
{
    QDir cacheDir(QStandardPaths::writableLocation(QStandardPaths::CacheLocation));
    return cacheDir.absoluteFilePath("trakt.db");
}

DBItemCache Database::fromCache(const QString &table, const QString &id) const
{
    QMap<QString, DBItemCache> items = m_cache.value(table);
    if (items.contains(id)) {
        return items.value(id);
    }
    return DBItemCache(0);
}

void Database::addToCache(const DBItemCache &item)
{
    QMap<QString, DBItemCache> items = m_cache.value(item->table());
    items.insert(item->id(), item);
    m_cache.insert(item->table(), items);
}
