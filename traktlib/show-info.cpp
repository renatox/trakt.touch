/*
 *  This file is part of Trakt.tv.touch
 *
 * Trakt.tv.touch App is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * Ubuntu Calendar App is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "config.h"
#include "show-info.h"
#include "season-info.h"
#include "episode-info.h"
#include "trakt-request.h"
#include "database.h"

#include <QNetworkAccessManager>

#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>

#define BASIC_INFO_URL_SUFFIX       "/shows/%1?extended=images"
#define EXTEND_INFO_URL_SUFFIX      "/shows/%1?extended=full"
#define SEASON_LIST_URL_SUFFIX      "/shows/%1/seasons?extended=images"
#define SEASON_EXTENDED_URL_SUFFIX  "/shows/%1/seasons?extended=full"

ShowInfo::ShowInfo(const QString &id, QObject *parent)
    : MediaInfo(staticTableName(), id, parent)
{
    setField(ShowInfo::FieldTraktId, id);
    loadSeasons();
    connect(this, SIGNAL(infoChanged()), SIGNAL(showChanged()));
}

ShowInfo::ShowInfo(DBItemCache item, QObject *parent)
    : MediaInfo(item, parent)
{
    loadSeasons();
    connect(this, SIGNAL(infoChanged()), SIGNAL(showChanged()));
}

ShowInfo::~ShowInfo()
{
    qDeleteAll(m_seasons);
    m_seasons.clear();
}

uint ShowInfo::year() const
{
    return MediaInfo::value(ShowInfo::FieldYear).toUInt();
}

QVariantMap ShowInfo::fanArt() const
{
    return MediaInfo::value(ShowInfo::FieldFanArt).toMap();
}

QVariantMap ShowInfo::poster() const
{
    return MediaInfo::value(ShowInfo::FieldPoster).toMap();
}

QVariantMap ShowInfo::logo() const
{
    return MediaInfo::value(ShowInfo::FieldLogo).toMap();
}

QVariantMap ShowInfo::clearArt() const
{
    return MediaInfo::value(ShowInfo::FieldClearArt).toMap();
}

QVariantMap ShowInfo::banner() const
{
    return MediaInfo::value(ShowInfo::FieldBanner).toMap();
}

QUrl ShowInfo::thumb() const
{
    return MediaInfo::value(ShowInfo::FieldThumb).toUrl();
}

QString ShowInfo::overview() const
{
    return MediaInfo::value(ShowInfo::FieldOverview).toString();
}

QDate ShowInfo::firstAired() const
{
    QDateTime v = QDateTime::fromString(MediaInfo::value(ShowInfo::FieldFirstAired).toString(), Qt::ISODate);
    return  v.date();
}

QVariantMap ShowInfo::airs() const
{
    return MediaInfo::value(ShowInfo::FieldAirs).toMap();
}

QString ShowInfo::network() const
{
    return MediaInfo::value(ShowInfo::FieldNetwork).toString();
}

QUrl ShowInfo::homePage() const
{
    return MediaInfo::value(ShowInfo::FieldHomePage).toUrl();
}

QString ShowInfo::status() const
{
    return MediaInfo::value(ShowInfo::FieldStatus).toString();
}

double ShowInfo::rating() const
{
    return MediaInfo::value(ShowInfo::FieldRating).toDouble();
}

QStringList ShowInfo::genres() const
{
    return MediaInfo::value(ShowInfo::FieldGenres).toStringList();
}

uint ShowInfo::airedEpisodes()
{
    return MediaInfo::value(ShowInfo::FieldAiredEpisodes).toUInt();
}

QString ShowInfo::fieldName(uint field) const
{
    switch(field) {
    case FieldYear:
        return QStringLiteral("year");
    case FieldFanArt:
        return QStringLiteral("fanArt");
    case FieldPoster:
        return QStringLiteral("poster");
    case FieldLogo:
        return QStringLiteral("logo");
    case FieldClearArt:
        return QStringLiteral("clearArt");
    case FieldBanner:
        return QStringLiteral("banner");
    case FieldThumb:
        return QStringLiteral("thumb");
    case FieldOverview:
        return QStringLiteral("overview");
    case FieldFirstAired:
        return QStringLiteral("firstAired");
    case FieldAirs:
        return QStringLiteral("airs");
    case FieldNetwork:
        return QStringLiteral("network");
    case FieldHomePage:
        return QStringLiteral("homePage");
    case FieldStatus:
        return QStringLiteral("status");
    case FieldRating:
        return QStringLiteral("rating");
    case FieldGenres:
        return QStringLiteral("genres");
    case FieldAiredEpisodes:
        return QStringLiteral("airedEpisodes");
    default:
        return MediaInfo::fieldName(field);
    };
}

void ShowInfo::saveAll()
{
    save();
    Q_FOREACH(SeasonInfo *season, m_seasons) {
        season->save();
        Q_FOREACH(EpisodeInfo *episode, season->episodes()) {
            episode->save();
        }
    }
}

QQmlListProperty<SeasonInfo> ShowInfo::seasonList()
{
     return QQmlListProperty<SeasonInfo>(this, m_seasons);
}

QList<SeasonInfo*> ShowInfo::seasons() const
{
    return m_seasons;
}

void ShowInfo::download(bool force)
{
    if ((force || title().isEmpty()) &&
        downloadAboutToStart()) {
        reloadInfo(QString(BASIC_INFO_URL_SUFFIX).arg(traktId()));
    } else {
        downloadAboutToFinish(false);
    }
}

void ShowInfo::downloadSeasons(bool force)
{
    if (force) {
        qDeleteAll(m_seasons);
        m_seasons.clear();
        Q_EMIT seasonsChanged();
    }

    if (m_seasons.isEmpty()) {
        reloadInfo(QString(SEASON_LIST_URL_SUFFIX).arg(traktId()));
    } else {
        Q_EMIT seasonsDowloaded(false);
    }
}

QVariant ShowInfo::value(uint field) const
{
    switch (field) {
    case ShowInfo::FieldFirstAired:
        return firstAired();
        break;
    default:
        return MediaInfo::value(field);
        break;
    }
}

bool ShowInfo::parseResult(const QUrl &url, const QJsonDocument &result)
{
    QJsonObject show = result.object();
    if (url.toString().endsWith("/seasons?extended=images")) {
        QJsonArray seasons = result.array();
        for(int i=0; i < seasons.count(); i++) {
            SeasonInfo *s = SeasonInfo::fromJsonObject(traktId(), seasons.at(i).toObject(), this);
            m_seasons << s;
        }
        reloadInfo(QString(SEASON_EXTENDED_URL_SUFFIX).arg(traktId()));
        return true;
    } else if (url.toString().endsWith("/seasons?extended=full")) {
        QJsonArray seasons = result.array();
        if (seasons.size() != m_seasons.size()) {
            qWarning() << "Fail to dowload seasos.";
            Q_EMIT seasonsDowloaded(true);
        } else {
            for(int i=0; i < seasons.count(); i++) {
                m_seasons[i]->setExtendedData(seasons.at(i).toObject());
            }
        }
        Q_EMIT seasonsChanged();
        Q_EMIT seasonsDowloaded(false);
        return true;
    } else if (url.toString().endsWith("extended=images")) {
        setField(ShowInfo::FieldYear, show["year"].toInt());
        QJsonObject images = show["images"].toObject();

        QVariantMap oldImages = fanArt();
        QVariantMap newImages= parseImageEntry(images["fanart"].toObject());
        updateNonLocalImages(&oldImages, newImages);
        setField(ShowInfo::FieldFanArt, oldImages);

        oldImages = poster();
        newImages = parseImageEntry(images["poster"].toObject());
        updateNonLocalImages(&oldImages, newImages);
        setField(ShowInfo::FieldPoster, oldImages);

        oldImages = logo();
        newImages = parseImageEntry(images["logo"].toObject());
        updateNonLocalImages(&oldImages, newImages);
        setField(ShowInfo::FieldLogo, oldImages);

        oldImages = banner();
        newImages = parseImageEntry(images["banner"].toObject());
        updateNonLocalImages(&oldImages, newImages);
        setField(ShowInfo::FieldBanner, oldImages);

        oldImages = clearArt();
        newImages = parseImageEntry(images["clearart"].toObject());
        updateNonLocalImages(&oldImages, newImages);
        setField(ShowInfo::FieldClearArt, oldImages);

        QJsonObject thumb = images.value("thumb").toObject();
        QVariant oldThumb = value(ShowInfo::FieldThumb);
        QVariant newThumb = QUrl(thumb.value("full").toString());
        updateNonLocalImage(&oldThumb, newThumb);
        setField(ShowInfo::FieldThumb, oldThumb);

        if (MediaInfo::parseResult(url, result)) {
            reloadInfo(QString(EXTEND_INFO_URL_SUFFIX).arg(traktId()));
            return true;
        }
    } else if (url.toString().endsWith("extended=full")) {
        setField(ShowInfo::FieldOverview, show["overview"].toString());
        setField(ShowInfo::FieldFirstAired, show.value("first_aired").toString());
        setField(ShowInfo::FieldAirs, show["airs"].toObject().toVariantMap());
        setField(ShowInfo::FieldNetwork, show["network"].toString());
        setField(ShowInfo::FieldHomePage, QUrl(show["homepage"].toString()));
        setField(ShowInfo::FieldRating, show["rating"].toDouble());
        setField(ShowInfo::FieldStatus, show["status"].toString());

        QJsonArray genres = show["genres"].toArray();
        QStringList genresList;
        Q_FOREACH(const QVariant &v, genres.toVariantList()) {
            genresList << v.toString();
        }
        setField(ShowInfo::FieldGenres, genresList);
        setField(ShowInfo::FieldAiredEpisodes, show["aired_episodes"].toInt());

        downloadAboutToFinish(false);
        return parseExtendedInfo(url, result);;
    }

    downloadAboutToFinish(true);
    return false;
}

QUrl ShowInfo::getSmallestImage(const QVariantMap &urls) const
{
    QUrl url = urls.value("thumb").toUrl();
    if (url.isValid()) {
        return url;
    } else {
        url = urls.value("medium").toUrl();
        if (url.isValid()) {
            return url;
        }
        url = urls.value("full").toUrl();
        if (url.isValid()) {
            return url;
        }
    }
    return QUrl();
}

void ShowInfo::loadSeasons()
{
    if (!m_seasons.isEmpty()) {
        qDeleteAll(m_seasons);
        m_seasons.clear();
    }

    m_seasons = SeasonInfo::all(traktId(), this);
    Q_EMIT seasonsChanged();
}

QList<QUrl> ShowInfo::filesForDownload() const
{
    QList<QUrl> urls;

    QUrl url = getSmallestImage(poster());
    if (url.isValid()) {
        urls << url;
    }

    url = thumb();
    if (url.isValid()) {
        urls << url;
    }

    return urls;
}


QString ShowInfo::staticTableName()
{
    return QStringLiteral("shows");
}

QList<ShowInfo *> ShowInfo::all(QObject *parent)
{
    QList<ShowInfo*> shows;
    QList<DBItemCache > items = Database::instance()->items(ShowInfo::staticTableName());

    Q_FOREACH(const DBItemCache &item, items) {
        shows << new ShowInfo(item, parent);
    }
    return shows;
}
