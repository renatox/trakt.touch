/*
 *  This file is part of Trakt.tv.touch
 *
 * Trakt.tv.touch App is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * Ubuntu Calendar App is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "config.h"
#include "online-shows.h"
#include "season-info.h"
#include "episode-info.h"
#include "trakt-request.h"

#include <QNetworkAccessManager>
#include <QNetworkReply>

#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>

OnlineShows::OnlineShows(QObject *parent)
    : QAbstractListModel(parent),
      m_currentRequest(0),
      m_currentDownloading(0),
      m_pageIndex(1),
      m_pageSize(10),
      m_hasMore(false),
      m_searchMode(false)
{
    loadPage(m_pageIndex, m_pageSize);
}

OnlineShows::~OnlineShows()
{
    qDeleteAll(m_shows);
    m_shows.clear();

    if (m_currentRequest) {
        delete m_currentRequest;
        m_currentRequest = 0;
    }
}

QVariant OnlineShows::data(const QModelIndex &index, int role) const
{
    if ((index.row() >= 0) && (index.row() < m_shows.size())) {
        ShowInfo *show = m_shows.at(index.row());
        switch (role) {
        case OnlineShows::RuleSaved:
            return show->saved();
        default:
            return m_shows.at(index.row())->value(role);
        }
    }
    return QVariant();
}

int OnlineShows::rowCount(const QModelIndex &) const
{
    return m_shows.count();
}

QHash<int, QByteArray> OnlineShows::roleNames() const
{
    static QHash<int, QByteArray> roles;
    if (roles.isEmpty()) {
        roles.insert(ShowInfo::FieldTraktId, "traktId");
        roles.insert(ShowInfo::FieldImdbId, "imdbId");
        roles.insert(ShowInfo::FieldTitle, "showTitle");
        roles.insert(ShowInfo::FieldYear, "year");
        roles.insert(ShowInfo::FieldBanner, "banner");
        roles.insert(ShowInfo::FieldClearArt, "clearArt");
        roles.insert(ShowInfo::FieldLogo, "logo");
        roles.insert(ShowInfo::FieldPoster, "poster");
        roles.insert(ShowInfo::FieldThumb, "thumb");
        roles.insert(ShowInfo::FieldOverview, "overview");
        roles.insert(ShowInfo::FieldFirstAired, "firstAired");
        roles.insert(ShowInfo::FieldAirs, "airs");
        roles.insert(ShowInfo::FieldNetwork, "network");
        roles.insert(ShowInfo::FieldHomePage, "homePage");
        roles.insert(ShowInfo::FieldStatus, "status");
        roles.insert(ShowInfo::FieldRating, "rating");
        roles.insert(ShowInfo::FieldAiredEpisodes, "airedEpisodes");
        roles.insert(ShowInfo::FieldGenres, "genres");
        roles.insert(OnlineShows::RuleSaved, "saved");
    }
    return roles;
}

bool OnlineShows::canFetchMore(const QModelIndex &parent) const
{
    return (m_hasMore && (m_currentRequest == 0));
}

void OnlineShows::fetchMore(const QModelIndex &parent)
{
    loadPage(m_pageIndex + 1, m_pageSize);
}

int OnlineShows::pageSize() const
{
    return m_pageSize;
}

void OnlineShows::setPageSize(int pageSize)
{
    if (m_pageSize != pageSize) {
        m_pageSize = pageSize;
        Q_EMIT pageSizeChanged();
    }
}

bool OnlineShows::loading() const
{
    return (m_currentRequest != 0);
}

bool OnlineShows::downloading() const
{
    return m_currentDownloading;
}

qreal OnlineShows::downloadingProgress() const
{
    if (!m_downloader.isNull()) {
        return m_downloader->progress();
    }
    return 0.0;
}

void OnlineShows::clear()
{
    beginResetModel();

    if (m_currentRequest) {
        delete m_currentRequest;
        m_currentRequest = 0;
    }

    m_pageIndex = 0;
    qDeleteAll(m_shows);
    m_shows.clear();

    endResetModel();
}

void OnlineShows::save(int index)
{
    if (m_currentDownloading) {
        qWarning() << "Downloading in progress";
        return;
    }
    m_currentDownloading = m_shows.at(index);
    if (m_currentDownloading) {
        Q_EMIT downloadingChanged();
        m_downloader = new ShowDownloader(m_currentDownloading, true, this);
        qDebug() << "Will save" << index << m_currentDownloading->title();
        connect(m_downloader.data(),
                SIGNAL(downloadFinished()),
                SLOT(onShowDownloaded()));
        connect(m_downloader.data(),
                SIGNAL(downloadError()),
                SLOT(onShowDownloadError()));
        connect(m_downloader.data(),
                SIGNAL(progressChanged()),
                SIGNAL(downloadingProgressChanged()));
        m_downloader->start();
    }
}

void OnlineShows::remove(int index)
{
    if (m_currentDownloading) {
        qWarning() << "Downloading in progress";
        return;
    }
    ShowInfo *s = m_shows.at(index);
    if (s) {
        s->remove();
    }
}

void OnlineShows::search(const QString &query)
{
    m_searchText = query;
    m_searchMode = !query.isEmpty();
    clear();
    loadPage(m_pageIndex, m_pageSize);
}

void OnlineShows::cancelSearch()
{
    search("");
}

void OnlineShows::loadPage(int page, int size)
{
    if (m_currentRequest) {
        qWarning() << "Load page in progress";
        return;
    }

    m_currentRequest = newTrackRequest(this);
    if (m_searchMode) {
        m_currentRequest->setSuffix(QStringLiteral("/search?query=%2&type=show").arg(m_searchText));
    } else {
        m_currentRequest->setSuffix(QStringLiteral("/shows/popular"));
    }
    m_currentRequest->setPagination(page, size);

    qDebug() << "Will request data" << m_currentRequest->url();

    connect(m_currentRequest,
            SIGNAL(finished(QUrl,QJsonDocument)),
            SLOT(onReplyFinished(QUrl,QJsonDocument)));
    connect(m_currentRequest,
            SIGNAL(error(QNetworkReply::NetworkError)),
            SLOT(onReplyError(QNetworkReply::NetworkError)));
    m_currentRequest->start();
    Q_EMIT loadingChanged();
}

uint OnlineShows::parseShowList(const QJsonDocument &jsonResponse)
{
    QJsonArray shows = jsonResponse.array();

    beginInsertRows(QModelIndex(),
                    m_shows.size(),
                    m_shows.size() + shows.size() - 1);
    Q_FOREACH (const QJsonValue &show, shows) {
        QJsonObject s = show.toObject();
        QJsonObject ids = s.value("ids").toObject();
        QString id = QString::number(ids.value("trakt").toInt());
        ShowInfo *info = new ShowInfo(id, this);
        connect(info, SIGNAL(showChanged()), SLOT(onShowChanged()));
        connect(info, SIGNAL(savedChanged()), SLOT(onShowChanged()));
        info->download(false);
        m_shows.append(info);
    }
    endInsertRows();

    return shows.size();
}

uint OnlineShows::parseSearchResult(const QJsonDocument &jsonResponse)
{
    QJsonArray results = jsonResponse.array();

    beginInsertRows(QModelIndex(),
                    m_shows.size(),
                    m_shows.size() + results.size() - 1);

    Q_FOREACH (const QJsonValue &result, results) {
        QJsonObject r = result.toObject();
        QJsonObject show = r.value("show").toObject();
        QJsonObject ids = show.value("ids").toObject();
        QString id = QString::number(ids.value("trakt").toInt());
        ShowInfo *info = new ShowInfo(id, this);
        connect(info, SIGNAL(showChanged()), SLOT(onShowChanged()));
        connect(info, SIGNAL(savedChanged()), SLOT(onShowChanged()));
        info->download(false);
        m_shows.append(info);
    }
    endInsertRows();

    return results.size();
}

void OnlineShows::onReplyFinished(const QUrl &url, const QJsonDocument &data)
{
    int count = 0;
    if (m_searchMode) {
        count = parseSearchResult(data);
    } else {
        count = parseShowList(data);
    }

    m_hasMore = (count >= m_pageSize);
    m_currentRequest->deleteLater();
    m_currentRequest = 0;
    m_pageIndex++;

    Q_EMIT loadingChanged();
}

void OnlineShows::onReplyError(QNetworkReply::NetworkError error)
{
    qWarning() << "Fail to retrieve show list" << error;
    m_currentRequest->deleteLater();
    m_currentRequest = 0;
}

void OnlineShows::onShowChanged()
{
    ShowInfo *info = qobject_cast<ShowInfo*>(QObject::sender());
    int row = m_shows.indexOf(info);
    Q_EMIT dataChanged(index(row), index(row));
}

void OnlineShows::onShowDownloaded()
{
    qDebug() << "Show downloaded" << m_currentDownloading->title();
    m_currentDownloading->saveAll();
    m_currentDownloading = 0;
    Q_EMIT downloadingChanged();
}

void OnlineShows::onShowDownloadError()
{
    m_currentDownloading = 0;
    Q_EMIT downloadingChanged();
}
