/*
 *
 * This file is part of Trakt.tv.touch
 *
 * Trakt.tv.touch App is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * Ubuntu Calendar App is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "config.h"
#include "calendar-model.h"
#include "episode-info.h"
#include "show-info.h"
#include "season-info.h"
#include "database.h"

#define SHOW_ROLE_MASK(x)    (x+10000)
#define SHOW_ROLE_UNMASK(x)  (x-10000)
#define SEASON_ROLE_MASK(x)    (x+20000)
#define SEASON_ROLE_UNMASK(x)  (x-20000)


#define LIST_SECTION_ROLE     30000

#define _(X)    (X)

struct EpisodeLess
{
    bool operator()(const EpisodeInfo *a, const EpisodeInfo *b) const
    {
        if (a->firstAired() == b->firstAired()) {
            return a->number() < b->number();
        } else {
            return a->firstAired() < b->firstAired();
        }
  }
};

CalendarModel::CalendarModel(QObject *parent)
    : QAbstractListModel(parent)
{
    uint timeT = QDateTime::currentDateTime().toTime_t();

    // three months
    QDateTime startTime = QDateTime::fromTime_t(timeT - 7776000);
    QDateTime endTime = QDateTime::fromTime_t(timeT + 7776000);

    m_startDate = startTime.date();
    m_endDate = endTime.date();

    loadData();

    connect(Database::instance(),
            SIGNAL(itemAdded(QString,QString)),
            SLOT(onItemAdded(QString,QString)),
            Qt::QueuedConnection);
    connect(Database::instance(),
            SIGNAL(itemRemoved(QString,QString)),
            SLOT(onItemRemoved(QString,QString)),
            Qt::QueuedConnection);
}

CalendarModel::~CalendarModel()
{
    clear();
}

QVariant CalendarModel::data(const QModelIndex &index, int role) const
{
    if ((index.row() >= 0) && (index.row() < m_episodes.size())) {
        EpisodeInfo *episode = m_episodes.at(index.row());
        if (role == CalendarModel::RoleSection) {
            QDateTime airedDate = episode->firstAired().toLocalTime();
            return QVariant(dateSection(airedDate));
        } else  if (role < SHOW_ROLE_MASK(ShowInfo::FieldTraktId)) {
            if (episode) {
                return episode->value(role);
            }
        } else if (role < SEASON_ROLE_MASK(SeasonInfo::FieldNumber)){
            ShowInfo *show = m_shows.value(episode->showId());
            if (show) {
                return show->value(SHOW_ROLE_UNMASK(role));
            }
        } else {
            ShowInfo *show = m_shows.value(episode->showId());
            Q_FOREACH(SeasonInfo *s, show->seasons()) {
                if (s->number() == episode->season()) {
                    return s->value(SEASON_ROLE_UNMASK(role));
                }
            }
        }
    }
    return QVariant();
}

int CalendarModel::rowCount(const QModelIndex &) const
{
    return m_episodes.count();
}

int CalendarModel::mostRecent() const
{
    QDateTime current = QDateTime::currentDateTime();
    for(int i=0; i < m_episodes.size(); i++) {
        EpisodeInfo *e = m_episodes[i];
        if (e->firstAired().isValid() &&
            e->firstAired() > current) {
            return i;
        }
    }

    return m_episodes.size() - 1;
}

void CalendarModel::setWatched(int index, bool value)
{
    EpisodeInfo *e = m_episodes.value(index);
    if (e) {
        e->setWatched(value);
        e->save();
    }
}

EpisodeInfo *CalendarModel::episode(int index) const
{
    return m_episodes.value(index, 0);
}

QDate CalendarModel::startDate() const
{
    return m_startDate;
}

QDate CalendarModel::endDate() const
{
    return m_endDate;
}

void CalendarModel::setStartDate(const QDate &date)
{
    if (m_startDate != date) {
        m_startDate = date;
        Q_EMIT startDateChanged();
        loadData();
    }
}

void CalendarModel::setEndDate(const QDate &date)
{
    if (m_endDate != date) {
        m_endDate = date;
        Q_EMIT endDateChanged();
        loadData();
    }
}

void CalendarModel::loadData()
{
    beginResetModel();
    clear();
    // load shows and episodes
    Q_FOREACH(ShowInfo *show, ShowInfo::all(this)) {
        m_shows.insert(show->traktId(), show);
        Q_FOREACH(SeasonInfo *season, show->seasons()) {
            connect(season,
                    SIGNAL(seasonChanged()),
                    SLOT(onSeasonChanged()));
            Q_FOREACH(EpisodeInfo *episode, season->episodes()) {
                connect(episode,
                        SIGNAL(episodeChanged()),
                        SLOT(onEpisodeChanged()));
                m_episodes << episode;
            }
        }
    }
    qSort(m_episodes.begin(), m_episodes.end(), EpisodeLess());
    endResetModel();
}

void CalendarModel::clear()
{
    qDeleteAll(m_shows.values());
    m_shows.clear();
    m_episodes.clear();
}

QString CalendarModel::dateSection(const QDateTime &date) const
{
    if (!date.isValid()) {
        return _("Unknown");
    }

    QDate today = QDate::currentDate();
    qint32 yDiff = date.date().year() - today.year();
    qint32 diff = today.daysTo(date.date());
    qint32 wDiff = (date.date().weekNumber() - today.weekNumber()) + (yDiff * 52);
    qint32 mDiff = date.date().month() - today.month() + (yDiff * 12);

    switch(diff)
    {
    case -2:
        return _("Two days ago");
    case -1:
        return _("Yesterday");
    case 0:
        return _("Today");
    case 1:
        return _("Tomorrow");
    case 2:
        return _("In two days");
    case 3:
        if (wDiff < 1) {
            return _("In three days");
        }
    case 4:
        if (wDiff < 1) {
            return _("In four days");
        }
    case 5:
        if (wDiff < 1) {
            return _("In five days");
        }
    default:
        if (wDiff == 0) {
            if (diff < 0) {
                return _("Few days ago");
            } else if (diff > 0) {
                return _("This week");
            }
        } else if (wDiff == -1) {
            return _("Last week");
        } else if (wDiff == 1) {
            return _("Next week");
        } else if (mDiff == -1) {
            return _("Last month");
        } else if (mDiff == 1) {
            return _("Next month");
        } else if (mDiff == 0) {
            return _("This month");
        }
        return date.date().toString("dd, MMMM");
    }
}


QHash<int, QByteArray> CalendarModel::roleNames() const
{
    static QHash<int, QByteArray> roles;
    if (roles.isEmpty()) {
        roles.insert(EpisodeInfo::FieldTraktId, "traktId");
        roles.insert(EpisodeInfo::FieldImdbId, "imdbId");
        roles.insert(EpisodeInfo::FieldTitle, "title");
        roles.insert(EpisodeInfo::FieldShowId, "showId");
        roles.insert(EpisodeInfo::FieldSeason, "season");
        roles.insert(EpisodeInfo::FieldNumber, "number");
        roles.insert(EpisodeInfo::FieldScreenshot, "screenshot");
        roles.insert(EpisodeInfo::FieldOverview, "overview");
        roles.insert(EpisodeInfo::FieldRating, "rating");
        roles.insert(EpisodeInfo::FieldVotes, "votes");
        roles.insert(EpisodeInfo::FieldFirstAired, "firstAired");
        roles.insert(EpisodeInfo::FieldWatched, "watched");

        roles.insert(SHOW_ROLE_MASK(ShowInfo::FieldTraktId), "showTraktId");
        roles.insert(SHOW_ROLE_MASK(ShowInfo::FieldImdbId), "showImdbId");
        roles.insert(SHOW_ROLE_MASK(ShowInfo::FieldTitle), "showTitle");
        roles.insert(SHOW_ROLE_MASK(ShowInfo::FieldYear), "showYear");
        roles.insert(SHOW_ROLE_MASK(ShowInfo::FieldBanner), "showBanner");
        roles.insert(SHOW_ROLE_MASK(ShowInfo::FieldClearArt), "showClearArt");
        roles.insert(SHOW_ROLE_MASK(ShowInfo::FieldLogo), "showLogo");
        roles.insert(SHOW_ROLE_MASK(ShowInfo::FieldPoster), "showPoster");
        roles.insert(SHOW_ROLE_MASK(ShowInfo::FieldThumb), "showThumb");
        roles.insert(SHOW_ROLE_MASK(ShowInfo::FieldOverview), "showOverview");
        roles.insert(SHOW_ROLE_MASK(ShowInfo::FieldFirstAired), "showFirstAired");
        roles.insert(SHOW_ROLE_MASK(ShowInfo::FieldAirs), "showAirs");
        roles.insert(SHOW_ROLE_MASK(ShowInfo::FieldNetwork), "showNetwork");
        roles.insert(SHOW_ROLE_MASK(ShowInfo::FieldHomePage), "showHomePage");
        roles.insert(SHOW_ROLE_MASK(ShowInfo::FieldStatus), "showStatus");
        roles.insert(SHOW_ROLE_MASK(ShowInfo::FieldRating), "showRating");
        roles.insert(SHOW_ROLE_MASK(ShowInfo::FieldAiredEpisodes), "showAiredEpisodes");
        roles.insert(SHOW_ROLE_MASK(ShowInfo::FieldGenres), "showGenres");

        roles.insert(SEASON_ROLE_MASK(SeasonInfo::FieldNumber), "seasonNumber");
        roles.insert(SEASON_ROLE_MASK(SeasonInfo::FieldPoster), "seasonPoster");
        roles.insert(SEASON_ROLE_MASK(SeasonInfo::FieldThumb), "seasonThumb");
        roles.insert(SEASON_ROLE_MASK(SeasonInfo::FieldAiredEpisodes), "seasonAiredEpisodes");
        roles.insert(SEASON_ROLE_MASK(SeasonInfo::FieldEpisodeCount), "seasonEpisodeCount");
        roles.insert(SEASON_ROLE_MASK(SeasonInfo::FieldOverview), "seasonOverview");
        roles.insert(SEASON_ROLE_MASK(SeasonInfo::FieldRating), "seasonRating");
        roles.insert(SEASON_ROLE_MASK(SeasonInfo::FieldWatchedCount), "seasonWatchedCount");

        roles.insert(CalendarModel::RoleSection, "section");
    }
    return roles;
}

void CalendarModel::onEpisodeChanged()
{
    EpisodeInfo *info = qobject_cast<EpisodeInfo*>(QObject::sender());
    int row = m_episodes.indexOf(info);
    Q_EMIT dataChanged(index(row), index(row));
}

void CalendarModel::onSeasonChanged()
{
    SeasonInfo *season = qobject_cast<SeasonInfo*>(QObject::sender());
    qDebug() << "SEASON CHANGED" << season->showId() << season->number();
    //TODO: Optimize this
    for(int i=0; i < m_episodes.size(); i++) {
        EpisodeInfo *e = m_episodes.at(i);
        if ((e->showId() == season->showId()) &&
            (e->season() == season->number())) {
            qDebug() << "EPISODE CHANGED" << i;
            Q_EMIT dataChanged(index(i), index(i));
        } else {
            qWarning() << "EPISODE NOT IN SEASON" << e->showId() << e->season();
        }
    }
}

void CalendarModel::onItemAdded(const QString &table, const QString &id)
{
    if (table == EpisodeInfo::staticTableName()) {
        QStringList ids = id.split(":");
        if (ids.count() != 3) {
            qWarning() << "Invalid episode id" << id;
            return;
        }
        EpisodeInfo *newEpisode = new EpisodeInfo(ids[0],
                                                  ids[1].toUInt(),
                                                  ids[2].toUInt(),
                                                  this);
        ShowInfo *newShow = m_shows.value(newEpisode->showId());
        if (!newShow) {
            qDebug() << "REGISTER NEW SHOW";
            newShow = new ShowInfo(newEpisode->showId(), this);
            Q_FOREACH(SeasonInfo *s, newShow->seasons()) {
                connect(s,
                        SIGNAL(seasonChanged()),
                        SLOT(onSeasonChanged()));
            }
            m_shows.insert(newShow->id(), newShow);
        }

        QList<EpisodeInfo*> newList = m_episodes;
        newList << newEpisode;
        qSort(newList.begin(), newList.end(), EpisodeLess());

        int index = newList.indexOf(newEpisode);

        beginInsertRows(QModelIndex(), index, index);
        m_episodes.insert(index, newEpisode);
        endInsertRows();

        connect(newEpisode,
                SIGNAL(episodeChanged()),
                SLOT(onEpisodeChanged()));
    }
}

void CalendarModel::onItemRemoved(const QString &table, const QString &id)
{
    if (table == EpisodeInfo::staticTableName()) {
        QStringList ids = id.split(":");
        if (ids.count() != 3) {
            qWarning() << "Invalid episode id" << id;
            return;
        }
        int toRemove = -1;
        for(int i=0; i < m_episodes.size(); i++) {
            if (m_episodes[i]->id() == id) {
                toRemove = i;
                break;
            }
        }
        if (toRemove != -1) {
            beginRemoveRows(QModelIndex(), toRemove,toRemove);
            delete m_episodes.takeAt(toRemove);
            endRemoveRows();
        }
    } else if (table == ShowInfo::staticTableName()) {
        if (m_shows.contains(id)) {
            delete m_shows.take(id);
        }
    }
}
