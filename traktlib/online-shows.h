/*
 *  This file is part of Trakt.tv.touch
 *
 * Trakt.tv.touch App is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * Ubuntu Calendar App is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __ONLINE_SHOWS_H__
#define __ONLINE_SHOWS_H__

#include <QObject>
#include <QAbstractListModel>
#include <QUrl>
#include <QJsonDocument>

#include "show-info.h"
#include "show-downloader.h"

class TraktRequest;

class OnlineShows : public QAbstractListModel
{
    Q_OBJECT
    Q_PROPERTY(int pageSize READ pageSize WRITE setPageSize NOTIFY pageSizeChanged)
    Q_PROPERTY(bool loading READ loading NOTIFY loadingChanged)
    Q_PROPERTY(bool downloading READ downloading NOTIFY downloadingChanged)
    Q_PROPERTY(qreal downloadingProgress READ downloadingProgress NOTIFY downloadingProgressChanged)

public:
    enum ExtraRules {
        RuleSaved = ShowInfo::FieldLast + 1
    };

    OnlineShows(QObject *parent = 0);
    ~OnlineShows();

    QVariant data(const QModelIndex &index, int role) const;
    QHash<int, QByteArray> roleNames() const;
    int rowCount(const QModelIndex&) const;

    //progressive load
    bool canFetchMore(const QModelIndex & parent) const;
    void fetchMore(const QModelIndex & parent);

    int pageSize() const;
    void setPageSize(int pageSize);

    bool loading() const;
    bool downloading() const;
    qreal downloadingProgress() const;

    void clear();

    Q_INVOKABLE void save(int idex);
    Q_INVOKABLE void remove(int index);
    Q_INVOKABLE void search(const QString &query);
    Q_INVOKABLE void cancelSearch();

Q_SIGNALS:
    void pageSizeChanged();
    void loadingChanged();
    void downloadingChanged();
    void downloadingProgressChanged();

private Q_SLOTS:
    void onReplyFinished(const QUrl &url, const QJsonDocument &data);
    void onReplyError(QNetworkReply::NetworkError);
    void onShowChanged();
    void onShowDownloaded();
    void onShowDownloadError();

private:
    TraktRequest *m_currentRequest;
    ShowInfo *m_currentDownloading;
    QPointer<ShowDownloader> m_downloader;
    int m_pageSize;
    int m_pageIndex;
    bool m_hasMore;
    QString m_searchText;
    QList<ShowInfo*> m_shows;
    bool m_searchMode;

    void loadPage(int page, int size);
    uint parseShowList(const QJsonDocument &jsonResponse);
    uint parseSearchResult(const QJsonDocument &jsonResponse);
};



#endif
