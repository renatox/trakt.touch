/*
 *  This file is part of Trakt.tv.touch
 *
 * Trakt.tv.touch App is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * Ubuntu Calendar App is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __OFFLINE_SHOWS_H__
#define __OFFLINE_SHOWS_H__

#include <QObject>
#include <QAbstractListModel>
#include <QNetworkReply>

#include "show-info.h"

class OfflineShows : public QAbstractListModel
{
    Q_OBJECT

public:
    OfflineShows(QObject *parent = 0);
    ~OfflineShows();

    QVariant data(const QModelIndex &index, int role) const;
    QHash<int, QByteArray> roleNames() const;
    int rowCount(const QModelIndex&) const;

    Q_INVOKABLE void save(int index);
    Q_INVOKABLE ShowInfo *show(int index);

private Q_SLOTS:
    void onShowChanged();
    void onItemAdded(const QString &table, const QString &id);
    void onItemRemoved(const QString &table, const QString &id);

private:
    QString m_searchText;
    QList<ShowInfo*> m_shows;

    void load();
};



#endif
