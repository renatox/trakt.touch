/*
 *  This file is part of Trakt.tv.touch
 *
 * Trakt.tv.touch App is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * Ubuntu Calendar App is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "config.h"
#include "season-info.h"
#include "episode-info.h"

#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>

#define SHOW_ID_FIELD_NAME           "show_id"
#define EPISODE_LIST_URL_SUFFIX      "shows/%1/seasons/%2"

static bool SeasonLessThan(const SeasonInfo *s1, const SeasonInfo *s2)
{
    if (s1->showId() != s2->showId()) {
        return s1->showId() < s2->showId();
    } else {
        return s1->number() < s2->number();
    }
}


static QString buildId(const QString &showId, uint number)
{
    return QString("%1:%2").arg(showId).arg(number);
}

SeasonInfo::SeasonInfo(const QString &showId, uint number, QObject *parent)
    : MediaInfo(staticTableName(), buildId(showId, number), parent),
      m_watched(0)
{
    setField(SeasonInfo::FieldShowId, showId);
    setField(SeasonInfo::FieldNumber, number);
    loadEpisodes();
    connect(this, SIGNAL(infoChanged()), SIGNAL(seasonChanged()));
}

SeasonInfo::SeasonInfo(const DBItemCache &item, QObject *parent)
    : MediaInfo(item, parent),
      m_watched(0)
{
    loadEpisodes();
    connect(this, SIGNAL(infoChanged()), SIGNAL(seasonChanged()));
}

SeasonInfo *SeasonInfo::fromJsonObject(const QString &showId, const QJsonObject &data, QObject *parent)
{
    int number = data.value("number").toInt();
    SeasonInfo *season = new SeasonInfo(showId, number, parent);
    season->setImagesData(data);
    return season;
}

SeasonInfo::~SeasonInfo()
{
    qDeleteAll(m_episodes);
    m_episodes.clear();
}

QString SeasonInfo::showId() const
{
    return value(SeasonInfo::FieldShowId).toString();
}

uint SeasonInfo::number() const
{
    return value(SeasonInfo::FieldNumber).toUInt();
}

QVariantMap SeasonInfo::poster() const
{
    return value(SeasonInfo::FieldPoster).toMap();
}

QUrl SeasonInfo::thumb() const
{
    return value(SeasonInfo::FieldThumb).toUrl();
}

uint SeasonInfo::airedEpisodes() const
{
    return value(SeasonInfo::FieldAiredEpisodes).toUInt();
}

uint SeasonInfo::episodeCount() const
{
    return value(SeasonInfo::FieldEpisodeCount).toUInt();
}

QString SeasonInfo::overview() const
{
    return value(SeasonInfo::FieldOverview).toString();
}

uint SeasonInfo::watchedEpisodes() const
{
    return m_watched;
}

QString SeasonInfo::fieldName(uint field) const
{
    switch(field) {
    case FieldShowId:
        return QStringLiteral(SHOW_ID_FIELD_NAME);
    case FieldNumber:
        return QStringLiteral("number");
    case FieldPoster:
        return QStringLiteral("poster");
    case FieldThumb:
        return QStringLiteral("thumb");
    case FieldAiredEpisodes:
        return QStringLiteral("aired_episodes");
    case FieldEpisodeCount:
        return QStringLiteral("episode_count");
    case FieldOverview:
        return QStringLiteral("overview");
    case FieldRating:
        return QStringLiteral("rating");
    case FieldWatchedCount:
        return QStringLiteral("watched_episodes");
    default:
        return MediaInfo::fieldName(field);
    }
}

void SeasonInfo::setImagesData(const QJsonObject &season)
{
    setField(SeasonInfo::FieldNumber, season.value("number").toInt());
    QJsonObject images = season.value("images").toObject();

    QVariantMap oldPoster = poster();
    QVariantMap newPoster = parseImageEntry(images.value("poster").toObject());
    updateNonLocalImages(&oldPoster, newPoster);
    setField(SeasonInfo::FieldPoster, oldPoster);

    QJsonObject thumb = images.value("thumb").toObject();
    QString thumbUrl;
    if (thumb.contains("small")) {
        thumbUrl = thumb.value("small").toString();
    } else if (thumb.contains("medium")) {
        thumbUrl = thumb.value("medium").toString();
    } else if (thumb.contains("full")) {
        thumbUrl = thumb.value("full").toString();
    }

    QVariant oldThumb = value(SeasonInfo::FieldThumb);
    QVariant newThumb = QUrl(thumbUrl);
    updateNonLocalImage(&oldThumb, newThumb);
    setField(SeasonInfo::FieldThumb, oldThumb);

    Q_EMIT seasonChanged();
}

void SeasonInfo::setExtendedData(const QJsonObject &season)
{
    setField(SeasonInfo::FieldAiredEpisodes,
             season.value("aired_episodes").toInt());
    setField(SeasonInfo::FieldEpisodeCount,
             season.value("episode_count").toInt());
    setField(SeasonInfo::FieldRating,
             season.value("rating").toDouble());
    setField(SeasonInfo::FieldOverview,
             season.value("overview").toString());

    Q_EMIT seasonChanged();
}

void SeasonInfo::loadEpisodes()
{
    if (m_episodes.isEmpty()) {
        m_watched = 0;
        m_episodes = EpisodeInfo::all(showId(), number(), this);
        Q_FOREACH(const EpisodeInfo *e, m_episodes) {
            connect(e, SIGNAL(episodeChanged()), this, SLOT(onEpisodeChanged()));
            if (e->watched()) {
                m_watched++;
            }
        }
    }
}

void SeasonInfo::download(bool force)
{
    if ((force || m_episodes.isEmpty()) &&
        downloadAboutToStart()) {
        qDeleteAll(m_episodes);
        m_episodes.clear();
        m_watched = 0;
        reloadInfo(QString(EPISODE_LIST_URL_SUFFIX)
                   .arg(showId())
                   .arg(number()));
    } else {
        downloadAboutToFinish(false);
    }
}

bool SeasonInfo::parseResult(const QUrl&, const QJsonDocument &result)
{
    QJsonArray episodes = result.array();
    for(int i=0; i < episodes.count(); i++) {
        int eNumber = episodes.at(i).toObject().value("number").toInt();
        EpisodeInfo *e = new EpisodeInfo(showId(), number(), eNumber, this);
        connect(e, SIGNAL(episodeChanged()), this, SLOT(onEpisodeChanged()));
        if (e->watched()) {
            m_watched++;
        }
        m_episodes << e;
    }
    Q_EMIT seasonChanged();
    downloadAboutToFinish(false);
    return true;
}

QList<EpisodeInfo *> SeasonInfo::episodes()
{
    loadEpisodes();
    return m_episodes;
}

EpisodeInfo *SeasonInfo::episode(int index)
{
    return episodes().value(index);
}

QString SeasonInfo::staticTableName()
{
    return QStringLiteral("seasons");
}

QList<SeasonInfo *> SeasonInfo::all(const QString &showId, QObject *parent)
{
    QList<SeasonInfo *> result;
    QString where;
    if (!showId.isEmpty()) {
        where = QString("%1 = '%2'").arg(SHOW_ID_FIELD_NAME).arg(showId);
    }
    QList<DBItemCache> items = Database::instance()->items(staticTableName(), where);
    Q_FOREACH(const DBItemCache &cache, items) {
        result << new SeasonInfo(cache, parent);
    }
    qSort(result.begin(), result.end(), SeasonLessThan);
    return result;
}

void SeasonInfo::onEpisodeChanged()
{
    uint watched = 0;
    Q_FOREACH(const EpisodeInfo *e, m_episodes) {
        if (e->watched()) {
            watched++;
        }
    }
    if (watched != m_watched) {
        m_watched = watched;
        Q_EMIT seasonChanged();
    }
}
