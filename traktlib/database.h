/*
 *
 * This file is part of Trakt.tv.touch
 *
 * Trakt.tv.touch App is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * Ubuntu Calendar App is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __DATABASE_H__
#define __DATABASE_H__

#include <QObject>
#include <QString>
#include <QStringList>
#include <QByteArray>
#include <QDir>
#include <QSharedPointer>

#include <QSqlDatabase>

#include "db-item.h"

typedef QSharedPointer<DBItem> DBItemCache;

class Database : public QObject
{
    Q_OBJECT
public:
    static Database *instance();
    void destroy();

    QList<DBItemCache > items(const QString &table);
    QList<DBItemCache > items(const QString &table, const QString &where);
    DBItemCache item(const QString &table, const QString &id);
    void clearCache();

    static QString fullPath();

Q_SIGNALS:
    void itemAdded(const QString &table, const QString &id);
    void itemRemoved(const QString &table, const QString &id);

private:
    static Database *m_instance;
    QMap<QString, QMap<QString, DBItemCache> > m_cache;
    QSqlDatabase m_db;

    Database();
    ~Database();
    bool open();

    DBItemCache fromCache(const QString &table, const QString &id) const;
    void addToCache(const DBItemCache &item);
};

#endif

