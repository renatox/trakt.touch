/*
 *
 * This file is part of Trakt.tv.touch
 *
 * Trakt.tv.touch App is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * Ubuntu Calendar App is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "db-item.h"
#include "database.h"

#include <QDebug>

#include <QSqlQuery>
#include <QSqlRecord>
#include <QSqlField>
#include <QSqlError>

DBItem::DBItem(const QSqlDatabase &db, const QString &table, const QString &id, QObject *parent)
    : QObject(parent),
      m_table(table),
      m_id(id),
      m_dirty(false),
      m_database(db)
{
    loadData();
}

DBItem::DBItem(const QSqlDatabase &db, const QString &table, const QSqlRecord &record, QObject *parent)
    : QObject(parent),
      m_table(table),
      m_dirty(false),
      m_database(db)
{
    loadData(record);
}

DBItem::~DBItem()
{
}

QString DBItem::id() const
{
    return m_id;
}

void DBItem::setData(const QString &field, const QVariant &value)
{
    QVariant oldValue = m_fields.value(field);
    if (oldValue != value) {
        m_dirty = true;
        m_fields.insert(field, value);
        Q_EMIT changed(m_table, m_id, field, value);
    }
}

QVariant DBItem::data(const QString &field) const
{
    return m_fields.value(field);
}

bool DBItem::hasField(const QString &field) const
{
    return m_fields.contains(field);
}

bool DBItem::save()
{
    if (!m_dirty) {
        return true;
    }

    if (!m_database.tables().contains(m_table)) {
        createTable(m_table, m_fields);
    }

    QStringList fields = m_fields.keys();
    QStringList valueNames;
    Q_FOREACH(const QString &key, fields){
        valueNames << QString(":%1").arg(key);
    }
    QString querySQL;

    // check if exits
    bool iExists = exists();
    if (iExists) {
        //update
        QStringList values;
        Q_FOREACH(const QString &key, m_fields.keys()){
            values << QString("%1 = :%1").arg(key);
        }

        querySQL = QString("UPDATE %1 SET %2 WHERE id = :id;")
                .arg(m_table)
                .arg(values.join(','));
    } else {
        // insert
        querySQL = QString("INSERT INTO %1 (%2) VALUES (%3);")
                .arg(m_table)
                .arg(fields.join(',') + QStringLiteral(", id"))
                .arg(valueNames.join(',') + QStringLiteral(", :id"));
    }

    QSqlQuery query(m_database);
    query.prepare(querySQL);

    query.bindValue(":id", m_id);
    querySQL.replace(":id", m_id);
    for(int i=0; i < fields.size(); i++) {
        querySQL.replace(valueNames[i], fieldToSQLData(m_fields.value(fields[i])).toString());
        query.bindValue(valueNames[i],
                        fieldToSQLData(m_fields.value(fields[i])));
    }

    if (!query.exec()) {
        qWarning() << "Fail to save item" <<  query.executedQuery() << query.lastError();
        return false;
    }
    m_dirty = false;
    if (!iExists) {
        Q_EMIT Database::instance()->itemAdded(m_table, m_id);
    }
    return true;
}

bool DBItem::remove()
{
    QString querySQL;
    querySQL = QString("DELETE FROM %1 WHERE id = :id;")
            .arg(m_table);

    QSqlQuery query(m_database);
    query.prepare(querySQL);
    query.bindValue(":id", m_id);
    if (query.exec()) {
        m_dirty = true;
        Q_EMIT Database::instance()->itemRemoved(m_table, m_id);
        return true;
    } else {
        qWarning() << "Fail to remove item" << m_table << m_id << query.lastError();
        return false;
    }
}

bool DBItem::isDirty() const
{
    return m_dirty;
}

QStringList DBItem::fieldNames() const
{
    return m_fields.keys();
}

QString DBItem::table() const
{
    return m_table;
}

void DBItem::loadData()
{
    if (!m_database.tables().contains(m_table)) {
        return;
    }
    QString querySQL = QString("SELECT * FROM %1 WHERE id = :id")
            .arg(m_table);
    QSqlQuery query(m_database);
    query.prepare(querySQL);
    query.bindValue(":id", m_id);
    if (!query.exec()) {
        qWarning() << "FAIL TO EXEC QUERY" << query.executedQuery()
                      << query.lastError();
        return;
    }
    if (query.next()) {
        loadData(query.record());
    }
}

void DBItem::loadData(const QSqlRecord &record)
{
    for(int i=0; i < record.count(); i++) {
        if (record.fieldName(i) == "id") {
            m_id = record.value(i).toString();
        } else {
            m_fields.insert(record.fieldName(i),
                            valueFromField(record.field(i)));
        }
    }
}

void DBItem::createTable(const QString &table, const DBItemFields &fields)
{
    if (!m_database.tables().contains(table)) {
        m_database.transaction();
        QStringList fields;
        fields << QStringLiteral("id TEXT");
        Q_FOREACH(const QString &field, m_fields.keys()) {
            fields << QString("%1 %2").arg(field).arg(typeSQLName(m_fields.value(field)));
        }

        QString querySQL = QString("CREATE TABLE %1 (%2)")
                .arg(m_table)
                .arg(fields.join(','));
        QSqlQuery query(m_database);
        query.prepare(querySQL);
        if (!query.exec()) {
            qWarning() << "Fail to create table" << query.lastError();
        }
        m_database.commit();
    }
}

QString DBItem::typeSQLName(const QVariant &v) const
{
    switch (v.type()) {
    case QMetaType::Bool:
        return QStringLiteral("NUMERIC");
    case QMetaType::Int:
    case QMetaType::UInt:
    case QMetaType::Long:
        return QStringLiteral("INTEGER");
    case QMetaType::Double:
        return QStringLiteral("REAL");
    case QMetaType::QByteArray:
    case QMetaType::QString:
    case QMetaType::QUrl:
    case QMetaType::QDate:
    case QMetaType::QDateTime:
        return QStringLiteral("TEXT");
    case QMetaType::QVariantMap:
    case QMetaType::QStringList:
        return QStringLiteral("BLOB");
    default:
        qWarning() << "Variant type no supported" << v.typeName();
        return QStringLiteral("TEXT");
    }
}

bool DBItem::exists() const
{
    if (!m_database.tables().contains(m_table)) {
        return false;
    }
    QString querySQL = QString("SELECT id FROM %1 WHERE id = :id")
            .arg(m_table);
    QSqlQuery query(m_database);
    query.prepare(querySQL);
    query.bindValue(":id", m_id);
    if (query.exec() && query.next()) {
        return (query.value("id").toString() == m_id);
    } else {
        return false;
    }
}

QVariant DBItem::fieldToSQLData(const QVariant &v)
{
    switch (v.type()) {
    case QMetaType::QVariantMap:
    case QMetaType::QStringList:
    {
        QByteArray data;
        QDataStream stream(&data, QIODevice::WriteOnly);
        stream << v;
        return QVariant(data.toBase64());
    }
    default:
        return v;
    }
}

QVariant DBItem::valueFromField(const QSqlField &f)
{
    QVariant result;

    switch (f.type()) {
    case QMetaType::QByteArray:
    {
        QByteArray decodedData = QByteArray::fromBase64(f.value().toByteArray());
        QDataStream stream(&decodedData, QIODevice::ReadOnly);
        stream >> result;
        break;
    }
    default:
        result = f.value();
    }

    return result;
}

