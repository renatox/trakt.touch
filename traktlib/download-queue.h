/*
 *
 * This file is part of Trakt.tv.touch
 *
 * Trakt.tv.touch App is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * Ubuntu Calendar App is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __DOWNLOAD_QUEUE__
#define __DOWNLOAD QUEUE__

#include <QObject>
#include <QQueue>
#include <QDir>
#include <QScopedPointer>

#include <QNetworkAccessManager>
#include <QNetworkReply>

class DownloadQueue : public QObject
{
    Q_OBJECT
public:
    static DownloadQueue *instance();
    void destroy();
    void append(const QUrl &url);
    void append(const QList<QUrl> &urls);

Q_SIGNALS:
    void fileDownloaded(const QUrl &remoteFile, const QUrl &localFile, bool sucess);

private Q_SLOTS:
    void onDownloadFinished();
    void onDownloadError(QNetworkReply::NetworkError);

private:
    static DownloadQueue *m_instance;
    QScopedPointer<QNetworkAccessManager> m_manager;
    QNetworkReply *m_currentReply;
    QDir m_basePath;
    QQueue<QUrl> m_downloadQueue;

    DownloadQueue(const QString &basePath, QObject *parent = 0);
    ~DownloadQueue();
    QTemporaryFile *localFileFromRemoteName(const QUrl &remoteFile);
    void downloadNext();
};

#endif
