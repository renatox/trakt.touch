/*
 *
 * This file is part of Trakt.tv.touch
 *
 * Trakt.tv.touch App is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * Ubuntu Calendar App is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __CALENDAR_MODEL_H__
#define __CALENDAR_MODEL_H__

#include <QObject>
#include <QDate>
#include <QAbstractListModel>
#include <QNetworkReply>

class EpisodeInfo;
class ShowInfo;

class CalendarModel : public QAbstractListModel
{
    Q_OBJECT
    Q_PROPERTY(QDate startDate READ startDate WRITE setStartDate NOTIFY startDateChanged)
    Q_PROPERTY(QDate endDate READ endDate WRITE setEndDate NOTIFY endDateChanged)
public:
    enum CalendarModelRoles {
        RoleSection = 3000
    };

    CalendarModel(QObject *parent = 0);
    ~CalendarModel();

    QVariant data(const QModelIndex &index, int role) const;
    QHash<int, QByteArray> roleNames() const;
    int rowCount(const QModelIndex&) const;

    Q_INVOKABLE int mostRecent() const;
    Q_INVOKABLE void setWatched(int index, bool value);
    Q_INVOKABLE EpisodeInfo *episode(int index) const;

    QDate startDate() const;
    QDate endDate() const;
    void setStartDate(const QDate &date);
    void setEndDate(const QDate &date);


Q_SIGNALS:
    void startDateChanged();
    void endDateChanged();

private Q_SLOTS:
    void onEpisodeChanged();
    void onSeasonChanged();
    void onItemAdded(const QString &table, const QString &id);
    void onItemRemoved(const QString &table, const QString &id);

private:
    QList<EpisodeInfo *> m_episodes;
    QMap<QString, ShowInfo *> m_shows;
    QDate m_startDate;
    QDate m_endDate;

    void loadData();
    void clear();
    QString dateSection(const QDateTime &date) const;
};



#endif
