/*
 *
 * This file is part of Trakt.tv.touch
 *
 * Trakt.tv.touch App is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * Ubuntu Calendar App is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "config.h"
#include "episode-info.h"
#include "trakt-request.h"

#include <QNetworkAccessManager>

#include <QJsonDocument>
#include <QJsonObject>

#define EXTEND_INFO_URL_SUFFIX      "/shows/%1/seasons/%2/episodes/%3?extended=images"
#define BASIC_INFO_URL_SUFFIX       "/shows/%1/seasons/%2/episodes/%3?extended=full"
#define SHOW_ID_FIELD_NAME          "show_id"
#define SEASON_NUMBER_FIELD_NAME    "season"

static QString buildId(const QString &showId, uint season, uint number)
{
    return QString("%1:%2:%3").arg(showId).arg(season).arg(number);
}

EpisodeInfo::EpisodeInfo(const QString &showId, uint season, uint number, QObject *parent)
    : MediaInfo(tableName(), buildId(showId, season, number), parent)
{
    setField(EpisodeInfo::FieldShowId, showId);
    setField(EpisodeInfo::FieldSeason, season);
    setField(EpisodeInfo::FieldNumber, number);
    connect(this, SIGNAL(infoChanged()), SIGNAL(episodeChanged()));
}

EpisodeInfo::EpisodeInfo(const DBItemCache &cache, QObject *parent)
    : MediaInfo(cache, parent)
{
    connect(this, SIGNAL(infoChanged()), SIGNAL(episodeChanged()));
}

EpisodeInfo::~EpisodeInfo()
{
}

QString EpisodeInfo::showId() const
{
    return MediaInfo::value(EpisodeInfo::FieldShowId).toString();
}

uint EpisodeInfo::season() const
{
    return MediaInfo::value(EpisodeInfo::FieldSeason).toUInt();
}

uint EpisodeInfo::number() const
{
    return MediaInfo::value(EpisodeInfo::FieldNumber).toUInt();
}

QString EpisodeInfo::overview() const
{
    return MediaInfo::value(EpisodeInfo::FieldOverview).toString();
}

double EpisodeInfo::rating() const
{
    return MediaInfo::value(EpisodeInfo::FieldRating).toDouble();
}

uint EpisodeInfo::votes() const
{
    return MediaInfo::value(EpisodeInfo::FieldVotes).toUInt();
}

QVariantMap EpisodeInfo::screenshot() const
{
    return MediaInfo::value(EpisodeInfo::FieldScreenshot).toMap();
}

QDateTime EpisodeInfo::firstAired() const
{
    return QDateTime::fromString(MediaInfo::value(EpisodeInfo::FieldFirstAired).toString(), Qt::ISODate);
}

bool EpisodeInfo::watched() const
{
    return MediaInfo::value(EpisodeInfo::FieldWatched).toBool();
}

void EpisodeInfo::setWatched(bool w)
{
    if (watched() != w) {
        setField(EpisodeInfo::FieldWatched, w);
    }
}

QString EpisodeInfo::fieldName(uint field) const
{
    switch(field) {
    case FieldShowId:
        return QStringLiteral(SHOW_ID_FIELD_NAME);
    case FieldSeason:
        return QStringLiteral(SEASON_NUMBER_FIELD_NAME);
    case FieldNumber:
        return QStringLiteral("number");
    case FieldScreenshot:
        return QStringLiteral("screenshot");
    case FieldOverview:
        return QStringLiteral("overview");
    case FieldRating:
        return QStringLiteral("rating");
    case FieldVotes:
        return QStringLiteral("votes");
    case FieldFirstAired:
        return QStringLiteral("first_aired");
    case FieldWatched:
        return QStringLiteral("watched");
    default:
        return MediaInfo::fieldName(field);
    }
}

QVariant EpisodeInfo::value(uint field) const
{
    switch (field) {
    case EpisodeInfo::FieldFirstAired:
        return firstAired();
        break;
    default:
        return MediaInfo::value(field);
        break;
    }
}

void EpisodeInfo::save()
{
    if (!hasField(EpisodeInfo::FieldWatched)) {
        setField(EpisodeInfo::FieldWatched, false);
    }
    if (!hasField(EpisodeInfo::FieldScreenshot)) {
        setField(EpisodeInfo::FieldScreenshot, QVariantMap());
    }
    MediaInfo::save();
}

void EpisodeInfo::download(bool force)
{

    if ((force || !lastDownload().isValid()) &&
        downloadAboutToStart()) {
        QString requestSuffix = QString(BASIC_INFO_URL_SUFFIX)
                .arg(showId())
                .arg(season())
                .arg(number());
        reloadInfo(requestSuffix);
    } else {
        downloadAboutToFinish(false);
    }
}

void EpisodeInfo::downloadExtendedInfo(bool force)
{
    if (force || screenshot().isEmpty()) {
        QString requestSuffix = QString(EXTEND_INFO_URL_SUFFIX)
                .arg(showId())
                .arg(season())
                .arg(number());
        reloadInfo(requestSuffix);
    } else {
        Q_EMIT extendedInfoDownloaded(false);
    }
}

QString EpisodeInfo::staticTableName()
{
    return QStringLiteral("episodes");
}

QList<EpisodeInfo *> EpisodeInfo::all(const QString &showId, int season, QObject *parent)
{
    QList<EpisodeInfo *> result;
    QStringList where;
    if (!showId.isEmpty()) {
        where << QString("%1 = '%2'").arg(SHOW_ID_FIELD_NAME).arg(showId);
    }
    if (season >= 0) {
        where << QString("%1 = %2").arg(SEASON_NUMBER_FIELD_NAME).arg(season);
    }

    QList<DBItemCache> items = Database::instance()->items(staticTableName(), where.join(" AND "));
    Q_FOREACH(const DBItemCache &cache, items) {
        result << new EpisodeInfo(cache, parent);
    }
    return result;
}

bool EpisodeInfo::parseResult(const QUrl &url, const QJsonDocument &result)
{
    QJsonObject episode = result.object();

    if (url.toString().endsWith("?extended=full")) {
        setField(EpisodeInfo::FieldSeason, episode.value("season").toInt());
        setField(EpisodeInfo::FieldNumber, episode.value("number").toInt());
        setField(EpisodeInfo::FieldFirstAired, episode.value("first_aired").toString());
        setField(EpisodeInfo::FieldOverview, episode.value("overview").toString());
        setField(EpisodeInfo::FieldRating, episode.value("rating").toDouble());
        setField(EpisodeInfo::FieldVotes, episode.value("votes").toInt());

        parseExtendedInfo(url, result);
        Q_EMIT episodeChanged();
        downloadAboutToFinish(false);
        return MediaInfo::parseResult(url, result);
    } else if (url.toString().endsWith("?extended=images")) {
        QJsonObject images = episode.value("images").toObject();
        QVariantMap newScreenshot = parseImageEntry(images.value("screenshot").toObject());
        QVariantMap oldScreenshot = screenshot();
        qDebug() << "EPISODE";
        updateNonLocalImages(&oldScreenshot, newScreenshot);
        setField(EpisodeInfo::FieldScreenshot, oldScreenshot);

        Q_EMIT extendedInfoDownloaded(false);
        Q_EMIT episodeChanged();
        return true;
    }

    downloadAboutToFinish(true);
    return false;
}

QString EpisodeInfo::tableName() const
{
    return EpisodeInfo::staticTableName();
}

QList<QUrl> EpisodeInfo::filesForDownload() const
{
    return QList<QUrl>();
}
