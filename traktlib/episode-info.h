/*
 *
 * This file is part of Trakt.tv.touch
 *
 * Trakt.tv.touch App is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * Ubuntu Calendar App is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __EPISODE_INFO_H__
#define __EPISODE_INFO_H__

#include "media-info.h"
#include <QObject>
#include <QNetworkReply>


class EpisodeInfo : public MediaInfo
{
    Q_OBJECT
    Q_PROPERTY(QString showId READ showId NOTIFY episodeChanged)
    Q_PROPERTY(uint season READ season NOTIFY episodeChanged)
    Q_PROPERTY(uint number READ number NOTIFY episodeChanged)
    Q_PROPERTY(QVariantMap screenshot READ screenshot NOTIFY episodeChanged)
    Q_PROPERTY(QString overview READ overview NOTIFY episodeChanged)
    Q_PROPERTY(double rating READ rating NOTIFY episodeChanged)
    Q_PROPERTY(uint votes READ votes NOTIFY episodeChanged)
    Q_PROPERTY(QDateTime firstAired READ firstAired NOTIFY episodeChanged)
    Q_PROPERTY(bool watched READ watched WRITE setWatched NOTIFY episodeChanged)

public:
    enum EpisodeField {
        FieldShowId = 1000,
        FieldSeason,
        FieldNumber,
        FieldScreenshot,
        FieldOverview,
        FieldRating,
        FieldVotes,
        FieldFirstAired,
        FieldWatched
    };
    EpisodeInfo(const QString &showId, uint season, uint number, QObject *parent = 0);
    ~EpisodeInfo();

    QString showId() const;
    uint season() const;
    uint number() const;
    QString overview() const;
    double rating() const;
    uint votes() const;
    QVariantMap screenshot() const;
    QDateTime firstAired() const;
    bool watched() const;
    void setWatched(bool w);
    QString fieldName(uint field) const;
    QVariant value(uint field) const;
    virtual void save();

    Q_INVOKABLE void download(bool force = false);

    static QString staticTableName();
    static QList<EpisodeInfo*> all(const QString &showId = "", int season = -1, QObject *parent = 0);

public Q_SLOTS:
    void downloadExtendedInfo(bool force=false);

Q_SIGNALS:
    void episodeChanged();
    void extendedInfoDownloaded(bool fail);

protected:
    bool parseResult(const QUrl &url, const QJsonDocument &result);
    QString tableName() const;
    QList<QUrl> filesForDownload() const;

private:
    EpisodeInfo(const DBItemCache &cache, QObject *parent = 0);

};

#endif
