/*
 *  This file is part of Trakt.tv.touch
 *
 * Trakt.tv.touch App is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * Ubuntu Calendar App is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "show-downloader.h"
#include "show-info.h"
#include "season-info.h"
#include "episode-info.h"

ShowDownloader::ShowDownloader(QObject *parent)
    : QObject(parent),
      m_singleDownload(false),
      m_forceDownload(false),
      m_downloading(false),
      m_checkForNewEpisodes(false),
      m_downloadEpisodeExtendedData(false),
      m_progress(0),
      m_totalProgress(0)
{
    connect(this, SIGNAL(downloadError()), SLOT(onDownloadingDone()));
    connect(this, SIGNAL(downloadFinished()), SLOT(onDownloadingDone()));
}

ShowDownloader::ShowDownloader(ShowInfo *info,
                               bool singleDownload,
                               bool forceDownload,
                               QObject *parent)
    : QObject(parent),
      m_show(info),
      m_singleDownload(singleDownload),
      m_forceDownload(forceDownload),
      m_downloading(false),
      m_checkForNewEpisodes(false),
      m_downloadEpisodeExtendedData(false),
      m_progress(0),
      m_totalProgress(0)
{
    connect(this, SIGNAL(downloadError()), SLOT(onDownloadingDone()));
    connect(this, SIGNAL(downloadFinished()), SLOT(onDownloadingDone()));
}

ShowDownloader::~ShowDownloader()
{
    disconnect(m_show.data());
    Q_FOREACH(const MediaInfo *i, m_downloadQueue) {
        disconnect(i);
    }
    m_downloadQueue.clear();
}

ShowInfo *ShowDownloader::show() const
{
    return m_show.data();
}

void ShowDownloader::setShow(ShowInfo *show)
{
    if (m_show.data() != show) {
        m_show = show;
        Q_EMIT showChanged();
    }
}

bool ShowDownloader::forceDownload() const
{
    return m_forceDownload;
}

void ShowDownloader::setForceDownload(bool flag)
{
    if (m_forceDownload != flag) {
        m_forceDownload = flag;
        Q_EMIT forceDownloadChanged();
    }
}

bool ShowDownloader::checkForNewEpisode() const
{
    return m_checkForNewEpisodes;
}

void ShowDownloader::setCheckForNewEpisode(bool flag)
{
    if (m_checkForNewEpisodes != flag) {
        m_checkForNewEpisodes = flag;
        Q_EMIT checkForNewEpisodeChanged();
    }
}

bool ShowDownloader::downloading() const
{
    return m_downloading;
}

bool ShowDownloader::downloadEpisodeExtendedData() const
{
    return m_downloadEpisodeExtendedData;
}

void ShowDownloader::setDownloadEpisodeExtendedData(bool flag)
{
    if (m_downloadEpisodeExtendedData != flag) {
        m_downloadEpisodeExtendedData = flag;
        Q_EMIT downloadEpisodeExtendedDataChanged();
    }
}

qreal ShowDownloader::progress() const
{
    qreal p = m_totalProgress > 0 && m_progress > 0 ? (m_progress / m_totalProgress) : 0.0;
    return p;
}

void ShowDownloader::start()
{
    if (!m_downloadQueue.isEmpty()) {
        qWarning() << "Download already in progress";
        return;
    }
    m_progress = 0.0;
    m_totalProgress = 6.0;
    m_downloading = true;
    Q_EMIT downloadingChanged();

    connect(m_show.data(), SIGNAL(downloadFinished(bool)), SLOT(onShowDownloaded(bool)));
    m_show->download(m_forceDownload);
}

void ShowDownloader::onShowDownloaded(bool error)
{
    disconnect(m_show.data(), SIGNAL(downloadFinished(bool)),
               this, SLOT(onShowDownloaded(bool)));
    if (error) {
        Q_EMIT downloadError();
        return;
    }
    m_progress += 1.0;
    Q_EMIT progressChanged();
    connect(m_show.data(),
            SIGNAL(downloadDataFinished()),
            SLOT(onShowDataDownloaded()));
    m_show->downloadData();
}

void ShowDownloader::onShowDataDownloaded()
{
    disconnect(m_show.data(), SIGNAL(downloadDataFinished()),
               this, SLOT(onShowDataDownloaded()));

    connect(m_show.data(), SIGNAL(seasonsDowloaded(bool)),
            this, SLOT(onSeasonsDownloaded(bool)));

    m_progress += 1.0;
    Q_EMIT progressChanged();
    m_show->downloadSeasons(m_forceDownload || m_checkForNewEpisodes);
}

void ShowDownloader::onSeasonsDownloaded(bool error)
{
    disconnect(m_show.data(), SIGNAL(seasonsDowloaded(bool)),
               this, SLOT(onSeasonDownloaded(bool)));
    if (error) {
        Q_EMIT downloadError();
        return;
    }

    m_progress += 1.0;
    Q_EMIT progressChanged();

    Q_FOREACH(SeasonInfo *s, m_show->seasons()) {
        m_totalProgress += 1.0;
        m_downloadQueue.append(s);
    }
    onSeasonDataDownloaded();
}

void ShowDownloader::onSeasonDownloaded(bool error)
{
    SeasonInfo *season = qobject_cast<SeasonInfo*>(QObject::sender());
    if (season) {
        disconnect(season, SIGNAL(downloadFinished(bool)),
                   this, SLOT(onSeasonDownloaded(bool)));
    }

    if (error) {
        m_downloadQueue.clear();
        Q_EMIT downloadError();
        return;
    }

    m_progress += 1.0;
    Q_EMIT progressChanged();

    connect(season, SIGNAL(downloadDataFinished()),
            SLOT(onSeasonDataDownloaded()));
    season->downloadData();
}

void ShowDownloader::onSeasonDataDownloaded()
{
    SeasonInfo *season = qobject_cast<SeasonInfo*>(QObject::sender());
    if (season) {
        disconnect(season, SIGNAL(downloadDataFinished()),
                   this, SLOT(onSeasonDataDownloaded()));
    }

    m_progress += 1.0;
    Q_EMIT progressChanged();

    if (m_downloadQueue.isEmpty()) {
        Q_FOREACH(SeasonInfo *si, m_show->seasons()) {
            Q_FOREACH(EpisodeInfo *ei, si->episodes()) {
                m_totalProgress += 3.0;
                m_downloadQueue.append(ei);
            }
        }
        onEpisodeDataDownloaded();
        return;
    }
    season = qobject_cast<SeasonInfo*>(m_downloadQueue.takeFirst());
    connect(season, SIGNAL(downloadFinished(bool)),
            SLOT(onSeasonDownloaded(bool)));
    season->download(m_forceDownload);
}

void ShowDownloader::onEpisodeDownloaded(bool error)
{
    EpisodeInfo *episode = qobject_cast<EpisodeInfo*>(QObject::sender());
    if (episode) {
        disconnect(episode, SIGNAL(downloadFinished(bool)),
                   this, SLOT(onEpisodeDownloaded(bool)));
    }

    if (error) {
        m_downloadQueue.clear();
        Q_EMIT downloadError();
        return;
    }

    if (m_downloadEpisodeExtendedData) {
        m_progress += 1.0;
        Q_EMIT progressChanged();
        connect(episode, SIGNAL(extendedInfoDownloaded(bool)),
                this, SLOT(onEpisodeExtendedDownloaded(bool)));
        episode->downloadExtendedInfo();
    } else {
        m_progress += 2.0;
        Q_EMIT progressChanged();
        connect(episode, SIGNAL(downloadDataFinished()),
                this, SLOT(onEpisodeDataDownloaded()));
        episode->downloadData();
    }
}

void ShowDownloader::onEpisodeExtendedDownloaded(bool error)
{
    EpisodeInfo *episode = qobject_cast<EpisodeInfo*>(QObject::sender());
    if (episode) {
        disconnect(episode, SIGNAL(extendedInfoDownloaded(bool)),
                   this, SLOT(onEpisodeExtendedDownloaded(bool)));
    }

    if (error) {
        m_downloadQueue.clear();
        Q_EMIT downloadError();
        return;
    }

    m_progress += 1.0;
    Q_EMIT progressChanged();

    connect(episode, SIGNAL(downloadDataFinished()),
            this, SLOT(onEpisodeDataDownloaded()));
    episode->downloadData();
}

void ShowDownloader::onEpisodeDataDownloaded()
{
    EpisodeInfo *episode = qobject_cast<EpisodeInfo*>(QObject::sender());
    if (episode) {
        disconnect(episode, SIGNAL(downloadDataFinished()),
                   this, SLOT(onEpisodeDataDownloaded()));
    }

    if (m_downloadQueue.isEmpty()) {
        Q_EMIT downloadFinished();
        return;
    }

    m_progress += 1.0;
    Q_EMIT progressChanged();

    episode = qobject_cast<EpisodeInfo*>(m_downloadQueue.takeFirst());
    connect(episode, SIGNAL(downloadFinished(bool)),
            SLOT(onEpisodeDownloaded(bool)));
    episode->download(m_forceDownload);
}

void ShowDownloader::onDownloadingDone()
{
    m_progress = m_totalProgress;
    Q_EMIT progressChanged();

    m_downloading = false;
    Q_EMIT downloadingChanged();
    if (m_singleDownload) {
        deleteLater();
    }
}

