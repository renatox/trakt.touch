/*
 * This file is part of Trakt.tv.touch
 *
 * Trakt.tv.touch App is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * Ubuntu Calendar App is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "config.h"
#include "download-queue.h"

#include <iostream>
#include <cstdio>
#include <string>

#include <QStandardPaths>
#include <QTemporaryFile>

DownloadQueue* DownloadQueue::m_instance = 0;

DownloadQueue::DownloadQueue(const QString &basePath, QObject *parent)
    : QObject(parent),
      m_manager(new QNetworkAccessManager(this)),
      m_currentReply(0),
      m_basePath(basePath)
{
    qDebug() << "Donwlod dir:" << m_basePath.absolutePath();
    m_basePath.mkdir(m_basePath.absolutePath());
}

DownloadQueue::~DownloadQueue()
{
    if (m_currentReply) {
        delete m_currentReply;
    }
}

DownloadQueue *DownloadQueue::instance()
{

    if (!m_instance) {
#ifdef CLICK_MODE_ON
        QString downloadBaseDir = QString("./cache/artwork");
#else
        QString downloadBaseDir = QString("%1/artwork")
                .arg(QStandardPaths::writableLocation(QStandardPaths::CacheLocation));
#endif

        qDebug() << "Download files in" << downloadBaseDir;
        m_instance = new DownloadQueue(downloadBaseDir);
    }
    return m_instance;
}

void DownloadQueue::destroy()
{
    if (m_instance) {
        delete m_instance;
        m_instance;
    }
}

void DownloadQueue::append(const QUrl &url)
{
    if (!m_downloadQueue.contains(url)) {
        m_downloadQueue.append(url);
        downloadNext();
    }
}

void DownloadQueue::append(const QList<QUrl> &urls)
{
    Q_FOREACH(const QUrl &url, urls) {
        append(url);
    }
}

void DownloadQueue::onDownloadFinished()
{
    if (!m_currentReply)
        return;

    QNetworkReply *reply = qobject_cast<QNetworkReply*>(QObject::sender());
    QByteArray data = reply->readAll();

    QUrl requestFile = reply->request().url().fileName();
    QTemporaryFile *localFile = localFileFromRemoteName(requestFile);
    if (localFile && localFile->open()) {
        qDebug() << "Save local file" << localFile->fileName();
        localFile->setAutoRemove(false);
        localFile->write(data);
        localFile->close();
        Q_EMIT fileDownloaded(reply->request().url(),
                              QUrl::fromLocalFile(localFile->fileName()), true);
    } else {
        qWarning() << "Fail to create local file";
        Q_EMIT fileDownloaded(requestFile, QUrl(), false);
    }

    delete localFile;
    reply->deleteLater();
    m_currentReply = 0;

    downloadNext();
}

void DownloadQueue::onDownloadError(QNetworkReply::NetworkError error)
{
    qWarning() << "Fail to download file" << m_currentReply->request().url() << error;
    Q_EMIT fileDownloaded(m_currentReply->request().url(), QUrl(), false);
    m_currentReply->deleteLater();
    disconnect(m_currentReply,
            SIGNAL(finished()),
            this, SLOT(onDownloadFinished()));
    disconnect(m_currentReply,
            SIGNAL(error(QNetworkReply::NetworkError)),
            this, SLOT(onDownloadError(QNetworkReply::NetworkError)));
    m_currentReply = 0;
    downloadNext();
}

QTemporaryFile *DownloadQueue::localFileFromRemoteName(const QUrl &remoteFile)
{
    QString fileName = m_basePath.absoluteFilePath(QString("data_XXXXXX.%1")
                                           .arg(remoteFile.toString().split('.').last()));
    return new QTemporaryFile(fileName);
}

void DownloadQueue::downloadNext()
{
    if (m_currentReply || m_downloadQueue.isEmpty()) {
        return;
    }

    QUrl nextFile = m_downloadQueue.takeLast();
    QNetworkRequest request(nextFile);

    m_currentReply = m_manager->get(request);
    connect(m_currentReply,
            SIGNAL(finished()),
            SLOT(onDownloadFinished()));
    connect(m_currentReply,
            SIGNAL(error(QNetworkReply::NetworkError)),
            SLOT(onDownloadError(QNetworkReply::NetworkError)));
}

