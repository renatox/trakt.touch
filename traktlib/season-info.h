/*
 *  This file is part of Trakt.tv.touch
 *
 * Trakt.tv.touch App is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * Ubuntu Calendar App is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __SEASON_INFO_H__
#define __SEASON_INFO_H__

#include "media-info.h"
#include <QObject>
#include <QNetworkReply>

class EpisodeInfo;

class SeasonInfo : public MediaInfo
{
    Q_OBJECT
    Q_PROPERTY(QString showId READ showId NOTIFY seasonChanged)
    Q_PROPERTY(uint number READ number NOTIFY seasonChanged)
    Q_PROPERTY(QVariantMap poster READ poster NOTIFY seasonChanged)
    Q_PROPERTY(QUrl thumb READ thumb NOTIFY seasonChanged)
    Q_PROPERTY(uint airedEpisodes READ airedEpisodes NOTIFY seasonChanged)
    Q_PROPERTY(uint episodeCount READ episodeCount NOTIFY seasonChanged)
    Q_PROPERTY(uint watchedEpisodes READ watchedEpisodes NOTIFY seasonChanged)
    Q_PROPERTY(QString overview READ overview NOTIFY seasonChanged)

public:
    enum SeasonField {
        FieldShowId = 1000,
        FieldNumber,
        FieldPoster,
        FieldThumb,
        FieldAiredEpisodes,
        FieldEpisodeCount,
        FieldOverview,
        FieldRating,
        FieldWatchedCount
    };
    static SeasonInfo* fromJsonObject(const QString &showId, const QJsonObject &data, QObject *parent = 0);
    ~SeasonInfo();

    QString showId() const;
    uint number() const;
    QVariantMap poster() const;
    QUrl thumb() const;
    uint airedEpisodes() const;
    uint episodeCount() const;
    QString overview() const;
    uint watchedEpisodes() const;
    QString fieldName(uint field) const;

    void download(bool force = false);

    Q_INVOKABLE QList<EpisodeInfo*> episodes();
    Q_INVOKABLE EpisodeInfo* episode(int index);

    static QString staticTableName();
    static QList<SeasonInfo*> all(const QString &showId = "", QObject *parent = 0);

Q_SIGNALS:
    void seasonChanged();

private Q_SLOTS:
    void onEpisodeChanged();

protected:
    bool parseResult(const QUrl &url, const QJsonDocument &result);

private:
    QList<EpisodeInfo*> m_episodes;
    QQueue<EpisodeInfo*> m_fullDownloadQueue;
    uint m_watched;

    SeasonInfo(const DBItemCache &item, QObject *parent=0);
    SeasonInfo(const QString &showId, uint number, QObject *parent = 0);

    void setImagesData(const QJsonObject &season);
    void setExtendedData(const QJsonObject &season);
    void downloadChildren();
    void loadEpisodes();

    friend class ShowInfo;
};

#endif
