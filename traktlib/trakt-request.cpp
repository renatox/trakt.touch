/*
 *  This file is part of Trakt.tv.touch
 *
 * Trakt.tv.touch App is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * Ubuntu Calendar App is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "config.h"
#include "trakt-request.h"

#include <QNetworkAccessManager>

TraktRequest::TraktRequest(QObject *parent)
    : QObject(parent),
      m_manager(new QNetworkAccessManager(this)),
      m_reply(0),
      m_pageNumber(-1),
      m_pageSize(-1)
{
}

TraktRequest::~TraktRequest()
{
}

void TraktRequest::setPagination(int pageNumber, int pageSize)
{
    m_pageNumber = pageNumber;
    m_pageSize = pageSize;
}

void TraktRequest::setSuffix(const QString &suffix)
{
    if (m_suffix != suffix) {
        m_suffix = suffix;
    }
}

QString TraktRequest::appendPageInfo(const QString &url) const
{
    QString newUrl(url);
    if ((m_pageNumber != -1) && (m_pageSize >0)) {
        newUrl = QString("%1%2page=%3&limit=%4")
                .arg(newUrl)
                .arg(url.indexOf("?") == -1 ? "?" : "&")
                .arg(m_pageNumber)
                .arg(m_pageSize);
    }

    return newUrl;
}

void TraktRequest::start()
{
    QNetworkRequest request;
    request.setHeader(QNetworkRequest::ContentTypeHeader, QVariant("application/json"));
    request.setRawHeader(QByteArray("trakt-api-version"), QByteArray("2"));
    request.setRawHeader(QByteArray("trakt-api-key"), QByteArray(TRAKT_CLIENT_ID));
    request.setUrl(url());

    m_reply = m_manager->get(request);
    m_reply->setParent(this);
    connect(m_reply,
            SIGNAL(finished()),
            SLOT(onReplyFinished()));
    connect(m_reply,
            SIGNAL(error(QNetworkReply::NetworkError)),
            SLOT(onReplyError(QNetworkReply::NetworkError)));
}

QUrl TraktRequest::url() const
{
    QString newUrl = QString("%1/%2").arg(TRAKT_URL).arg(m_suffix);
    return QUrl(appendPageInfo(newUrl));

}

void TraktRequest::onReplyFinished()
{
    if (m_reply) {
        QUrl url = m_reply->request().url();
        QByteArray data = m_reply->readAll();

        m_reply->deleteLater();
        m_reply = 0;

        Q_EMIT finished(url, QJsonDocument::fromJson(data));
    }
}

void TraktRequest::onReplyError(QNetworkReply::NetworkError errorCode)
{
    qDebug() << "Request" << m_reply->request().url() << "Error:" << errorCode;
    m_reply->deleteLater();
    m_reply = 0;

    Q_EMIT error(errorCode);
}
