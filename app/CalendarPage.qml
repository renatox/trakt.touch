/*
 *  This file is part of Trakt.tv.touch
 *
 * Trakt.tv.touch App is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * Ubuntu Calendar App is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.2
import Ubuntu.Components 1.1
import Ubuntu.Components.ListItems 1.0 as ListItem
import Trakt 0.1 as Trakt
import "Show.js" as ShowJs

Page {
    id: root
    property var currentDate: new Date()

    title: i18n.tr("Calendar")

    head.actions: [
        Action {
            iconName: "list-add"
            text: i18n.tr("Add")
            onTriggered: pageStack.push(Qt.resolvedUrl("OnlinePage.qml"))
        },
        Action {
            iconName: "starred"
            text: i18n.tr("Library")
            onTriggered: pageStack.push(Qt.resolvedUrl("FavoritePage.qml"))
        },
        Action {
            iconName: "calendar-today"
            text: i18n.tr("Today")
            onTriggered: {
                var index = showModel.mostRecent()
                showList.positionViewAtIndex(index, ListView.Center)
            }
        }
    ]

    ListView {
        id: showList

        anchors.fill: parent
        model: Trakt.CalendarModel {
            id: showModel
        }

        section {
            property: "section"
            criteria: ViewSection.FullString
            delegate: ListItem.Header {
               text: section
            }
        }

        delegate: ShowEpisodeDelegate {
            showTitleText: "%1, %2".arg(showTitle).arg(season > 0 ? i18n.tr("Season %1").arg(season) : i18n.tr("Specials"))
            episodeTitleText: "%1, %2".arg(number).arg(title ? title : i18n.tr("Unknown"))
            extendInfo: Qt.formatDateTime(firstAired, 'MMM d, yyyy hh:mm')
            imageSource: ShowJs.getSmallestAvailableImage(seasonPoster, showPoster)
            ratingNumber: rating
            opacity: isNaN(firstAired.getTime()) || (firstAired.getTime() < currentDate.getTime()) ? 0.5 : 1.0
            showWatched: watched == true
            onShowWatchedChanged: {
                 if (watched != showWatched) {
                    showModel.setWatched(index, showWatched)
                }
            }
            onClicked: pageStack.push(Qt.resolvedUrl("EpisodePage.qml"),
                                      {"episodeInfo" : showModel.episode(index)} )
        }
    }

    Timer {
        interval: 1000
        running: true
        repeat: false
        onTriggered: {
            var index = showModel.mostRecent()
            showList.positionViewAtIndex(index, ListView.Center)
        }
    }
}
