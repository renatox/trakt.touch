/*
 *  This file is part of Trakt.tv.touch
 *
 * Trakt.tv.touch App is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * Ubuntu Calendar App is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.2
import Ubuntu.Components 1.1
import Ubuntu.Components.ListItems 1.0 as ListItem
import "Show.js" as ShowJs

Page {
    id: root

    property var showInfo

    title: showInfo.title

    Flickable {
        anchors.fill: parent
        contentHeight: columnContents.height + units.gu(8)

        Column {
            id: columnContents

            anchors{
                top: parent.top
                left: parent.left
                right: parent.right
                margins: units.gu(2)
            }
            height: childrenRect.height
            spacing: units.gu(1)

            Image {
                anchors{
                    left: parent.left
                    right: parent.right
                }
                height: units.gu(20)
                source: showInfo.thumb
                asynchronous: true
                fillMode: Image.PreserveAspectCrop
            }

            ListItem.ThinDivider {}

            ShowDetail {
                title: i18n.tr("Overview")
                text: showInfo.overview
            }

            ShowDetail {
                title: i18n.tr("Network")
                text: showInfo.network
            }

            ShowDetail {
                title: i18n.tr("Status")
                text: ShowJs.showTime(showInfo.status, showInfo.airs)
            }

            ShowDetail {
                title: i18n.tr("Genres")
                text: showInfo.genres.join(', ')
            }

            ShowDetail {
                title: i18n.tr("Rating")
                text: showInfo.rating.toFixed(2) + " / 10.0"
            }

            ShowDetail {
                title: i18n.tr("Aired Episodes")
                text: showInfo.airedEpisodes
            }
        }
    }
 }
