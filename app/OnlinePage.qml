/*
 *  This file is part of Trakt.tv.touch
 *
 * Trakt.tv.touch App is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * Ubuntu Calendar App is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.2
import Ubuntu.Components 1.1
import Trakt 0.1 as Trakt

Page {
    id: root

    title: i18n.tr("Trending")

    state: "default"
    states: [
        PageHeadState {
            id: defaultState

            name: "default"
            actions: [
                Action {
                    text: i18n.tr("Search")
                    iconName: "search"
                    onTriggered: {
                        root.state = "search"
                        searchField.forceActiveFocus()
                    }
                }
            ]
            PropertyChanges {
                target: root.head
                actions: defaultState.actions
            }
            PropertyChanges {
                target: searchField
                text: ""
                visible: false
            }
        },
        PageHeadState {
            id: searchingState

            name: "search"
            backAction: Action {
                iconName: "back"
                text: i18n.tr("Cancel")
                onTriggered: {
                    listView.forceActiveFocus()
                    showModel.cancelSearch()
                    root.state = "default"
                }
            }

            PropertyChanges {
                target: root.head
                backAction: searchingState.backAction
                contents: searchField
            }

            PropertyChanges {
                target: searchField
                text: ""
                visible: true
            }
        }
    ]

    TextField {
        id: searchField

        anchors {
            left: parent.left
            right: parent.right
            rightMargin: units.gu(2)
        }
        visible: false
        inputMethodHints: Qt.ImhNoPredictiveText
        placeholderText: i18n.tr("Search...")
        onAccepted: {
            showModel.search(text)
            listView.currentIndex = -1
        }
    }

    ListView {
        id: listView

        anchors.fill: parent
        model: Trakt.OnlineShowsModel {
            id: showModel
        }
        delegate: ShowDelegate {
            title: "%1 (%2)".arg(showTitle).arg(year)
            imageSource: poster ? poster.thumb : ""
            ratingNumber: rating
            extendInfo: {
                var detail = status
                if (status === "returning series") {
                    detail = "%2 %3".arg(airs.day).arg(airs.time)
                } else if (status === "in production") {
                    detail = i18n.tr("TBD")
                } else if (status === "canceled") {
                    detail = "Canceled"
                } else if (status === "ended") {
                    detail = "Ended"
                }
                return "%1 - %2".arg(network).arg(detail)
            }
            Icon {
                anchors {
                    right: parent.right
                    top: parent.top
                    margins: units.gu(1)
                }
                height: units.gu(3)
                width: height
                name: saved ? "starred" : "non-starred"
                MouseArea {
                    anchors.fill: parent
                    onClicked: {
                        if (saved) {
                            listView.model.remove(index)
                        } else {
                            downloadingDialog.showTitle = showTitle
                            listView.model.save(index)
                        }
                    }
                }
            }
        }
    }
    DownloadingDialog {
        id: downloadingDialog

        property string showTitle

        title: i18n.tr("Downloading\n%1...").arg(downloadingDialog.showTitle)
        visible: showModel.downloading
        progress: showModel.downloadingProgress
    }
}
