/*
 *  This file is part of Trakt.tv.touch
 *
 * Trakt.tv.touch App is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * Ubuntu Calendar App is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.2
import QtQuick.Layouts 1.1
import Ubuntu.Components 1.1
import Ubuntu.Components.ListItems 1.0 as ListItem
import "Show.js" as ShowJs

Page {
    id: root

    property var seasonInfo
    property var showInfo

    head.actions: [
        Action {
            iconName: "select"
            text: i18n.tr("Select all")
            onTriggered: {
                for(var i = 0; i < seasonInfo.episodeCount; i++) {
                    var e = seasonInfo.episode(i)
                    if (!e.watched) {
                        e.watched = true
                        e.save()
                    }
                }
            }
        }
    ]

    title: i18n.tr("Season %1").arg(seasonInfo.number)

    Flickable {
        anchors {
            fill: parent
            margins: units.gu(2)
        }
        contentHeight: list.height

        Column {
            id: list

            anchors {
                top: parent.top
                left: parent.left
                right: parent.right
            }
            height: childrenRect.height

            Repeater {
                id: repeaterEpisodes

                model: seasonInfo ? seasonInfo.episodeCount : 0
                delegate: EpisodeDelegate {
                    episode: root.seasonInfo.episode(index)
                    onClicked: pageStack.push(Qt.resolvedUrl("EpisodePage.qml"),
                                              {"episodeInfo" : episode} )
                }
            }
        }
    }
}
