/*
 *  This file is part of Trakt.tv.touch
 *
 * Trakt.tv.touch App is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * Ubuntu Calendar App is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.2
import QtQuick.Layouts 1.1
import Ubuntu.Components 1.1
import Ubuntu.Components.ListItems 1.0 as ListItem
import Trakt 0.1 as Trakt
import "Show.js" as ShowJs

Page {
    id: root

    property var showInfo
    readonly property var showSeasons: showInfo ? showInfo.seasons : null

    title: showInfo.title

    head.actions: [
        Action {
            iconName: "reload"
            enabled: !downloader.downloading
            text: i18n.tr("Reload")
            onTriggered: downloader.start()
        },
        Action {
            iconName: "info"
            text: i18n.tr("Info")
            onTriggered: pageStack.push(Qt.resolvedUrl("ShowDetails.qml"),
                                        {"showInfo" : root.showInfo})
        }

    ]

    ColumnLayout {
        anchors {
            margins: units.gu(2)
            fill: parent
        }

        Image {
            Layout.fillWidth: true
            Layout.maximumHeight: units.gu(20)
            Layout.minimumHeight: units.gu(20)
            source: showInfo.thumb
            asynchronous: true
            fillMode: Image.PreserveAspectCrop
        }

        ListItem.ThinDivider {
            Layout.fillWidth: true
        }

        Flickable {
            Layout.fillWidth: true
            Layout.fillHeight: true
            contentHeight: list.height
            clip: true
            Column {
                id: list

                anchors {
                    top: parent.top
                    left: parent.left
                    right: parent.right
                }
                height: childrenRect.height

                Repeater {
                    model: showSeasons ? showSeasons.length : 0
                    delegate: SeasonDelegate {
                        season: root.showSeasons[index]
                        onClicked: pageStack.push(Qt.resolvedUrl("EpisodesPage.qml"),
                                                  {"seasonInfo": season,
                                                   "showInfo": root.showInfo,
                                                   "title": root.title})
                    }
                }
            }
        }
    }

    DownloadingDialog {
        title: i18n.tr("Updating info...")
        visible: downloader.downloading
        progress: downloader.progress
    }

    Trakt.ShowDownloader {
        id: downloader

        show: root.showInfo
        checkForNewEpisode: true
        onDownloadFinished: root.showInfo.saveAll()
    }
}
