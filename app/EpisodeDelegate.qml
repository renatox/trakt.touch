/*
 *  This file is part of Trakt.tv.touch
 *
 * Trakt.tv.touch App is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * Ubuntu Calendar App is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.2
import Ubuntu.Components 1.1
import Ubuntu.Components.ListItems 1.0 as ListItem

MouseArea {
    id: root

    property var episode: null

    anchors {
        left: parent.left
        right: parent.right
    }
    height: units.gu(8)

    Label {
        id: epTitle

        anchors {
            top: parent.top
            left: parent.left
            right: epNumber.left
            rightMargin: units.gu(1)
            topMargin: units.gu(1)
        }
        text: episode.title
        color: UbuntuColors.orange
    }
    Label {
        id: epNumber
        anchors {
            top: parent.top
            right: parent.right
            topMargin: units.gu(1)
        }
        text: episode.number
    }
    Label {
        id: epDate
        anchors {
            left: parent.left
            right: parent.right
            top: epTitle.bottom
            topMargin: units.gu(1)
        }
        text: Qt.formatDate(episode.firstAired, 'MMM d, yyyy')
        onTextChanged: console.debug("EPISODE DATE" + episode.firstAired)
    }
    CheckBox {
        id: watch

        anchors {
            bottom: parent.bottom
            right: parent.right
            bottomMargin: units.gu(1)
        }
        checked: episode.watched === true
        onCheckedChanged: {
             if (checked !== episode.watched) {
                 episode.watched = checked
                 episode.save()
            }
        }
    }


    ListItem.ThinDivider {
        anchors.bottom: parent.bottom
    }
}
