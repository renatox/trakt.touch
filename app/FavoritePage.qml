/*
 *  This file is part of Trakt.tv.touch
 *
 * Trakt.tv.touch App is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * Ubuntu Calendar App is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.2
import Ubuntu.Components 1.1
import Trakt 0.1 as Trakt
import "Show.js" as ShowJs

Page {
    title: i18n.tr("Favorites")

    head.actions: [
        Action {
            iconName: "list-add"
            text: i18n.tr("Add")
            onTriggered: pageStack.push(Qt.resolvedUrl("OnlinePage.qml"))
        }
    ]

    ListView {
        id: listView

        anchors.fill: parent
        model: Trakt.OfflineShowsModel {}
        delegate: ShowDelegate {
            title: "%1 (%2)".arg(showTitle).arg(year)
            imageSource: ShowJs.getSmallestImage(poster)
            ratingNumber: rating

            extendInfo: ("%1 - %2").arg(network).arg(ShowJs.showTime(status, airs))
            MouseArea {
                anchors.fill: parent
                onClicked: pageStack.push(Qt.resolvedUrl("SeasonsPage.qml"),
                                          {'showInfo': listView.model.show(index)} )
            }
        }
    }
}
