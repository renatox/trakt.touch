/*
 *  This file is part of Trakt.tv.touch
 *
 * Trakt.tv.touch App is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * Ubuntu Calendar App is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.2
import Ubuntu.Components 1.1
import Ubuntu.Components.ListItems 1.0 as ListItem

MouseArea {
    property alias showTitleText: title.text
    property alias episodeTitleText: subTitle.text
    property alias extendInfo: extendInfo.text
    property alias imageSource: image.source
    property double ratingNumber: 0
    property string imageFallback: Qt.resolvedUrl("art/trakt_poster.png")
    readonly property double realRating: (ratingNumber > 0) ? Math.floor(ratingNumber / 2) : 0
    property alias showWatched: watch.checked

    anchors {
        left: parent.left
        right: parent.right
    }
    height: units.gu(15)

    UbuntuShape {
        id: imageShape

        anchors {
            top: parent.top
            left: parent.left
            bottom: parent.bottom
            margins: units.gu(2)
        }
        width: units.gu(8)
        visible: image.status === Image.Ready

        image: Image {
            id: image
            fillMode: Image.PreserveAspectCrop
        }
    }
    UbuntuShape {
        id: imageFallbackShape

        anchors.fill: imageShape
        visible: image.status != Image.Ready

        image: Image {
            source: imageFallbackShape.visible ? imageFallback : ""
        }
    }

    Label {
        id: title

        anchors {
            left: imageShape.right
            top: parent.top
            right: watch.left
            margins: units.gu(2)
            leftMargin: units.gu(1)
        }
        color: UbuntuColors.orange
        elide: Text.ElideRight
    }

    Label {
        id: subTitle

        anchors {
            left: imageShape.right
            right: watch.left
            top: title.bottom
            margins: units.gu(2)
            leftMargin: units.gu(1)
        }
        elide: Text.ElideRight
    }
    Row {
        anchors {
            left: imageShape.right
            top: subTitle.bottom
            margins: units.gu(2)
        }
        height: units.gu(3)
        width: childrenRect.width
        spacing: units.gu(0.5)

        Repeater {
            id: ratingRepeat
            model: 5
            Icon {
                name: (realRating > index) ? "starred" : "non-starred"
                height: units.gu(1.5)
                width: height
                color: UbuntuColors.warmGrey
            }
        }
    }

    Label {
        id: extendInfo

        anchors {
            right: parent.right
            bottom: parent.bottom
            margins: units.gu(2)
        }
        fontSize: "small"
    }

    CheckBox {
        id: watch

        anchors {
            right: parent.right
            top: parent.top
            margins: units.gu(2)
        }
        height: units.gu(4)
        width: height
    }

    ListItem.ThinDivider {
        anchors {
            bottom: parent.bottom
            left: parent.left
            right: parent.right
            leftMargin: units.gu(2)
            rightMargin: units.gu(2)
        }
    }
}
