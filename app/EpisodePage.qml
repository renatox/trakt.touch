/*
 *  This file is part of Trakt.tv.touch
 *
 * Trakt.tv.touch App is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * Ubuntu Calendar App is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.2
import Ubuntu.Components 1.1
import Ubuntu.Components.ListItems 1.0 as ListItem
import "Show.js" as ShowJs

Page {
    id: root

    property var episodeInfo

    title: episodeInfo.title && episodeInfo.title !== "" ?  episodeInfo.title : i18n.tr("Unknow")

    head.foregroundColor: episodeInfo.watched ? UbuntuColors.green : undefined
    head.actions: [
        Action {
            iconName: "tick"
            text: i18n.tr("Watched")
            onTriggered: {
                episodeInfo.watched = !episodeInfo.watched
                episodeInfo.save()
            }
        }
    ]

    Flickable {
        anchors.fill: parent
        contentHeight: columnContents.height + units.gu(8)

        Column {
            id: columnContents

            anchors{
                top: parent.top
                left: parent.left
                right: parent.right
                margins: units.gu(2)
            }
            height: childrenRect.height
            spacing: units.gu(1)

            Image {
                id: episodeImage

                anchors{
                    left: parent.left
                    right: parent.right
                }
                height: units.gu(20)
                source: Qt.resolvedUrl("./art/trakt_banner.png")
                asynchronous: true
                fillMode: Image.PreserveAspectCrop

                ActivityIndicator {
                    id: donwloadIndicator

                    visible: episodeImage.status != Image.Ready
                    running: visible
                    anchors.centerIn: parent
                    height: units.gu(4)
                }
            }

            ListItem.ThinDivider {}

            ShowDetail {
                title: ShowJs.formatSeasonEpisode(episodeInfo.season, episodeInfo.number)
                text: episodeInfo.overview
            }

            ShowDetail {
                title: i18n.tr("First Aired")
                text: Qt.formatDateTime(episodeInfo.firstAired, 'MMM d, yyyy hh:mm ap')
            }

            ShowDetail {
                title: i18n.tr("Rating")
                text: episodeInfo.rating.toFixed(2) + " / 10.0"
            }
        }
    }

    Component.onCompleted: {
        if (episodeInfo) {
            episodeInfo.downloadExtendedInfo()
        }
    }

    Connections {
        target: episodeInfo
        onExtendedInfoDownloaded: {
            episodeInfo.save()
            var img = ShowJs.getSmallestImage(episodeInfo.screenshot)
            if (img != "")
                episodeImage.source = img
        }
    }
}
