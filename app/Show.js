/*
 *  This file is part of Trakt.tv.touch
 *
 * Trakt.tv.touch App is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * Ubuntu Calendar App is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

function showTime(originalStatus, airs)
{
    if (originalStatus === "returning series") {
       return "%2 %3".arg(airs.day).arg(airs.time)
    } else if (originalStatus === "in production") {
       return i18n.tr("TBD")
    } else if (originalStatus === "canceled") {
       return i18n.tr("Canceled")
    } else if (originalStatus === "ended") {
       return i18n.tr("Ended")
    }
    return originalStatus
}

function getSmallestImage(imgs)
{
    if (!imgs)
        return ""

    if (imgs.thumb && imgs.thumb != "") {
        return imgs.thumb
    } if (imgs.medium && imgs.medium != "") {
        return imgs.medium
    }
    return imgs.full
}

function getSmallestAvailableImage(imgLst1, imgLst2)
{
    var img = getSmallestImage(imgLst1)
    if (img == "") {
        return getSmallestImage(imgLst2)
    }
    return img
}

function formatSeasonEpisode(season, episode)
{
    var seasonNum = season < 10 ? "0" + season : season
    var episodeNum = episode < 10 ? "0" + episode : episode
    return "S%1E%2".arg(seasonNum).arg(episodeNum)
}

function episodeImage(showImage, seasonImage, episodeImages)
{
    var episodeImg = getSmallestImage(episodeImages)
    console.debug("Return episode image:" + episodeImg)
    if (episodeImg !== "") {
        return episodeImg
    }

     console.debug("Return season image:" + seasonImage)
    if (seasonImage !== "") {
        return seasonImage
    } else {
        return showImage
    }
}

