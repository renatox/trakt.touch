/*
 *  This file is part of Trakt.tv.touch
 *
 * Trakt.tv.touch App is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * Ubuntu Calendar App is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.2
import Ubuntu.Components 1.1
import Ubuntu.Components.ListItems 1.0 as ListItem

Item {
    id: root

    property alias title: downloadingLabel.text
    property alias progress: progressBar.value

    anchors.fill: parent

    UbuntuShape {
        id: bgRect

        anchors.centerIn: parent
        height: units.gu(15)
        width: units.gu(25)
        color: UbuntuColors.coolGrey
        opacity: 0.8
    }

    Column {
        anchors {
            verticalCenter: bgRect.verticalCenter
            left: bgRect.left
            right: bgRect.right
            margins: units.gu(1)
        }
        height: childrenRect.height
        spacing: units.gu(1)
        Label {
            id: downloadingLabel
            anchors {
                left: parent.left
                right: parent.right
            }
            height: paintedHeight
            verticalAlignment: Text.AlignHCenter
            horizontalAlignment: Text.AlignHCenter
            elide: Text.ElideRight
            color: "white"
        }
        ActivityIndicator {
            id: donwloadIndicator

            running: root.visible
            anchors.horizontalCenter: parent.horizontalCenter
            height: units.gu(4)
            visible: progress == 0.0
        }
        ProgressBar {
            id: progressBar

            anchors {
                left: parent.left
                right: parent.right
                margins: units.gu(2)
            }
            height: units.gu(2)
            showProgressPercentage: false
            minimumValue: 0.0
            maximumValue: 1.0
            visible: value > 0.0
        }
    }

    MouseArea {
        anchors.fill: parent
    }
}
