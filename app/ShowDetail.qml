/*
 *  This file is part of Trakt.tv.touch
 *
 * Trakt.tv.touch App is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * Ubuntu Calendar App is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.2
import Ubuntu.Components 1.1
import Ubuntu.Components.ListItems 1.0 as ListItem

Column {
    property alias title: title.text
    property alias text: label.text

    anchors {
        left: parent.left
        right: parent.right
    }
    height: childrenRect.height
    spacing: units.gu(0.5)

    Label {
        id: title

        anchors {
            left: parent.left
            right: parent.right
        }
        height: units.gu(2)
        color: UbuntuColors.orange
    }
    ListItem.ThinDivider {}
    Label {
        id: label

        anchors {
            left: parent.left
            right: parent.right
        }
        height: paintedHeight
        wrapMode: Text.Wrap
    }
}
