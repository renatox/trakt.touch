/*
 *  This file is part of Trakt.tv.touch
 *
 * Trakt.tv.touch App is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * Ubuntu Calendar App is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.2
import Ubuntu.Components 1.1

MainView {
    id: mainWindow

    width: units.gu(40)
    height: units.gu(71)
    useDeprecatedToolbar: false

    //backgroundColor: UbuntuColors.coolGrey
    applicationName: "trakt-tv-touch.renato"

    PageStack {
        id: mainStack

        anchors.fill: parent
    }

    Component.onCompleted: {
        mainStack.push(Qt.resolvedUrl("CalendarPage.qml"))
    }
}
