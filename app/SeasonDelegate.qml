/*
 *  This file is part of Trakt.tv.touch
 *
 * Trakt.tv.touch App is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * Ubuntu Calendar App is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.2
import Ubuntu.Components 1.1
import Ubuntu.Components.ListItems 1.0 as ListItem

MouseArea {
    id: root

    property var season: null

    anchors {
        left: parent.left
        right: parent.right
    }
    height: units.gu(8)

    Label {
        id: title
        anchors {
            top: parent.top
            left: parent.left
            right: parent.right
            topMargin: units.gu(2)
        }
        text: season && season.number > 0 ? i18n.tr("Season %1").arg(season.number) : season.number === 0 ? i18n.tr("Specials") : ""
        color: UbuntuColors.orange
    }
    Label {
        id: subTitle
        anchors {
            top: parent.top
            right: parent.right
            topMargin: units.gu(2)
        }
        text: ("%1/%2").arg(root.season.airedEpisodes).arg(root.season.episodeCount)
        fontSize: "small"
    }

    Rectangle {
        id: totalEpisodes
        color: UbuntuColors.lightGrey
        anchors {
            bottom: parent.bottom
            right: parent.right
            left: parent.left
            bottomMargin: units.gu(2)
        }
        height: units.gu(1)
    }

    Rectangle {
        id: totalAiredEpisodes
        color: UbuntuColors.orange
        anchors {
            left: totalEpisodes.left
            top: totalEpisodes.top
            bottom: totalEpisodes.bottom
        }
        width: root.season && root.season.episodeCount > 0 ?
                   (root.season.airedEpisodes * totalEpisodes.width) / root.season.episodeCount : 0
    }

    Rectangle {
        id: totalWatchedEpisodes

        color: UbuntuColors.green
        anchors {
            left: totalEpisodes.left
            top: totalEpisodes.top
            bottom: totalEpisodes.bottom
        }
        width: root.season && root.season.watchedEpisodes > 0 ?
                   (root.season.watchedEpisodes * totalEpisodes.width) / root.season.episodeCount : 0
    }

    ListItem.ThinDivider {
        anchors.bottom: parent.bottom
    }
}
