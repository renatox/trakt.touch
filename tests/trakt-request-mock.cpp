#include "config.h"
#include "trakt-request-mock.h"

#include <QJsonDocument>

#define MAX_SEASONS     2
#define MAX_EPISODES    10

static const char show_353_images[] = {
    "{"
    "  \"title\": \"Game of Thrones\","
    "  \"year\": 2011,"
    "  \"ids\": {"
    "    \"trakt\": 353,"
    "    \"slug\": \"game-of-thrones\","
    "    \"tvdb\": 121361,"
    "    \"imdb\": \"tt0944947\","
    "    \"tmdb\": 1399,"
    "    \"tvrage\": 24493"
    "  },"
    "  \"images\": {"
    "    \"fanart\": {"
    "      \"full\": \"https://walter.trakt.us/images/shows/000/000/353/fanarts/original/5a13b51164.jpg?1409353811\","
    "      \"medium\": \"https://walter.trakt.us/images/shows/000/000/353/fanarts/medium/5a13b51164.jpg?1409353811\","
    "      \"thumb\": \"https://walter.trakt.us/images/shows/000/000/353/fanarts/thumb/5a13b51164.jpg?1409353811\""
    "    },"
    "    \"poster\": {"
    "      \"full\": \"https://walter.trakt.us/images/shows/000/000/353/posters/original/46cafaec1f.jpg?1409353810\","
    "      \"medium\": \"https://walter.trakt.us/images/shows/000/000/353/posters/medium/46cafaec1f.jpg?1409353810\","
    "      \"thumb\": \"https://walter.trakt.us/images/shows/000/000/353/posters/thumb/46cafaec1f.jpg?1409353810\""
    "    },"
    "    \"logo\": {"
    "      \"full\": \"https://walter.trakt.us/images/shows/000/000/353/logos/original/13b614ad43.png?1409353812\""
    "    },"
    "    \"clearart\": {"
    "      \"full\": \"https://walter.trakt.us/images/shows/000/000/353/cleararts/original/5cbde9e647.png?1409353813\""
    "    },"
    "    \"banner\": {"
    "      \"full\": \"https://walter.trakt.us/images/shows/000/000/353/banners/original/9fefff703d.jpg?1409353813\""
    "    },"
    "    \"thumb\": {"
    "      \"full\": \"https://walter.trakt.us/images/shows/000/000/353/thumbs/original/607f27fade.jpg?1409353814\""
    "    }"
    "  }"
    "}"
};

static const char show_353_full[] = {
    "{"
    "  \"title\": \"Game of Thrones\","
    "  \"year\": 2011,"
    "  \"ids\": {"
    "    \"trakt\": 353,"
    "    \"slug\": \"game-of-thrones\","
    "    \"tvdb\": 121361,"
    "    \"imdb\": \"tt0944947\","
    "    \"tmdb\": 1399,"
    "    \"tvrage\": 24493"
    "  },"
    "  \"overview\": \"Game of Thrones is an American fantasy drama television series created for HBO by David Benioff and D. B. Weiss. It is an adaptation of A Song of Ice and Fire, George R. R. Martin's series of fantasy novels, the first of which is titled A Game of Thrones.\n\nThe series, set on the fictional continents of Westeros and Essos at the end of a decade-long summer, interweaves several plot lines. The first follows the members of several noble houses in a civil war for the Iron Throne of the Seven Kingdoms; the second covers the rising threat of the impending winter and the mythical creatures of the North; the third chronicles the attempts of the exiled last scion of the realm's deposed dynasty to reclaim the throne. Through its morally ambiguous characters, the series explores the issues of social hierarchy, religion, loyalty, corruption, sexuality, civil war, crime, and punishment.\","
    "  \"first_aired\": \"2011-04-18T01:00:00.000Z\","
    "  \"airs\": {"
    "    \"day\": \"Sunday\","
    "    \"time\": \"21:00\","
    "    \"timezone\": \"America/New_York\""
    "  },"
    "  \"runtime\": 60,"
    "  \"certification\": \"TV-MA\","
    "  \"network\": \"HBO\","
    "  \"country\": \"us\","
    "  \"updated_at\": \"2014-08-22T08:32:06.000Z\","
    "  \"trailer\": null,"
    "  \"homepage\": \"http://www.hbo.com/game-of-thrones/index.html\","
    "  \"status\": \"returning series\","
    "  \"rating\": 9,"
    "  \"votes\": 111,"
    "  \"language\": \"en\","
    "  \"available_translations\": ["
    "    \"en\","
    "    \"tr\","
    "    \"sk\","
    "    \"de\","
    "    \"ru\","
    "    \"fr\","
    "    \"hu\","
    "    \"zh\","
    "    \"el\","
    "    \"pt\","
    "    \"es\","
    "    \"bg\","
    "    \"ro\","
    "    \"it\","
    "    \"ko\","
    "    \"he\","
    "    \"nl\","
    "    \"pl\""
    "  ],"
    "  \"genres\": ["
    "    \"drama\","
    "    \"fantasy\""
    "  ],"
    "  \"aired_episodes\": 50"
    "}"
};

static const char show_353_seasons_image[] = {
    "["
    "  {"
    "    \"number\": 0,"
    "    \"ids\": {"
    "      \"trakt\": 1,"
    "      \"tvdb\": 137481,"
    "      \"tmdb\": 3627,"
    "      \"tvrage\": null"
    "    },"
    "    \"images\": {"
    "      \"poster\": {"
    "        \"full\": \"https://walter.trakt.us/images/seasons/000/002/145/posters/original/41221f3712.jpg?1409351965\","
    "        \"medium\": \"https://walter.trakt.us/images/seasons/000/002/145/posters/medium/41221f3712.jpg?1409351965\","
    "        \"thumb\": \"https://walter.trakt.us/images/seasons/000/002/145/posters/thumb/41221f3712.jpg?1409351965\""
    "      },"
    "      \"thumb\": {"
    "        \"full\": \"https://walter.trakt.us/images/seasons/000/002/145/thumbs/original/c41b46dd09.jpg?1409351965\""
    "      }"
    "    }"
    "  },"
    "  {"
    "    \"number\": 1,"
    "    \"ids\": {"
    "      \"trakt\": 2,"
    "      \"tvdb\": 364731,"
    "      \"tmdb\": 3624,"
    "      \"tvrage\": null"
    "    },"
    "    \"images\": {"
    "      \"poster\": {"
    "        \"full\": \"https://walter.trakt.us/images/seasons/000/002/146/posters/original/1834978783.jpg?1409351969\","
    "        \"medium\": \"https://walter.trakt.us/images/seasons/000/002/146/posters/medium/1834978783.jpg?1409351969\","
    "        \"thumb\": \"https://walter.trakt.us/images/seasons/000/002/146/posters/thumb/1834978783.jpg?1409351969\""
    "      },"
    "      \"thumb\": {"
    "        \"full\": \"https://walter.trakt.us/images/seasons/000/002/146/thumbs/original/6c996deed7.jpg?1409351970\""
    "      }"
    "    }"
    "  }"
    "]"
};

static char show_353_seasons_full[] = {
    "["
    "  {"
    "    \"number\": 0,"
    "    \"ids\": {"
    "      \"trakt\": 1,"
    "      \"tvdb\": 137481,"
    "      \"tmdb\": 3627,"
    "      \"tvrage\": null"
    "    },"
    "    \"rating\": 9,"
    "    \"votes\": 111,"
    "    \"episode_count\": 10,"
    "    \"aired_episodes\": 10,"
    "    \"overview\": null"
    "  },"
    "  {"
    "    \"number\": 1,"
    "    \"ids\": {"
    "      \"trakt\": 2,"
    "      \"tvdb\": 364731,"
    "      \"tmdb\": 3624,"
    "      \"tvrage\": null"
    "    },"
    "    \"rating\": 9,"
    "    \"votes\": 111,"
    "    \"episode_count\": 10,"
    "    \"aired_episodes\": 10,"
    "    \"overview\": \"Season 1 overview.\""
    "  }"
    "]"
};

static char show_353_season_1_episode_list[] = {
    "["
    "  {"
    "    \"season\": 1,"
    "    \"number\": 1,"
    "    \"title\": \"Winter Is Coming\","
    "    \"ids\": {"
    "      \"trakt\": 456,"
    "      \"tvdb\": 3254641,"
    "      \"imdb\": \"tt1480055\","
    "      \"tmdb\": 63056,"
    "      \"tvrage\": null"
    "    }"
    "  },"
    "  {"
    "    \"season\": 1,"
    "    \"number\": 2,"
    "    \"title\": \"The Kingsroad\","
    "    \"ids\": {"
    "      \"trakt\": 457,"
    "      \"tvdb\": 3436411,"
    "      \"imdb\": \"tt1668746\","
    "      \"tmdb\": 63057,"
    "      \"tvrage\": null"
    "    }"
    "  },"
    "  {"
    "    \"season\": 1,"
    "    \"number\": 3,"
    "    \"title\": \"Lord Snow\","
    "    \"ids\": {"
    "      \"trakt\": 458,"
    "      \"tvdb\": 3436421,"
    "      \"imdb\": \"tt1829962\","
    "      \"tmdb\": 63058,"
    "      \"tvrage\": null"
    "    }"
    "  },"
    "  {"
    "    \"season\": 1,"
    "    \"number\": 4,"
    "    \"title\": \"Cripples, Bastards, and Broken Things\","
    "    \"ids\": {"
    "      \"trakt\": 459,"
    "      \"tvdb\": 3436431,"
    "      \"imdb\": \"tt1829963\","
    "      \"tmdb\": 63059,"
    "      \"tvrage\": null"
    "    }"
    "  },"
    "  {"
    "    \"season\": 1,"
    "    \"number\": 5,"
    "    \"title\": \"The Wolf and the Lion\","
    "    \"ids\": {"
    "      \"trakt\": 460,"
    "      \"tvdb\": 3436441,"
    "      \"imdb\": \"tt1829964\","
    "      \"tmdb\": 63060,"
    "      \"tvrage\": null"
    "    }"
    "  },"
    "  {"
    "    \"season\": 1,"
    "    \"number\": 6,"
    "    \"title\": \"A Golden Crown\","
    "    \"ids\": {"
    "      \"trakt\": 461,"
    "      \"tvdb\": 3436451,"
    "      \"imdb\": \"tt1837862\","
    "      \"tmdb\": 63061,"
    "      \"tvrage\": null"
    "    }"
    "  },"
    "  {"
    "    \"season\": 1,"
    "    \"number\": 7,"
    "    \"title\": \"You Win or You Die\","
    "    \"ids\": {"
    "      \"trakt\": 462,"
    "      \"tvdb\": 3436461,"
    "      \"imdb\": \"tt1837863\","
    "      \"tmdb\": 63062,"
    "      \"tvrage\": null"
    "    }"
    "  },"
    "  {"
    "    \"season\": 1,"
    "    \"number\": 8,"
    "    \"title\": \"The Pointy End\","
    "    \"ids\": {"
    "      \"trakt\": 463,"
    "      \"tvdb\": 3360391,"
    "      \"imdb\": \"tt1837864\","
    "      \"tmdb\": 63063,"
    "      \"tvrage\": null"
    "    }"
    "  },"
    "  {"
    "    \"season\": 1,"
    "    \"number\": 9,"
    "    \"title\": \"Baelor\","
    "    \"ids\": {"
    "      \"trakt\": 464,"
    "      \"tvdb\": 4063481,"
    "      \"imdb\": \"tt1851398\","
    "      \"tmdb\": 63064,"
    "      \"tvrage\": null"
    "    }"
    "  },"
    "  {"
    "    \"season\": 1,"
    "    \"number\": 10,"
    "    \"title\": \"Fire and Blood\","
    "    \"ids\": {"
    "      \"trakt\": 465,"
    "      \"tvdb\": 4063491,"
    "      \"imdb\": \"tt1851397\","
    "      \"tmdb\": 63065,"
    "      \"tvrage\": null"
    "    }"
    "  }"
    "]"
};

static const char episode_images_template[] = {
    "{"
    "  \"season\": %SEASON%,"
    "  \"number\": %EPISODE%,"
    "  \"title\": \"Episode S%SEASON%E%EPISODE%\","
    "  \"ids\": {"
    "    \"trakt\": \"trakt-%SEASON%-%EPISODE%\","
    "    \"tvdb\": \"tvdb-%SEASON%-%EPISODE%\","
    "    \"imdb\": \"imdb-%SEASON%-%EPISODE%\","
    "    \"tmdb\": \"tmdb-%SEASON%-%EPISODE%\","
    "    \"tvrage\": null"
    "  },"
    "  \"images\": {"
    "    \"screenshot\": {"
    "      \"full\": \"https://walter.trakt.us/images/screenshot_full_%SEASON%_%EPISODE%.jpg\","
    "      \"medium\": \"https://walter.trakt.us/images/screenshot_medium_%SEASON%_%EPISODE%.jpg\","
    "      \"thumb\": \"https://walter.trakt.us/images/screenshot_thumb_%SEASON%_%EPISODE%.jpg\""
    "    }"
    "  }"
    "}"
};
static const char episode_full_template[] = {
    "{"
    "  \"season\": %SEASON%,"
    "  \"number\": %EPISODE%,"
    "  \"title\": \"Episode S%SEASON%E%EPISODE%\","
    "  \"ids\": {"
    "    \"trakt\": \"trakt-%SEASON%-%EPISODE%\","
    "    \"tvdb\": \"tvdb-%SEASON%-%EPISODE%\","
    "    \"imdb\": \"imdb-%SEASON%-%EPISODE%\","
    "    \"tmdb\": \"tmdb-%SEASON%-%EPISODE%\","
    "    \"tvrage\": null"
    "  },"
    "  \"number_abs\": null,"
    "  \"overview\": \"Overview episode %EPISODE% season %SEASON%\","
    "  \"first_aired\": \"2011-04-18T01:00:00.000Z\","
    "  \"updated_at\": \"2014-08-29T23:16:39.000Z\","
    "  \"rating\": 9,"
    "  \"votes\": 111,"
    "  \"available_translations\": ["
    "    \"en\""
    "  ]"
    "}"
};

static const char show_popular_page_1[] = {
    "["
    "  {"
    "    \"title\": \"Community\","
    "    \"year\": 2009,"
    "    \"ids\": {"
    "      \"trakt\": 41,"
    "      \"slug\": \"community\","
    "      \"tvdb\": 94571,"
    "      \"imdb\": \"tt1439629\","
    "      \"tmdb\": 18347,"
    "      \"tvrage\": 22589"
    "    }"
    "  },"
    "  {"
    "    \"title\": \"The Walking Dead\","
    "    \"year\": 2010,"
    "    \"ids\": {"
    "      \"trakt\": 2,"
    "      \"slug\": \"the-walking-dead\","
    "      \"tvdb\": 153021,"
    "      \"imdb\": \"tt1520211\","
    "      \"tmdb\": 1402,"
    "      \"tvrage\": 25056"
    "    }"
    "  },"
    "  {"
    "    \"title\": \"Dexter\","
    "    \"year\": 2006,"
    "    \"ids\": {"
    "      \"trakt\": 19,"
    "      \"slug\": \"dexter\","
    "      \"tvdb\": 79349,"
    "      \"imdb\": \"tt0773262\","
    "      \"tmdb\": 1405,"
    "      \"tvrage\": null"
    "    }"
    "  },"
    "  {"
    "    \"title\": \"The Simpsons\","
    "    \"year\": 1989,"
    "    \"ids\": {"
    "      \"trakt\": 91,"
    "      \"slug\": \"the-simpsons\","
    "      \"tvdb\": 71663,"
    "      \"imdb\": \"tt0096697\","
    "      \"tmdb\": 456,"
    "      \"tvrage\": 6190"
    "    }"
    "  },"
    "  {"
    "    \"title\": \"Game of Thrones\","
    "    \"year\": 2011,"
    "    \"ids\": {"
    "      \"trakt\": 353,"
    "      \"slug\": \"game-of-thrones\","
    "      \"tvdb\": 121361,"
    "      \"imdb\": \"tt0944947\","
    "      \"tmdb\": 1399,"
    "      \"tvrage\": 24493"
    "    }"
    "  },"
    "  {"
    "    \"title\": \"Lost\","
    "    \"year\": 2004,"
    "    \"ids\": {"
    "      \"trakt\": 511,"
    "      \"slug\": \"lost\","
    "      \"tvdb\": 73739,"
    "      \"imdb\": \"tt0411008\","
    "      \"tmdb\": 4607,"
    "      \"tvrage\": null"
    "    }"
    "  },"
    "  {"
    "    \"title\": \"24\","
    "    \"year\": 2001,"
    "    \"ids\": {"
    "      \"trakt\": 460,"
    "      \"slug\": \"24\","
    "      \"tvdb\": 76290,"
    "      \"imdb\": \"tt0285331\","
    "      \"tmdb\": 1973,"
    "      \"tvrage\": 2445"
    "    }"
    "  },"
    "  {"
    "    \"title\": \"Battlestar Galactica\","
    "    \"year\": 2005,"
    "    \"ids\": {"
    "      \"trakt\": 331,"
    "      \"slug\": \"battlestar-galactica\","
    "      \"tvdb\": 73545,"
    "      \"imdb\": \"tt0407362\","
    "      \"tmdb\": 1972,"
    "      \"tvrage\": null"
    "    }"
    "  },"
    "  {"
    "    \"title\": \"Breaking Bad\","
    "    \"year\": 2008,"
    "    \"ids\": {"
    "      \"trakt\": 1,"
    "      \"slug\": \"breaking-bad\","
    "      \"tvdb\": 81189,"
    "      \"imdb\": \"tt0903747\","
    "      \"tmdb\": 1396,"
    "      \"tvrage\": 18164"
    "    }"
    "  },"
    "  {"
    "    \"title\": \"Firefly\","
    "    \"year\": 2002,"
    "    \"ids\": {"
    "      \"trakt\": 329,"
    "      \"slug\": \"firefly\","
    "      \"tvdb\": 78874,"
    "      \"imdb\": \"tt0303461\","
    "      \"tmdb\": 1437,"
    "      \"tvrage\": null"
    "    }"
    "  }"
    "]"
};

TraktRequest *newTrackRequest(QObject *parent)
{
    return new TraktRequestMock(parent);
}

TraktRequestMock::TraktRequestMock(QObject *parent)
    : TraktRequest(parent)
{
    for(int s = 0; s < MAX_SEASONS; s++) {
        for(int e = 1; e <= MAX_EPISODES; e++) {
            QString episode_image(episode_images_template);
            QString season = QString::number(s);
            QString episode = QString::number(e);
            episode_image.replace(QStringLiteral("%SEASON%"), season);
            episode_image.replace(QStringLiteral("%EPISODE%"), episode);
            show_353_seasons_episode_images[s] << episode_image;

            QString episode_full(episode_full_template);
            episode_full.replace(QStringLiteral("%SEASON%"), season);
            episode_full.replace(QStringLiteral("%EPISODE%"), episode);
            show_353_seasons_episode_full[s] << episode_full;
        }
    }
}

TraktRequestMock::~TraktRequestMock()
{
}

void TraktRequestMock::start()
{
    QUrl rUrl = url();
    if (rUrl.toString() == "https://api-v2launch.trakt.tv//shows/353?extended=images") {
        Q_EMIT finished(rUrl, QJsonDocument::fromJson(show_353_images));
    } else if (rUrl.toString() == "https://api-v2launch.trakt.tv//shows/353?extended=full"){
        Q_EMIT finished(rUrl, QJsonDocument::fromJson(show_353_full));
    } else if (rUrl.toString() == "https://api-v2launch.trakt.tv//shows/353/seasons?extended=images") {
        Q_EMIT finished(rUrl, QJsonDocument::fromJson(show_353_seasons_image));
    } else if (rUrl.toString() == "https://api-v2launch.trakt.tv//shows/353/seasons?extended=full") {
        Q_EMIT finished(rUrl, QJsonDocument::fromJson(show_353_seasons_full));
    } else if (rUrl.toString() == "https://api-v2launch.trakt.tv//shows/popular?page=1&limit=10") {
        Q_EMIT finished(rUrl, QJsonDocument::fromJson(show_popular_page_1));
    } else if (rUrl.toString() == "https://api-v2launch.trakt.tv/shows/353/seasons/1") {
        Q_EMIT finished(rUrl, QJsonDocument::fromJson(show_353_season_1_episode_list));
    } else if (rUrl.toString() == "https://api-v2launch.trakt.tv/shows/353/seasons/0") {
        Q_EMIT finished(rUrl, QJsonDocument::fromJson(show_353_season_1_episode_list));
    } else {
        QByteArray data = episodeData(rUrl);
        if (!data.isEmpty()) {
            Q_EMIT finished(rUrl, QJsonDocument::fromJson(data));
        } else {
            qWarning() << "Not implemented:" << rUrl;
            Q_EMIT error(QNetworkReply::ContentNotFoundError);
        }
    }
}

QByteArray TraktRequestMock::episodeData(const QUrl &url) const
{
    QString rUrl = url.toString();

    bool ok = false;
    int season = QString(rUrl.mid(49, rUrl.indexOf("/", 49) - 49)).toInt(&ok);
    if (!ok) {
        qWarning() << "INVALID CHAR AT 51" << rUrl.mid(49, rUrl.indexOf("/", 49) - 49);
        return QByteArray();
    }
    int episode = QString(rUrl.mid(60, rUrl.indexOf("?", 60) - 60)).toInt(&ok);
    if (!ok) {
        qWarning() << "INVALID CHAR AT 60" << rUrl.mid(60, rUrl.indexOf("?", 60) -60 );
        return QByteArray();
    }
    if ((season < MAX_SEASONS)  && (episode > 0) && (episode <= MAX_EPISODES)) {
        if (rUrl.endsWith("images")) {
            return show_353_seasons_episode_images[season][episode-1].toUtf8();
        } else if (rUrl.endsWith("full")) {
            return show_353_seasons_episode_full[season][episode-1].toUtf8();
        } else {
            qWarning() << "Invalid episode url" << rUrl;
        }
    } else {
        qWarning() << "Invalid episode range" << season << episode;
    }
    return QByteArray();
}
