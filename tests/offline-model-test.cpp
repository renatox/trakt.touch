#include <QObject>
#include <QtTest>
#include <QDebug>
#include <QDir>

#include "show-info.h"
#include "offline-shows.h"

class OfflineModelTest : public QObject
{
    Q_OBJECT
private:
    void removeDatabase()
    {
        QFileInfo dbFile(Database::fullPath());
        if (dbFile.exists()) {
            QVERIFY(QFile::remove(dbFile.absoluteFilePath()));
        }
    }

private Q_SLOTS:
    void initTestCase()
    {
        QStandardPaths::setTestModeEnabled(true);
    }

    void init()
    {
        removeDatabase();
    }

    void cleanup()
    {
        Database::instance()->destroy();
        removeDatabase();
    }

    void testModelInitialization()
    {
        QScopedPointer<OfflineShows> model(new OfflineShows());
        QTRY_COMPARE(model->rowCount(QModelIndex()), 0);

        {
            QScopedPointer<ShowInfo> s(new ShowInfo("353"));
            QSignalSpy downloadFinishedSpy(s.data(), SIGNAL(downloadFinished(bool)));
            s->download(false);
            QTRY_COMPARE(downloadFinishedSpy.count(), 1);
            s->save();
        }

        QTRY_COMPARE(model->rowCount(QModelIndex()), 1);
        QCOMPARE(model->data(model->index(0), ShowInfo::FieldTraktId).toString(), QStringLiteral("353"));
        QCOMPARE(model->data(model->index(0), ShowInfo::FieldImdbId).toString(), QStringLiteral("tt0944947"));
        QCOMPARE(model->data(model->index(0), ShowInfo::FieldTitle).toString(), QStringLiteral("Game of Thrones"));

        {
            QScopedPointer<ShowInfo> s(new ShowInfo("353"));
            s->remove();
        }

        QTRY_COMPARE(model->rowCount(QModelIndex()), 0);
    }
};

QTEST_MAIN(OfflineModelTest)

#include "offline-model-test.moc"
