
#include <QObject>
#include <QtTest>
#include <QDebug>
#include <QDir>
#include <QScopedPointer>

#include "show-info.h"
#include "season-info.h"
#include "trakt-request-mock.h"

class SeasonTest : public QObject
{
    Q_OBJECT
private:
    QMap<QString, ShowInfo*> m_shows;

    void removeDatabase()
    {
        QFileInfo dbFile(Database::fullPath());
        if (dbFile.exists()) {
            QVERIFY(QFile::remove(dbFile.absoluteFilePath()));
        }
    }

    ShowInfo *show(const QString &id)
    {
        if (!m_shows.contains(id)) {
            m_shows.insert(id, new ShowInfo(id));
        }
        return m_shows[id];
    }

    void downloadSeasonInfo(const QString &showId,
                            uint seasonNumber,
                            SeasonInfo **season)
    {
        ShowInfo *s = show(showId);
        QSignalSpy downloadFinishedSpy(s, SIGNAL(downloadFinished(bool)));
        s->download(false);
        QTRY_COMPARE(downloadFinishedSpy.count(), 1);

        QSignalSpy downloadSeasonsFinishedSpy(s, SIGNAL(seasonsDowloaded(bool)));
        s->downloadSeasons();
        QTRY_COMPARE(downloadSeasonsFinishedSpy.count(), 1);

        QList<SeasonInfo*> seasons = s->seasons();
        QVERIFY(seasons.size() >= seasonNumber);
        *season = seasons[seasonNumber];
    }

private Q_SLOTS:
    void initTestCase()
    {
        QStandardPaths::setTestModeEnabled(true);
    }

    void init()
    {
        removeDatabase();
    }

    void cleanup()
    {
        qDeleteAll(m_shows.values());
        m_shows.clear();
        Database::instance()->destroy();
        //removeDatabase();
    }

    void testParseSeason()
    {
        SeasonInfo *season = 0;
        downloadSeasonInfo(QStringLiteral("353"), 0, &season);
        QCOMPARE(season->showId(), QStringLiteral("353"));
        QCOMPARE(season->number(), (uint) 0);

        QCOMPARE(season->poster()["full"].toUrl(), QUrl("https://walter.trakt.us/images/seasons/000/002/145/posters/original/41221f3712.jpg?1409351965"));
        QCOMPARE(season->poster()["medium"].toUrl(), QUrl("https://walter.trakt.us/images/seasons/000/002/145/posters/medium/41221f3712.jpg?1409351965"));
        QCOMPARE(season->poster()["thumb"].toUrl(), QUrl("https://walter.trakt.us/images/seasons/000/002/145/posters/thumb/41221f3712.jpg?1409351965"));

        QCOMPARE(season->thumb(), QUrl("https://walter.trakt.us/images/seasons/000/002/145/thumbs/original/c41b46dd09.jpg?1409351965"));
        QCOMPARE(season->airedEpisodes(), (uint) 10);
        QCOMPARE(season->episodeCount(), (uint) 10);
        QCOMPARE(season->watchedEpisodes(), (uint) 0);
        QCOMPARE(season->overview(), QStringLiteral(""));

        SeasonInfo *season1 = 0;
        downloadSeasonInfo(QStringLiteral("353"), 1, &season1);
        QCOMPARE(season1->showId(), QStringLiteral("353"));
        QCOMPARE(season1->number(), (uint) 1);

        QCOMPARE(season1->poster()["full"].toUrl(), QUrl("https://walter.trakt.us/images/seasons/000/002/146/posters/original/1834978783.jpg?1409351969"));
        QCOMPARE(season1->poster()["medium"].toUrl(), QUrl("https://walter.trakt.us/images/seasons/000/002/146/posters/medium/1834978783.jpg?1409351969"));
        QCOMPARE(season1->poster()["thumb"].toUrl(), QUrl("https://walter.trakt.us/images/seasons/000/002/146/posters/thumb/1834978783.jpg?1409351969"));

        QCOMPARE(season1->thumb(), QUrl("https://walter.trakt.us/images/seasons/000/002/146/thumbs/original/6c996deed7.jpg?1409351970"));
        QCOMPARE(season1->airedEpisodes(), (uint) 10);
        QCOMPARE(season1->episodeCount(), (uint) 10);
        QCOMPARE(season1->watchedEpisodes(), (uint) 0);
        QCOMPARE(season1->overview(), QStringLiteral("Season 1 overview."));
    }

    void testSaveSeasonInfo()
    {
        {
            SeasonInfo *season = 0;
            downloadSeasonInfo(QStringLiteral("353"), 0, &season);
            season->save();
        }

        {
            QList<SeasonInfo*> seasons = SeasonInfo::all(QStringLiteral("353"), 0);
            QCOMPARE(seasons.size(), 1);
            SeasonInfo *season = seasons.value(0);

            QCOMPARE(season->showId(), QStringLiteral("353"));
            QCOMPARE(season->number(), (uint) 0);
            QCOMPARE(season->thumb(), QUrl("https://walter.trakt.us/images/seasons/000/002/145/thumbs/original/c41b46dd09.jpg?1409351965"));
            QCOMPARE(season->airedEpisodes(), (uint) 10);
            QCOMPARE(season->episodeCount(), (uint) 10);
            QCOMPARE(season->watchedEpisodes(), (uint) 0);
            QCOMPARE(season->overview(), QStringLiteral(""));

            qDeleteAll(seasons);
        }
    }
};

QTEST_MAIN(SeasonTest)

#include "season-test.moc"
