#include <QObject>
#include <QtTest>
#include <QDebug>
#include <QDir>

#include "show-info.h"
#include "season-info.h"
#include "episode-info.h"
#include "show-downloader.h"
#include "trakt-request-mock.h"


class ShowDownloaderTest : public QObject
{
    Q_OBJECT
private:
    void removeDatabase()
    {
        QFileInfo dbFile(Database::fullPath());
        if (dbFile.exists()) {
            QVERIFY(QFile::remove(dbFile.absoluteFilePath()));
        }
    }

private Q_SLOTS:
    void initTestCase()
    {
        QStandardPaths::setTestModeEnabled(true);
    }

    void init()
    {
        removeDatabase();
    }

    void cleanup()
    {
        Database::instance()->destroy();
        removeDatabase();
    }

    void testShowDownload()
    {
        QScopedPointer<ShowInfo> s(new ShowInfo("353"));
        QPointer<ShowDownloader> d(new ShowDownloader(s.data()));

        QSignalSpy downloadFinishedSpy(d.data(), SIGNAL(downloadFinished()));
        d->start();

        QTRY_COMPARE(downloadFinishedSpy.count(), 1);
        QTRY_VERIFY(d.isNull());

        // show info
        QCOMPARE(s->saved(), false);
        QCOMPARE(s->traktId(), QStringLiteral("353"));
        QCOMPARE(s->imdbId(), QStringLiteral("tt0944947"));
        QCOMPARE(s->title(), QStringLiteral("Game of Thrones"));
        QCOMPARE(s->updatedAt(), QDateTime::fromString("2014-08-22T08:32:06.000Z", Qt::ISODate));
        QCOMPARE(s->year(), (uint) 2011);
        QCOMPARE(s->airedEpisodes(), (uint)50);

        // season info
        QList<SeasonInfo*> seasons = s->seasons();
        QCOMPARE(seasons.size(), 2);

        SeasonInfo *s0 = seasons.value(0);
        QCOMPARE(s0->showId(), QStringLiteral("353"));
        QCOMPARE(s0->number(), (uint) 0);
        QCOMPARE(s0->thumb(), QUrl("https://walter.trakt.us/images/seasons/000/002/145/thumbs/original/c41b46dd09.jpg?1409351965"));
        QCOMPARE(s0->airedEpisodes(), (uint) 10);
        QCOMPARE(s0->episodeCount(), (uint) 10);
        QCOMPARE(s0->watchedEpisodes(), (uint) 0);
        QCOMPARE(s0->overview(), QStringLiteral(""));

        SeasonInfo *s1 = seasons.value(1);
        QCOMPARE(s1->showId(), QStringLiteral("353"));
        QCOMPARE(s1->number(), (uint) 1);
        QCOMPARE(s1->thumb(), QUrl("https://walter.trakt.us/images/seasons/000/002/146/thumbs/original/6c996deed7.jpg?1409351970"));
        QCOMPARE(s1->airedEpisodes(), (uint) 10);
        QCOMPARE(s1->episodeCount(), (uint) 10);
        QCOMPARE(s1->watchedEpisodes(), (uint) 0);
        QCOMPARE(s1->overview(), QStringLiteral("Season 1 overview."));

        // episodes info
        QList<EpisodeInfo*> episodesS0 = s0->episodes();
        QCOMPARE(episodesS0.count(), 10);
        QCOMPARE(s0->episodeCount(), (uint) episodesS0.count());
        for(uint i=0; i < episodesS0.count(); i++) {
            EpisodeInfo *e = episodesS0[i];
            QCOMPARE(e->showId(), QStringLiteral("353"));
            QCOMPARE(e->season(), (uint) 0);
            QCOMPARE(e->number(), i + 1);
            QCOMPARE(e->overview(), QStringLiteral("Overview episode %1 season 0").arg(i + 1));
            QCOMPARE(e->watched(), false);
        }

        QList<EpisodeInfo*> episodesS1 = s1->episodes();
        QCOMPARE(episodesS1.count(), 10);
        QCOMPARE(s1->episodeCount(), (uint) episodesS1.count());
        for(uint i=0; i < episodesS1.count(); i++) {
            EpisodeInfo *e = episodesS1[i];
            QCOMPARE(e->showId(), QStringLiteral("353"));
            QCOMPARE(e->season(), (uint) 1);
            QCOMPARE(e->number(), i + 1);
            QCOMPARE(e->overview(), QStringLiteral("Overview episode %1 season 1").arg(i + 1));
            QCOMPARE(e->watched(), false);
        }
    }

    void testForNewEpisodes()
    {
        {
            QScopedPointer<ShowInfo> s(new ShowInfo("353"));
            QPointer<ShowDownloader> d(new ShowDownloader(s.data()));

            QSignalSpy downloadFinishedSpy(d.data(), SIGNAL(downloadFinished()));
            d->start();

            QTRY_COMPARE(downloadFinishedSpy.count(), 1);
            QTRY_VERIFY(d.isNull());

            SeasonInfo *season = s->seasons()[0];
            QCOMPARE(season->number(), (uint) 0);
            QCOMPARE(season->watchedEpisodes(), (uint) 0);
            season->episode(0)->setWatched(true);
            QCOMPARE(season->watchedEpisodes(), (uint) 1);

            QPointer<ShowDownloader> d2(new ShowDownloader(s.data()));
            d2->setCheckForNewEpisode(true);

            QSignalSpy downloadFinishedSpy2(d2.data(), SIGNAL(downloadFinished()));
            d2->start();
            QTRY_COMPARE(downloadFinishedSpy2.count(), 1);

            season = s->seasons()[0];
            QCOMPARE(season->number(), (uint) 0);
            QCOMPARE(season->watchedEpisodes(), (uint) 1);
        }
    }
};

QTEST_MAIN(ShowDownloaderTest)

#include "show-downloader-test.moc"
