#include <QObject>
#include <QtTest>
#include <QDebug>
#include <QDir>

#include "show-info.h"
#include "database.h"

class DatabaseTest : public QObject
{
    Q_OBJECT
private:
    void removeDatabase()
    {
        QFileInfo dbFile(Database::fullPath());
        if (dbFile.exists()) {
            QVERIFY(QFile::remove(dbFile.absoluteFilePath()));
        }
    }

private Q_SLOTS:
    void initTestCase()
    {
        QStandardPaths::setTestModeEnabled(true);
    }

    void init()
    {
        removeDatabase();
    }

    void cleanup()
    {
        Database::instance()->destroy();
        removeDatabase();
    }

    void testItemAddedAndItemRemovedSignal()
    {
        Database *db = Database::instance();
        QSignalSpy itemAddedSpy(db, SIGNAL(itemAdded(QString,QString)));
        QSignalSpy itemRemovedSpy(db, SIGNAL(itemRemoved(QString,QString)));

        QScopedPointer<ShowInfo> s(new ShowInfo("353"));
        QSignalSpy downloadFinishedSpy(s.data(), SIGNAL(downloadFinished(bool)));
        s->download(false);
        QTRY_COMPARE(downloadFinishedSpy.count(), 1);

        s->save();
        QTRY_COMPARE(itemAddedSpy.count(), 1);
        QCOMPARE(itemAddedSpy.first()[0].toString(), ShowInfo::staticTableName());
        QCOMPARE(itemAddedSpy.first()[1].toString(), s->id());

        s->remove();
        QTRY_COMPARE(itemRemovedSpy.count(), 1);
        QCOMPARE(itemRemovedSpy.first()[0].toString(), ShowInfo::staticTableName());
        QCOMPARE(itemRemovedSpy.first()[1].toString(), s->id());
    }

};

QTEST_MAIN(DatabaseTest)

#include "database-test.moc"
