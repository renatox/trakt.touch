#include <QObject>
#include <QtTest>
#include <QDebug>
#include <QDir>

#include "show-info.h"
#include "season-info.h"
#include "episode-info.h"
#include "calendar-model.h"
#include "show-downloader.h"

class CalendarModelTest : public QObject
{
    Q_OBJECT
private:
    void removeDatabase()
    {
        QFileInfo dbFile(Database::fullPath());
        if (dbFile.exists()) {
            QVERIFY(QFile::remove(dbFile.absoluteFilePath()));
        }
    }

private Q_SLOTS:
    void initTestCase()
    {
        QStandardPaths::setTestModeEnabled(true);
    }

    void init()
    {
        removeDatabase();
    }

    void cleanup()
    {
        Database::instance()->destroy();
        removeDatabase();
    }

    void testModelInitialization()
    {
        QScopedPointer<CalendarModel> model(new CalendarModel());
        QTRY_COMPARE(model->rowCount(QModelIndex()), 0);

        {
            QScopedPointer<ShowInfo> s(new ShowInfo("353"));
            QPointer<ShowDownloader> d(new ShowDownloader(s.data()));

            QSignalSpy downloadFinishedSpy(d.data(), SIGNAL(downloadFinished()));
            d->start();
            QTRY_COMPARE(downloadFinishedSpy.count(), 1);
            s->save();
            Q_FOREACH(SeasonInfo *season, s->seasons()) {
                season->save();
                Q_FOREACH(EpisodeInfo *e, season->episodes()) {
                    e->save();
                }
            }
        }

        QTRY_COMPARE(model->rowCount(QModelIndex()), 20);
        QCOMPARE(model->data(model->index(0), CalendarModel::RoleSection).toString(),
                 QStringLiteral("17, abril"));

        {
            QScopedPointer<ShowInfo> s(new ShowInfo("353"));
            Q_FOREACH(SeasonInfo *season, s->seasons()) {
                Q_FOREACH(EpisodeInfo *e, season->episodes()) {
                    e->remove();
                }
            }
        }

        QTRY_COMPARE(model->rowCount(QModelIndex()), 0);
    }
};

QTEST_MAIN(CalendarModelTest)

#include "calendar-model-test.moc"
