
#include <QObject>
#include <QtTest>
#include <QDebug>
#include <QDir>
#include <QScopedPointer>

#include "show-info.h"
#include "season-info.h"
#include "episode-info.h"
#include "trakt-request-mock.h"

class EpisodeTest : public QObject
{
    Q_OBJECT
private:
    QList<ShowInfo*> m_shows;

    void removeDatabase()
    {
        QFileInfo dbFile(Database::fullPath());
        if (dbFile.exists()) {
            QVERIFY(QFile::remove(dbFile.absoluteFilePath()));
        }
    }

    void downloadEpisode(const QString &showId,
                         uint seasonNumber,
                         uint episodeNumber,
                         EpisodeInfo **episode)
    {
        ShowInfo *s = new ShowInfo(showId);
        m_shows << s;
        QSignalSpy downloadFinishedSpy(s, SIGNAL(downloadFinished(bool)));
        s->download(false);
        QTRY_COMPARE(downloadFinishedSpy.count(), 1);

        QSignalSpy downloadSeasonsFinishedSpy(s, SIGNAL(seasonsDowloaded(bool)));
        s->downloadSeasons();
        QTRY_COMPARE(downloadSeasonsFinishedSpy.count(), 1);

        QList<SeasonInfo*> seasons = s->seasons();
        QVERIFY(seasons.size() >= seasonNumber);
        SeasonInfo *season = seasons[seasonNumber];

        QSignalSpy seasonDownloadedSpy(season, SIGNAL(downloadFinished(bool)));
        season->download();
        QTRY_COMPARE(seasonDownloadedSpy.count(), 1);

        QCOMPARE(season->episodes().count(), 10);
        *episode = season->episode(episodeNumber - 1);
        QCOMPARE((*episode)->id(),
                 QString("%1:%2:%3").arg(showId).arg(seasonNumber).arg(episodeNumber));
        QVERIFY(*episode);
        QSignalSpy episodeDownloaded((*episode), SIGNAL(downloadFinished(bool)));
        (*episode)->download();
        QTRY_COMPARE(episodeDownloaded.count(), 1);
    }

private Q_SLOTS:
    void initTestCase()
    {
        QStandardPaths::setTestModeEnabled(true);
    }

    void init()
    {
        removeDatabase();
    }

    void cleanup()
    {
        qDeleteAll(m_shows);
        m_shows.clear();
        Database::instance()->destroy();
        removeDatabase();
    }

    void testParseEpisode()
    {
        EpisodeInfo *e = 0;
        downloadEpisode(QStringLiteral("353"), 1, 1, &e);
        QCOMPARE(e->showId(), QStringLiteral("353"));
        QCOMPARE(e->season(), (uint) 1);
        QCOMPARE(e->number(), (uint) 1);
        QCOMPARE(e->overview(), QStringLiteral("Overview episode 1 season 1"));
        QCOMPARE(e->rating(), 9.0);
        QCOMPARE(e->votes(), (uint) 111);
        QCOMPARE(e->firstAired(), QDateTime::fromString("2011-04-18T01:00:00.000Z", Qt::ISODate));
        QCOMPARE(e->watched(), false);

        QSignalSpy episodeExtendedInfoDownloaded(e, SIGNAL(extendedInfoDownloaded(bool)));
        e->downloadExtendedInfo();
        QTRY_COMPARE(episodeExtendedInfoDownloaded.count(), 1);

        QCOMPARE(e->screenshot()["full"].toUrl(),
                QUrl("https://walter.trakt.us/images/screenshot_full_1_1.jpg"));
        QCOMPARE(e->screenshot()["medium"].toUrl(),
                QUrl("https://walter.trakt.us/images/screenshot_medium_1_1.jpg"));
        QCOMPARE(e->screenshot()["thumb"].toUrl(),
                QUrl("https://walter.trakt.us/images/screenshot_thumb_1_1.jpg"));
    }

    void testSaveEpisodeInfo()
    {
        {
            EpisodeInfo *e = 0;
            downloadEpisode(QStringLiteral("353"), 1, 1, &e);
            e->save();
        }

        {
            QList<EpisodeInfo*> episodes = EpisodeInfo::all(QStringLiteral("353"), 1, 0);
            QCOMPARE(episodes.size(), 1);
            EpisodeInfo *episode = episodes.value(0);

            QCOMPARE(episode->showId(), QStringLiteral("353"));
            QCOMPARE(episode->season(), (uint) 1);
            QCOMPARE(episode->number(), (uint) 1);
            QCOMPARE(episode->overview(), QStringLiteral("Overview episode 1 season 1"));
            QCOMPARE(episode->rating(), 9.0);
            QCOMPARE(episode->votes(), (uint) 111);
            QCOMPARE(episode->firstAired(), QDateTime::fromString("2011-04-18T01:00:00.000Z", Qt::ISODate));
            QCOMPARE(episode->watched(), false);

            qDeleteAll(episodes);
        }
    }

    void testChangePropagate()
    {
        EpisodeInfo *e1 = 0;
        downloadEpisode(QStringLiteral("353"), 1, 1, &e1);

        EpisodeInfo *e2 = 0;
        downloadEpisode(QStringLiteral("353"), 1, 1, &e2);

        QSignalSpy episode1Changed(e1, SIGNAL(episodeChanged()));
        QSignalSpy episode2Changed(e2, SIGNAL(episodeChanged()));

        e1->setWatched(true);

        QTRY_COMPARE(episode1Changed.count(), 1);
        QTRY_COMPARE(episode2Changed.count(), 1);

        QCOMPARE(e1->watched(), true);
        QCOMPARE(e2->watched(), true);
    }

    void testUpdateEpisode()
    {
        {
            EpisodeInfo *e1 = 0;
            downloadEpisode(QStringLiteral("353"), 1, 1, &e1);
            e1->save();
        }

        {
            QList<EpisodeInfo*> episodes = EpisodeInfo::all(QStringLiteral("353"), 1, 0);
            QCOMPARE(episodes.size(), 1);
            EpisodeInfo *episode = episodes.value(0);
            episode->setWatched(true);
            episode->save();
            QCOMPARE(episode->watched(), true);
            qDeleteAll(episodes);
            episodes.clear();
        }
        Database::instance()->clearCache();
        {
            QList<EpisodeInfo*> episodes = EpisodeInfo::all(QStringLiteral("353"), 1, 0);
            QCOMPARE(episodes.size(), 1);
            EpisodeInfo *episode = episodes.value(0);
            QCOMPARE(episode->watched(), true);
            qDeleteAll(episodes);
            episodes.clear();
        }
    }
};

QTEST_MAIN(EpisodeTest)

#include "episode-test.moc"
