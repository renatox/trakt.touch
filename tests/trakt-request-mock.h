#ifndef __TRAKT_REQUEST_MOCK_H__
#define __TRAKT_REQUEST_MOCK_H__

#include <QObject>

#include "trakt-request.h"

class TraktRequestMock : public TraktRequest
{
    Q_OBJECT
public:
    TraktRequestMock(QObject *parent = 0);
    ~TraktRequestMock();

    // virtual
    void start();
    QByteArray episodeData(const QUrl &url) const;

private:
    QStringList show_353_seasons_episode_images[2];
    QStringList show_353_seasons_episode_full[2];
};

#endif
