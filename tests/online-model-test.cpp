#include <QObject>
#include <QtTest>
#include <QDebug>
#include <QDir>

#include "show-info.h"
#include "online-shows.h"

class OnlineModelTest : public QObject
{
    Q_OBJECT
private:
    void removeDatabase()
    {
        QFileInfo dbFile(Database::fullPath());
        if (dbFile.exists()) {
            QVERIFY(QFile::remove(dbFile.absoluteFilePath()));
        }
    }

private Q_SLOTS:
    void initTestCase()
    {
        QStandardPaths::setTestModeEnabled(true);
    }

    void init()
    {
        removeDatabase();
    }

    void cleanup()
    {
        Database::instance()->destroy();
        removeDatabase();
    }

    void testModelInitialization()
    {
        QScopedPointer<OnlineShows> model(new OnlineShows());
        QTRY_COMPARE(model->loading(), false);
        QTRY_COMPARE(model->rowCount(QModelIndex()), 10);

        QCOMPARE(model->data(model->index(4), ShowInfo::FieldTraktId).toString(), QStringLiteral("353"));
        QCOMPARE(model->data(model->index(4), ShowInfo::FieldImdbId).toString(), QStringLiteral("tt0944947"));
        QCOMPARE(model->data(model->index(4), ShowInfo::FieldTitle).toString(), QStringLiteral("Game of Thrones"));
        QCOMPARE(model->data(model->index(4), OnlineShows::RuleSaved).toBool(), false);

        {
            QScopedPointer<ShowInfo> s(new ShowInfo("353"));
            QCOMPARE(model->data(model->index(4), ShowInfo::FieldTitle).toString(), s->title());
            QCOMPARE(s->saved(), false);

            QSignalSpy modelDataChanged(model.data(), SIGNAL(dataChanged(QModelIndex,QModelIndex)));
            model->save(4);
            QTRY_COMPARE(s->saved(), true);
            QVERIFY(modelDataChanged.count() > 1);
            QCOMPARE(modelDataChanged.first()[0].toModelIndex().row(), 4);
            QCOMPARE(model->data(model->index(4), OnlineShows::RuleSaved).toBool(), true);
        }

        {
            QScopedPointer<ShowInfo> s(new ShowInfo("353"));
            QTRY_COMPARE(s->saved(), true);

            QSignalSpy modelDataChanged(model.data(), SIGNAL(dataChanged(QModelIndex,QModelIndex)));
            model->remove(4);
            QTRY_COMPARE(s->saved(), false);
            QTRY_COMPARE(modelDataChanged.count(), 1);
            QCOMPARE(modelDataChanged.first()[0].toModelIndex().row(), 4);
            QCOMPARE(model->data(model->index(4), OnlineShows::RuleSaved).toBool(), false);
        }
    }

    void testModelDownloadSavedShows()
    {
        {
            QScopedPointer<ShowInfo> s(new ShowInfo("353"));
            QPointer<ShowDownloader> d(new ShowDownloader(s.data()));
            QSignalSpy downloadFinishedSpy(d.data(), SIGNAL(downloadFinished()));
            d->start();
            QTRY_COMPARE(downloadFinishedSpy.count(), 1);
            s->save();
        }

        QScopedPointer<OnlineShows> model(new OnlineShows());
        QTRY_COMPARE(model->loading(), false);
        QCOMPARE(model->data(model->index(4), ShowInfo::FieldTraktId).toString(), QStringLiteral("353"));
        QCOMPARE(model->data(model->index(4), OnlineShows::RuleSaved).toBool(), true);
    }

    void testSaveShow()
    {
        {
            QScopedPointer<OnlineShows> model(new OnlineShows());
            QTRY_COMPARE(model->loading(), false);
            model->save(4);
            QTRY_COMPARE(model->downloading(), false);
        }
        {
            QScopedPointer<OnlineShows> model(new OnlineShows());
            QTRY_COMPARE(model->loading(), false);
            QCOMPARE(model->data(model->index(4), OnlineShows::RuleSaved).toBool(), true);
        }
    }
};

QTEST_MAIN(OnlineModelTest)

#include "online-model-test.moc"
