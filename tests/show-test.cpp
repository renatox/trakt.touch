#include <QObject>
#include <QtTest>
#include <QDebug>
#include <QDir>

#include "show-info.h"
#include "trakt-request-mock.h"

class ShowTest : public QObject
{
    Q_OBJECT
private:
    void removeDatabase()
    {
        QFileInfo dbFile(Database::fullPath());
        if (dbFile.exists()) {
            QVERIFY(QFile::remove(dbFile.absoluteFilePath()));
        }
    }

private Q_SLOTS:
    void initTestCase()
    {
        QStandardPaths::setTestModeEnabled(true);
    }

    void init()
    {
        removeDatabase();
    }

    void cleanup()
    {
        Database::instance()->destroy();
        removeDatabase();
    }

    void testParseShowInfo()
    {
        QScopedPointer<ShowInfo> s(new ShowInfo("353"));
        QSignalSpy downloadFinishedSpy(s.data(), SIGNAL(downloadFinished(bool)));
        s->download(false);
        QTRY_COMPARE(downloadFinishedSpy.count(), 1);
        QCOMPARE(s->saved(), false);
        QCOMPARE(s->traktId(), QStringLiteral("353"));
        QCOMPARE(s->imdbId(), QStringLiteral("tt0944947"));
        QCOMPARE(s->title(), QStringLiteral("Game of Thrones"));
        QCOMPARE(s->updatedAt(), QDateTime::fromString("2014-08-22T08:32:06.000Z", Qt::ISODate));
        QCOMPARE(s->year(), (uint) 2011);

        QCOMPARE(s->fanArt()["full"].toUrl(), QUrl("https://walter.trakt.us/images/shows/000/000/353/fanarts/original/5a13b51164.jpg?1409353811"));
        QCOMPARE(s->fanArt()["medium"].toUrl(), QUrl("https://walter.trakt.us/images/shows/000/000/353/fanarts/medium/5a13b51164.jpg?1409353811"));
        QCOMPARE(s->fanArt()["thumb"].toUrl(), QUrl("https://walter.trakt.us/images/shows/000/000/353/fanarts/thumb/5a13b51164.jpg?1409353811"));

        QCOMPARE(s->poster()["full"].toUrl(), QUrl("https://walter.trakt.us/images/shows/000/000/353/posters/original/46cafaec1f.jpg?1409353810"));
        QCOMPARE(s->poster()["medium"].toUrl(), QUrl("https://walter.trakt.us/images/shows/000/000/353/posters/medium/46cafaec1f.jpg?1409353810"));
        QCOMPARE(s->poster()["thumb"].toUrl(), QUrl("https://walter.trakt.us/images/shows/000/000/353/posters/thumb/46cafaec1f.jpg?1409353810"));

        QCOMPARE(s->logo()["full"].toUrl(), QUrl("https://walter.trakt.us/images/shows/000/000/353/logos/original/13b614ad43.png?1409353812"));
        QCOMPARE(s->logo()["medium"].toUrl(), QUrl());
        QCOMPARE(s->logo()["thumb"].toUrl(), QUrl());

        QCOMPARE(s->clearArt()["full"].toUrl(), QUrl("https://walter.trakt.us/images/shows/000/000/353/cleararts/original/5cbde9e647.png?1409353813"));
        QCOMPARE(s->clearArt()["medium"].toUrl(), QUrl());
        QCOMPARE(s->clearArt()["thumb"].toUrl(), QUrl());

        QCOMPARE(s->banner()["full"].toUrl(), QUrl("https://walter.trakt.us/images/shows/000/000/353/banners/original/9fefff703d.jpg?1409353813"));
        QCOMPARE(s->banner()["medium"].toUrl(), QUrl());
        QCOMPARE(s->banner()["thumb"].toUrl(), QUrl());

        QCOMPARE(s->thumb(), QUrl("https://walter.trakt.us/images/shows/000/000/353/thumbs/original/607f27fade.jpg?1409353814"));

        QCOMPARE(s->overview(), QStringLiteral("Game of Thrones is an American fantasy drama television series created for HBO by David Benioff and D. B. Weiss. It is an adaptation of A Song of Ice and Fire, George R. R. Martin's series of fantasy novels, the first of which is titled A Game of Thrones.\n\nThe series, set on the fictional continents of Westeros and Essos at the end of a decade-long summer, interweaves several plot lines. The first follows the members of several noble houses in a civil war for the Iron Throne of the Seven Kingdoms; the second covers the rising threat of the impending winter and the mythical creatures of the North; the third chronicles the attempts of the exiled last scion of the realm's deposed dynasty to reclaim the throne. Through its morally ambiguous characters, the series explores the issues of social hierarchy, religion, loyalty, corruption, sexuality, civil war, crime, and punishment."));
        QCOMPARE(s->firstAired(), QDate::fromString("2011-04-18T01:00:00.000Z", Qt::ISODate));
        QCOMPARE(s->airs()["day"].toString(), QStringLiteral("Sunday"));
        QCOMPARE(s->airs()["time"].toString(), QStringLiteral("21:00"));
        QCOMPARE(s->airs()["timezone"].toString(), QStringLiteral("America/New_York"));
        QCOMPARE(s->network(), QStringLiteral("HBO"));
        QCOMPARE(s->homePage(), QUrl("http://www.hbo.com/game-of-thrones/index.html"));
        QCOMPARE(s->status(), QStringLiteral("returning series"));
        QCOMPARE(s->rating(), 9.0);
        QCOMPARE(s->genres(), QStringList() << "drama" << "fantasy");
        QCOMPARE(s->airedEpisodes(), (uint)50);
    }

    void testSaveShowInfo()
    {
        {
            QScopedPointer<ShowInfo> s(new ShowInfo("353"));
            QSignalSpy downloadFinishedSpy(s.data(), SIGNAL(downloadFinished(bool)));
            s->download(false);
            QTRY_COMPARE(downloadFinishedSpy.count(), 1);

            QSignalSpy saveChangedSpy(s.data(), SIGNAL(savedChanged()));
            s->save();
            QTRY_COMPARE(saveChangedSpy.count(), 1);
            QCOMPARE(s->saved(), true);
        }

        {
            QScopedPointer<ShowInfo> s(new ShowInfo("353"));
            QCOMPARE(s->traktId(), QStringLiteral("353"));
            QCOMPARE(s->imdbId(), QStringLiteral("tt0944947"));
            QCOMPARE(s->title(), QStringLiteral("Game of Thrones"));
            QCOMPARE(s->saved(), true);
            s->download(false);
            QCOMPARE(s->saved(), true);
        }
    }

    void testShowInfoSeries()
    {
        QScopedPointer<ShowInfo> s(new ShowInfo("353"));
        QSignalSpy downloadFinishedSpy(s.data(), SIGNAL(downloadFinished(bool)));
        s->download(false);
        QTRY_COMPARE(downloadFinishedSpy.count(), 1);

        QSignalSpy downloadSeasonsFinishedSpy(s.data(), SIGNAL(seasonsDowloaded(bool)));
        s->downloadSeasons();
        QTRY_COMPARE(downloadSeasonsFinishedSpy.count(), 1);
        QCOMPARE(s->seasons().size(), 2);
    }

    void testReloadShow()
    {
        {
            QScopedPointer<ShowInfo> s(new ShowInfo("353"));
            QSignalSpy downloadFinishedSpy(s.data(), SIGNAL(downloadFinished(bool)));
            s->download(false);
            QTRY_COMPARE(downloadFinishedSpy.count(), 1);
            s->save();
            QCOMPARE(s->saved(), true);
        }
        Database::instance()->clearCache();
        {
            QScopedPointer<ShowInfo> s(new ShowInfo("353"));
            QCOMPARE(s->saved(), true);
            QCOMPARE(s->traktId(), QStringLiteral("353"));
            QCOMPARE(s->imdbId(), QStringLiteral("tt0944947"));
            QCOMPARE(s->title(), QStringLiteral("Game of Thrones"));
            QCOMPARE(s->firstAired(), QDate::fromString("2011-04-18T01:00:00.000Z", Qt::ISODate));
            QCOMPARE(s->year(), (uint) 2011);

            QCOMPARE(s->fanArt()["full"].toUrl(), QUrl("https://walter.trakt.us/images/shows/000/000/353/fanarts/original/5a13b51164.jpg?1409353811"));
            QCOMPARE(s->fanArt()["medium"].toUrl(), QUrl("https://walter.trakt.us/images/shows/000/000/353/fanarts/medium/5a13b51164.jpg?1409353811"));
            QCOMPARE(s->fanArt()["thumb"].toUrl(), QUrl("https://walter.trakt.us/images/shows/000/000/353/fanarts/thumb/5a13b51164.jpg?1409353811"));

            QCOMPARE(s->poster()["full"].toUrl(), QUrl("https://walter.trakt.us/images/shows/000/000/353/posters/original/46cafaec1f.jpg?1409353810"));
            QCOMPARE(s->poster()["medium"].toUrl(), QUrl("https://walter.trakt.us/images/shows/000/000/353/posters/medium/46cafaec1f.jpg?1409353810"));
            QCOMPARE(s->poster()["thumb"].toUrl(), QUrl("https://walter.trakt.us/images/shows/000/000/353/posters/thumb/46cafaec1f.jpg?1409353810"));

            QCOMPARE(s->logo()["full"].toUrl(), QUrl("https://walter.trakt.us/images/shows/000/000/353/logos/original/13b614ad43.png?1409353812"));
            QCOMPARE(s->logo()["medium"].toUrl(), QUrl());
            QCOMPARE(s->logo()["thumb"].toUrl(), QUrl());

            QCOMPARE(s->clearArt()["full"].toUrl(), QUrl("https://walter.trakt.us/images/shows/000/000/353/cleararts/original/5cbde9e647.png?1409353813"));
            QCOMPARE(s->clearArt()["medium"].toUrl(), QUrl());
            QCOMPARE(s->clearArt()["thumb"].toUrl(), QUrl());

            QCOMPARE(s->banner()["full"].toUrl(), QUrl("https://walter.trakt.us/images/shows/000/000/353/banners/original/9fefff703d.jpg?1409353813"));
            QCOMPARE(s->banner()["medium"].toUrl(), QUrl());
            QCOMPARE(s->banner()["thumb"].toUrl(), QUrl());

            QCOMPARE(s->thumb(), QUrl("https://walter.trakt.us/images/shows/000/000/353/thumbs/original/607f27fade.jpg?1409353814"));

            QCOMPARE(s->overview(), QStringLiteral("Game of Thrones is an American fantasy drama television series created for HBO by David Benioff and D. B. Weiss. It is an adaptation of A Song of Ice and Fire, George R. R. Martin's series of fantasy novels, the first of which is titled A Game of Thrones.\n\nThe series, set on the fictional continents of Westeros and Essos at the end of a decade-long summer, interweaves several plot lines. The first follows the members of several noble houses in a civil war for the Iron Throne of the Seven Kingdoms; the second covers the rising threat of the impending winter and the mythical creatures of the North; the third chronicles the attempts of the exiled last scion of the realm's deposed dynasty to reclaim the throne. Through its morally ambiguous characters, the series explores the issues of social hierarchy, religion, loyalty, corruption, sexuality, civil war, crime, and punishment."));
            QCOMPARE(s->firstAired(), QDate::fromString("2011-04-18T01:00:00.000Z", Qt::ISODate));
            QCOMPARE(s->airs()["day"].toString(), QStringLiteral("Sunday"));
            QCOMPARE(s->airs()["time"].toString(), QStringLiteral("21:00"));
            QCOMPARE(s->airs()["timezone"].toString(), QStringLiteral("America/New_York"));
            QCOMPARE(s->network(), QStringLiteral("HBO"));
            QCOMPARE(s->homePage(), QUrl("http://www.hbo.com/game-of-thrones/index.html"));
            QCOMPARE(s->status(), QStringLiteral("returning series"));
            QCOMPARE(s->rating(), 9.0);
            QCOMPARE(s->genres(), QStringList() << "drama" << "fantasy");
            QCOMPARE(s->airedEpisodes(), (uint)50);
        }

    }
};

QTEST_MAIN(ShowTest)

#include "show-test.moc"
